//
//  Created by Michael May on 05/10/2012.
//  Copyright (c) 2012 Michael May. All rights reserved.
//

// See http://cocoadev.com/wiki/LSUIElement for details of LSUIElement in Info.plist

// What the dummy menu is for:
// http://sunbruce.wordpress.com/2008/05/19/how-to-enable-keyboard-copycutpaste-shortcuts-in-nstextfield/

@import Cocoa;

int main(int argc, char *argv[])
{
    NSLog(@"main");
    
    return NSApplicationMain(argc, (const char **)argv);
}
