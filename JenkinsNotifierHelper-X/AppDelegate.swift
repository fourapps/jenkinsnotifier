//
//  Created by Developer on 2016/02/21.
//  Copyright © 2016 Michael May. All rights reserved.
//

import Cocoa

//@NSApplicationMain
public class AppDelegate: NSObject, NSApplicationDelegate {
    @IBOutlet weak var window: NSWindow!

    public func applicationDidFinishLaunching(aNotification: NSNotification) {
        print("here")
        NSLog("here too")
        
        NSLog("applicationDidFinishLaunching \(#file), \(#line)")
        
        //guard self.isMainAppAlreadyRunning() == false else { return }

        self.registerObserver()
        
        self.launchMainApplication()
    }
    
    public func applicationDidBecomeActive(notification: NSNotification) {
        NSLog("applicationDidBecomeActive \(#file), \(#line)")
        self.launchMainApplication()
    }
    
    public func applicationWillTerminate(notification: NSNotification) {
        NSLog("applicationWillTerminate \(#file), \(#line)")
        self.unregisterObserver()
    }
    
    private func registerObserver() {
        NSLog("registerObserver \(#file), \(#line)")
        
        // So that main app can later notify helper to terminate
//        NSDistributedNotificationCenter.defaultCenter().addObserver(self,
//                                                                    selector: "terminate:",
//                                                                    name: SharedValues.HelperApplicationTerminateNotificationName,
//                                                                    object: SharedValues.MainApplicatioBundleIdentifier);
    }
    
    private func unregisterObserver() {
        NSLog("unregisterObserver \(#file), \(#line)")
        
//        NSDistributedNotificationCenter.defaultCenter().removeObserver(self,
//            name: SharedValues.HelperApplicationTerminateNotificationName,
//            object: SharedValues.MainApplicatioBundleIdentifier)
    }
    
    private func isMainAppAlreadyRunning() -> Bool {
        NSLog("isMainAppAlreadyRunning \(#file), \(#line)")
        
        let runningApplications = NSWorkspace.sharedWorkspace().runningApplications;

        return self.isAppInList(SharedValues.MainApplicatioBundleIdentifier,
            runningApplications: runningApplications)
    }

    private func isAppInList(bundleIdentifier: String, runningApplications: [NSRunningApplication]) -> Bool {
        NSLog("isAppInList \(#file), \(#line)")

        return runningApplications.reduce(false, combine: {found, application in
            found || (application.bundleIdentifier == SharedValues.MainApplicatioBundleIdentifier)
        })
    }

    private func launchMainApplication() {
        NSLog("launchMainApplication \(#file), \(#line)")

        NSWorkspace.sharedWorkspace().launchApplication("/Applications/JenkinsNotifier.app/Contents/MacOS/JenkinsNotifier")
//            SharedValues.MainApplicationName,
//            showIcon: true,
//            autolaunch: true)
    }
    
    //    // Terminates helper app
    //    // Called by main app after main app has checked if helper app is still running
    //    // This allows main app to determine whether it was launched at login or not
    //    // For complete documentation see http://blog.timschroeder.net/2014/01/25/detecting-launch-at-login-revisited/
    private func terminate() {
        NSLog("terminating helper app")
        
        NSApp.terminate(nil)
    }
}

