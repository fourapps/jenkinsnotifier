//
//  Created by Developer on 2016/03/05.
//  Copyright © 2016 Michael May. All rights reserved.
//

import Foundation

@objc public class SharedValues : NSObject {
    public static let MainApplicationName = "JenkinsNotifier"
    public static let MainApplicatioBundleIdentifier = "com.wordpress.fourapps.JenkinsNotifier"
    
    public static let HelperApplicationName = "JenkinsNotifierHelper"
    public static let HelperApplicatioBundleIdentifier = "com.wordpress.fourapps.JenkinsNotifierHelper"
    public static let HelperApplicationTerminateNotificationName = "TerminateNotificiation"
}
