//
//  Created by Michael May on 27/05/2013.
//  Copyright (c) 2013-2015 Four Apps. All rights reserved.
//

/*
#import "JenkinsNotifier-Swift.h"
#import "XCUnitTests-Swift.h"

#import "JNJenkinsJobsLog.h"

@import XCTest;

@interface JNFailedJobLogTests : XCTestCase
@end


@implementation JNFailedJobLogTests {
@private
    JNJenkinsJobsLog *failedJobLog;
    JNJenkinsAPIJob *failedJob;
    NSString* jobUserInfo;
}

-(void)setUp {
    failedJobLog = [JNJenkinsJobsLog jobsLog];
    
    NSDictionary *failedJobDictionary = @{[JNJenkinsAPIJob kNCTJenkinsAPIJobsNameKey] : @"Name",
                                          [JNJenkinsAPIJob kNCTJenkinsAPIJobsURLKey] : @"http://www.test.com",
                                          [JNJenkinsAPIJob kNCTJenkinsAPIJobsColourKey] : @"red"};
    
    failedJob = [[JNJenkinsAPIJob alloc] initWithDictionary:failedJobDictionary];
    
    jobUserInfo = @"JobUserInfo";
}

-(void)tearDown {
    failedJobLog = nil;
}

-(void)testThatClassConstructorHelperReturnsValidObject {
    // given
    
    // when
    
    // then
    XCTAssertNotNil(failedJobLog, @"");
}

#pragma mark - hasEntryForJob

-(void)testThatANewlyCreatedFailedJobLogHasNoEntries {
    // given
    
    // when
    
    // then
    XCTAssertFalse([failedJobLog hasEntryForJob:failedJob], @"");
}

-(void)testThatHasEntryForJobReturnsNoForANilJob {
    // given
    
    // when
    
    // then
    XCTAssertFalse([failedJobLog hasEntryForJob:nil], @"");
}

-(void)testThatAddingAJobToTheLogCausesHasEntryForJobToReturnTrue {
    // given
    XCTAssertFalse([failedJobLog hasEntryForJob:failedJob], @"");
    
    // when
    BOOL success = [failedJobLog setUserInfo:jobUserInfo forJob:failedJob];
    
    // then
    XCTAssertTrue(success, @"");
    XCTAssertTrue([failedJobLog hasEntryForJob:failedJob], @"");
}

-(void)testThatAddingAJobTwiceOnlyAddsItOnceToTheLog {
    // given
    BOOL success = [failedJobLog setUserInfo:jobUserInfo forJob:failedJob];
    XCTAssertTrue(success, @"");
    
    // when
    success = [failedJobLog setUserInfo:jobUserInfo forJob:failedJob];
    
    // then
    XCTAssertFalse(success, @"");
    XCTAssertTrue([failedJobLog hasEntryForJob:failedJob], @"");
}

-(void)testThatTryingTologAJobWithANilJobHasNoEffect {
    // given
    
    // when
    BOOL success = [failedJobLog setUserInfo:jobUserInfo forJob:nil];
    
    // then
    XCTAssertFalse(success, @"");
}

-(void)testThatTryingToLogAJobWithANilJobUserInfoHasNoEffect {
    // given
    
    // when
    BOOL success = [failedJobLog setUserInfo:nil forJob:failedJob];
    
    // then
    XCTAssertFalse(success, @"");
}

-(void)testThatTryingToLogAJobWithANilJobAndUserInfoHasNoEffect {
    // given
    
    // when
    BOOL success = [failedJobLog setUserInfo:nil forJob:nil];
    
    // then
    XCTAssertFalse(success, @"");
}

#pragma mark - clearLogForJob

-(void)testThatTryingToClearTheLogForANilJobHasNoEffect {
    // given
    BOOL success = [failedJobLog setUserInfo:jobUserInfo forJob:failedJob];
    XCTAssertTrue(success, @"");

    // when
    NSObject *jobUserInfoReturned = [failedJobLog removeUserInfoForJob:nil];
    
    // then
    XCTAssertNil(jobUserInfoReturned, @"");
}

-(void)testThatClearingTheLogForAnAddedJobRemovesTheEntry {
    // given
    BOOL success = [failedJobLog setUserInfo:jobUserInfo forJob:failedJob];
    XCTAssertTrue(success, @"");
    
    // when
    (void)[failedJobLog removeUserInfoForJob:failedJob];
    
    // then
    BOOL hasObject = [failedJobLog hasEntryForJob:failedJob];
    XCTAssertFalse(hasObject, @"");
}

-(void)testThatClearingTheLogForAnAddedJobRemovesOnlyThatEntry {
    // given
    BOOL success = [failedJobLog setUserInfo:jobUserInfo forJob:failedJob];
    XCTAssertTrue(success, @"");
    
    NSDictionary *failedJobDictionary2 = @{kNCTJenkinsAPIJobsNameKey : @"Name",
                                          kNCTJenkinsAPIJobsURLKey : @"http://www.test2.com",
                                          kNCTJenkinsAPIJobsColourKey : @"red"};
    JNJenkinsAPIJob *failedJob2 = [[JNJenkinsAPIJob alloc] initWithDictionary:failedJobDictionary2];
    NSString *jobUserInfo2 = @"JobUserInfo";
    success = [failedJobLog setUserInfo:jobUserInfo2 forJob:failedJob2];
    XCTAssertTrue(success, @"");
    
    // when
    (void)[failedJobLog removeUserInfoForJob:failedJob];
    
    // then
    BOOL hasObject = [failedJobLog hasEntryForJob:failedJob];
    XCTAssertFalse(hasObject, @"");
    
    hasObject = [failedJobLog hasEntryForJob:failedJob2];
    XCTAssertTrue(hasObject, @"");
}

-(void)testThatClearingTheLogForAnAddedJobReturnsTheRightUserInfo {
    // given
    BOOL success = [failedJobLog setUserInfo:jobUserInfo forJob:failedJob];
    XCTAssertTrue(success, @"");
    
    // when
    NSObject *jobUserInfoReturned = [failedJobLog removeUserInfoForJob:failedJob];
    
    // then
    XCTAssertEqualObjects(jobUserInfoReturned, jobUserInfo, @"");
}

@end
*/
