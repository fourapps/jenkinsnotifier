//
//  Created by Developer on 20/04/2013.
//  Copyright (c) 2013-2015 Four Apps. All rights reserved.
//

@import XCTest;

#import "NSArray+ObjectAtIndexIfNotOutOfBounds.h"

@interface NSArray_ObjectAtIndexIfNotOutOfBoundsTests : XCTestCase
@end


@implementation NSArray_ObjectAtIndexIfNotOutOfBoundsTests

#pragma mark - Empty arrays

-(void)testThatCallingObjectAtIndexIfNotOutOfBoundsReturnsNilForEmptyArrayAndIndex0 {
    // given
    NSArray *SUT = [NSArray array];
    
    // when
    NSObject *returnedObject = [SUT objectAtIndexIfNotOutOfBounds:0];
    
    // then
    XCTAssertNil(returnedObject, @"");
}

-(void)testThatCallingObjectAtIndexIfNotOutOfBoundsReturnsNilForEmptyArrayAndIndexMaxInt {
    // given
    NSArray *SUT = [NSArray array];
    
    // when
    NSObject *returnedObject = [SUT objectAtIndexIfNotOutOfBounds:NSIntegerMax];
    
    // then
    XCTAssertNil(returnedObject, @"");
}

-(void)testThatCallingObjectAtIndexIfNotOutOfBoundsReturnsNilForEmptyArrayAndIndexMinInt {
    // given
    NSArray *SUT = [NSArray array];
    
    // when
    NSObject *returnedObject = [SUT objectAtIndexIfNotOutOfBounds:NSIntegerMin];
    
    // then
    XCTAssertNil(returnedObject, @"");
}

#pragma mark - Valid array tests

-(void)testThatCallingObjectAtIndexIfNotOutOfBoundsReturnsValidObjectForArrayWithOneObjectAndIndex0 {
    // given
    NSString *object0 = @"object0";
    NSArray *SUT = [NSArray arrayWithObject:object0];
    
    // when
    NSObject *returnedObject = [SUT objectAtIndexIfNotOutOfBounds:0];
    
    // then
    XCTAssertEqualObjects(returnedObject, object0, @"");
}

-(void)testThatCallingObjectAtIndexIfNotOutOfBoundsReturnsValidObjectForArrayWithTwoObjectsAndIndex0 {
    // given
    NSString *object0 = @"object0";
    NSString *object1 = @"object1";
    NSArray *SUT = [NSArray arrayWithObjects:object0, object1, nil];
    
    // when
    NSObject *returnedObject = [SUT objectAtIndexIfNotOutOfBounds:0];
    
    // then
    XCTAssertEqualObjects(returnedObject, object0, @"");
}

-(void)testThatCallingObjectAtIndexIfNotOutOfBoundsReturnsValidObjectForArrayWithTwoObjectsAndIndex1 {
    // given
    NSString *object0 = @"object0";
    NSString *object1 = @"object1";
    NSArray *SUT = [NSArray arrayWithObjects:object0, object1, nil];
    
    // when
    NSObject *returnedObject = [SUT objectAtIndexIfNotOutOfBounds:1];
    
    // then
    XCTAssertEqualObjects(returnedObject, object1, @"");
}

#pragma mark - out of bounds tests

-(void)testThatCallingObjectAtIndexIfNotOutOfBoundsReturnsNilForArrayWithOneObjectAndIndex1 {
    // given
    NSString *object0 = @"object0";
    NSArray *SUT = [NSArray arrayWithObject:object0];
    
    // when
    NSObject *returnedObject = [SUT objectAtIndexIfNotOutOfBounds:1];
    
    // then
    XCTAssertNil(returnedObject, @"");
}

-(void)testThatCallingObjectAtIndexIfNotOutOfBoundsReturnsNilForArrayWithOneObjectAndIndexMaxInt {
    // given
    NSString *object0 = @"object0";
    NSArray *SUT = [NSArray arrayWithObject:object0];
    
    // when
    NSObject *returnedObject = [SUT objectAtIndexIfNotOutOfBounds:NSIntegerMax];
    
    // then
    XCTAssertNil(returnedObject, @"");
}

-(void)testThatCallingObjectAtIndexIfNotOutOfBoundsReturnsNilForArrayWithOneObjectAndIndexMinInt {
    // given
    NSString *object0 = @"object0";
    NSArray *SUT = [NSArray arrayWithObject:object0];
    
    // when
    NSObject *returnedObject = [SUT objectAtIndexIfNotOutOfBounds:NSIntegerMin];
    
    // then
    XCTAssertNil(returnedObject, @"");
}

@end
