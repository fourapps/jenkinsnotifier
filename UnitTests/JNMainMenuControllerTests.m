//
//  Created by Developer on 22/09/2013.
//  Copyright (c) 2013 Four Apps. All rights reserved.
//

@import XCTest;

#import <OCMock/OCMock.h>

#import "JNMainMenuController.h"

@interface JNMainMenuController (PrivateMemberAccessForTesting)
@property (nonatomic, strong, readonly) NSMenu* menu;
@property (nonatomic, strong, readonly) NSStatusItem* statusItem;
@property (nonatomic, strong, readonly) NSMutableDictionary* jobs;

+(NSAttributedString*)menuEntryWithTitle:(NSString*)URLString
                               timestamp:(NSDate*)timestamp
                           dateFormatter:(NSDateFormatter*)dateFormatter;
@end



@interface JNMainMenuControllerTests : XCTestCase

@end

@implementation JNMainMenuControllerTests {
@private
    NSString *URLString;
    NSDate *fixedDate;
    NSDateFormatter *dateFormatter;
}

-(void)setUp{
    URLString = @"http://www.myurl.com";
    
    fixedDate = [NSDate dateWithTimeIntervalSinceReferenceDate:0];
    
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
}

-(void)tearDown
{
    URLString = nil;
    fixedDate = nil;
}

#pragma mark -

-(void)testThatJobsContainerIsAvailableWhenMenuControllerIsCreated
{
    // given
    JNMainMenuController *menuController = [[JNMainMenuController alloc] init];
    
    // when
    
    // then
    XCTAssertNotNil([menuController jobs], @"");
}

#pragma mark -

static NSString *URLString = @"http://www.myurl.com";

-(void)testThatServerMenuEntryFromURLStringTimestampDateFormatterReturnsValidString
{
    // given
    
    // when
    NSAttributedString *attributedString = [JNMainMenuController menuEntryWithTitle:URLString
                                                                          timestamp:fixedDate
                                                                      dateFormatter:dateFormatter];
    
    // then
    NSString *expectedString = @"http://www.myurl.com\n2001/01/01, 00:00";
    NSString *actualString = [attributedString string];
    XCTAssertEqualObjects(expectedString, actualString, @"");
}

-(void)testThatServerMenuEntryFromURLStringTimestampDateFormatterReturnsValidStringWithANilURL
{
    // given
    URLString = nil;
    
    // when
    NSAttributedString *attributedString = [JNMainMenuController menuEntryWithTitle:URLString
                                                                          timestamp:fixedDate
                                                                      dateFormatter:dateFormatter];
    
    // then
    NSString *expectedString = @"2001/01/01, 00:00";
    NSString *actualString = [attributedString string];
    XCTAssertEqualObjects(expectedString, actualString, @"");
}

-(void)testThatServerMenuEntryFromURLStringTimestampDateFormatterReturnsValidStringWithANilURLAndTimestamp
{
    // given
    URLString = nil;
    fixedDate = nil;
    
    // when
    NSAttributedString *attributedString = [JNMainMenuController menuEntryWithTitle:URLString
                                                                          timestamp:fixedDate
                                                                      dateFormatter:dateFormatter];
    
    // then
    NSString *expectedString = @"";
    NSString *actualString = [attributedString string];
    XCTAssertEqualObjects(expectedString, actualString, @"");
}

-(void)testThatServerMenuEntryFromURLStringTimestampDateFormatterReturnsValidStringWithAllNilArguments
{
    // given
    URLString = nil;
    fixedDate = nil;
    dateFormatter = nil;
    
    // when
    NSAttributedString *attributedString = [JNMainMenuController menuEntryWithTitle:URLString
                                                                          timestamp:fixedDate
                                                                      dateFormatter:dateFormatter];
    
    // then
    NSString *expectedString = @"";
    NSString *actualString = [attributedString string];
    XCTAssertEqualObjects(expectedString, actualString, @"");
}


#pragma mark -

#pragma message("we need to crack this with an async extention to ocunit")

//-(void)testThatCallingUpdateMenuForServerURLErrorRecordsTheErrorAgainstTheServer
//{
//    // given
//    JNMainMenuController *menuController = [[JNMainMenuController alloc] init];
//
//    id mockError = [OCMockObject mockForClass:[NSError class]];
//    [[[mockError expect] andReturn:@"My Error Message"] localizedDescription];
//
//    // when
//    [menuController updateMenuForServerURL:@"http://www.myurl.com"
//                                     error:mockError];
//
//
//    NSDate *loopUntil = [NSDate dateWithTimeIntervalSinceNow:10];
//    while ([loopUntil timeIntervalSinceNow] > 0) {
//        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:loopUntil];
//    }
//
//    // then
//    //XCTAssertEqual([[menuController jobs] count], (NSUInteger)1, @"");
//
//    [mockError verify];
//}

@end
