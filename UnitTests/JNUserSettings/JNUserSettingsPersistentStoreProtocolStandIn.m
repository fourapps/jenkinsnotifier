//
//  Created by Developer on 02/04/2013.
//  Copyright (c) 2013 Four Apps. All rights reserved.
//

#import "JNUserSettingsPersistentStoreProtocolStandIn.h"

@interface JNUserSettingsPersistentStoreProtocolStandIn ()
@property (nonatomic, strong, readonly) NSMutableDictionary *backingStore;
@end

@implementation JNUserSettingsPersistentStoreProtocolStandIn

- (id)objectForKey:(NSString *)defaultName
{
    return [[self backingStore] objectForKey:defaultName];
}

- (void)setObject:(id)value forKey:(NSString *)defaultName
{
    [[self backingStore] setObject:value forKey:defaultName];
}

- (void)removeObjectForKey:(NSString *)defaultName
{
    [[self backingStore] removeObjectForKey:defaultName];
}

- (NSArray *)arrayForKey:(NSString *)defaultName
{
    return [[self backingStore] objectForKey:defaultName];
}

#pragma mark - 

-(id)init
{
    self = [super init];
    
    if(self) {
        _backingStore = [[NSMutableDictionary alloc] init];
    }
    
    return self;
}

#pragma mark - 

+(instancetype)userSettingsPersistentStore
{
    return [[JNUserSettingsPersistentStoreProtocolStandIn alloc] init];
}

@end
