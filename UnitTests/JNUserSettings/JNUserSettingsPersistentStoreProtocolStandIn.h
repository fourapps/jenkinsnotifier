//
//  Created by Developer on 02/04/2013.
//  Copyright (c) 2013 Four Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "JNUserSettingsPersistentStoreProtocol.h"

@interface JNUserSettingsPersistentStoreProtocolStandIn : NSObject <JNUserSettingsPersistentStoreProtocol>

+(instancetype)userSettingsPersistentStore;

@end
