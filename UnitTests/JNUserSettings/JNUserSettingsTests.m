//
//  Created by Michael May on 17/02/2013.
//  Copyright (c) 2013 Four Apps. All rights reserved.
//

@import XCTest;

#import <OCMock/OCMock.h>

#import "JNUserSettings.h"
#import "JNUserSettingsPersistentStoreProtocolStandIn.h"

#import "JNJenkinsServerEntries.h"
#import "JNJenkinsServerEntry.h"

@interface JNUserSettingsTests : XCTestCase
@end

@interface JNUserSettings ()
-(void)reset;
@end


@implementation JNUserSettingsTests {
@private
    NSObject<JNUserSettingsPersistentStoreProtocol> *userDefaults;
    JNUserSettings *userSettings;
}

-(void)setUp
{
    [super setUp];
    
    userDefaults = [JNUserSettingsPersistentStoreProtocolStandIn userSettingsPersistentStore];
    userSettings = [[JNUserSettings alloc] initWithUserSettingsStore:userDefaults];
}

-(void)tearDown
{
    [userSettings reset];
    
    userSettings = nil;
    userDefaults = nil;
}

#pragma mark - default values

-(void)testThatByDefaultShouldUseNotificationsIsNO
{
    // given
    [userSettings reset];
    
    // when
    
    // then
    XCTAssertTrue([userSettings shouldUseNotifications] == NO, @"");
}

-(void)testThatByDefaultNotifyOnFailedJobsOnlyIsYES
{
    // given
    
    // when
    BOOL notifyOnFailedJobsOnly = [userSettings notifyOnFailedJobsOnly];
    
    // then
    XCTAssertTrue(notifyOnFailedJobsOnly, @"");
}

-(void)testThatByDefaultServerListIsEmpty {
    // given
    
    // when
    [userSettings reset];
    
    // then
    XCTAssertTrue([userSettings totalNumberOfServerEntries] == 0, @"");
}

-(void)testThatByDefaultServerListEntry0IsEmpty {
    // given
    
    // when
    [userSettings reset];
    
    // then
    XCTAssertNil([userSettings serverEntryAtIndex:0], @"");
}

-(void)testThatNumberOfJenkinsPollURLEntriesReturnsSameAsCountOfServerURLListForDefaultSettings {
    // given
    
    // when
    [userSettings reset];
    
    // then
    XCTAssertTrue([userSettings totalNumberOfServerEntries] == [[userSettings allServerEntries] count], @"");
    
}

#pragma mark - getter/setter tests

-(JNJenkinsServerEntries*)serverEntryArrayFromServerURLArray:(NSArray*)serverURLStrings {
    JNJenkinsServerEntries *serverArray = [JNJenkinsServerEntries serverEntries];
    
    for(NSString *serverURL in serverURLStrings) {
        [serverArray addServerWithURLString:serverURL];
    }

    return serverArray;
}

-(void)isNumberOfJenkinsPollURLEntriesReturnsSameAsCountOfServerURLListForServerArray:(NSArray*)serverURLStrings {
    // given
    
    // when
    JNJenkinsServerEntries *serverArray = [self serverEntryArrayFromServerURLArray:serverURLStrings];
    [userSettings setServerEntries:serverArray];
    
    // then
    XCTAssertTrue([userSettings totalNumberOfServerEntries] == [serverArray count], @"");
    XCTAssertTrue([userSettings totalNumberOfServerEntries] == [[userSettings allServerEntries] count], @"");
}


-(void)testThatNumberOfJenkinsPollURLEntriesReturnsSameAsCountOfServerURLListForZeroEntries {
    // given
    
    // when
    NSArray *serverURLStrings = @[];
    [self isNumberOfJenkinsPollURLEntriesReturnsSameAsCountOfServerURLListForServerArray:serverURLStrings];
}


-(void)testThatNumberOfJenkinsPollURLEntriesReturnsSameAsCountOfServerURLListForOneEntry {
    // given
    
    // when
    NSArray *serverURLStrings = @[@"http://www.test.url"];
    [self isNumberOfJenkinsPollURLEntriesReturnsSameAsCountOfServerURLListForServerArray:serverURLStrings];
}

-(void)testThatNumberOfJenkinsPollURLEntriesReturnsSameAsCountOfServerURLListForTwoEntries {
    // given
    
    // when
    NSArray *serverURLStrings = @[@"http://www.test.url", @"http://www.test2.url"];
    [self isNumberOfJenkinsPollURLEntriesReturnsSameAsCountOfServerURLListForServerArray:serverURLStrings];
}

-(void)testThatSettingServerEntriesToNilReturnsEmptyServerList {
    // given
    NSArray *serverURLStrings = @[@"http://www.test.url", @"http://www.test2.url"];
    JNJenkinsServerEntries *serverEntries = [self serverEntryArrayFromServerURLArray:serverURLStrings];
    
    // when
    [userSettings setServerEntries:serverEntries];
    [userSettings setServerEntries:nil];
    
    // then
    XCTAssertNotNil([userSettings allServerEntries], @"");
    XCTAssertTrue([userSettings totalNumberOfServerEntries] == 0, @"");
    XCTAssertTrue([[userSettings allServerEntries] isKindOfClass:[JNJenkinsServerEntries class]], @"");
}

#pragma mark - 

-(void)testThatUsingJenkinsPollURLAtIndexAfterSettingReturnsSetValue {
    // given
    JNJenkinsServerEntries *serverEntries = [self serverEntryArrayFromServerURLArray:@[@"http://www.test.url", @"http://www.test2.url"]];
    
    // when
    [userSettings setServerEntries:serverEntries];
    
    // then
    NSObject *entry0 = [userSettings serverEntryAtIndex:0];
    
    XCTAssertNotNil(entry0, @"");
    XCTAssertTrue([[serverEntries entryAtIndex:0] isEqual:entry0], @"");
//    XCTAssertEqual([serverURLStrings objectAtIndex:1], [userSettings jenkinsPollURLAtIndex:1], @"");
}

#pragma mark - JenkinsPollServerSeconds

-(void)testThatSettingPollServerSecondsReturnsTheSameValue {
    // given
    NSInteger pollPeriodSecondsToUse = 60;
    
    // when
    [userSettings setJenkinsPollPeriodSeconds:pollPeriodSecondsToUse];
    
    // then
    XCTAssertTrue([userSettings jenkinsPollPeriodSeconds] == pollPeriodSecondsToUse, @"");
}

-(void)testThatSettingPollServerSecondsToNegativeReturnsZero {
    // given
    NSInteger pollPeriodSecondsToUse = -60;
    
    // when
    [userSettings setJenkinsPollPeriodSeconds:pollPeriodSecondsToUse];
    
    // then
    XCTAssertTrue([userSettings jenkinsPollPeriodSeconds] == 0, @"");
}


-(void)testThatSettingShouldUseNotificationsToYESThenReturnsYESInGetter {
    // given
    BOOL shouldUseNotificationsSetting = YES;
    
    // when
    [userSettings setShouldUseNotifications:shouldUseNotificationsSetting];
    
    // then
    XCTAssertTrue([userSettings shouldUseNotifications] == shouldUseNotificationsSetting, @"");
}

-(void)testThatSettingShouldUseNotificationsToNOThenReturnsNOInGetter {
    // given
    BOOL shouldUseNotificationsSetting = NO;
    
    // when
    [userSettings setShouldUseNotifications:shouldUseNotificationsSetting];
    
    // then
    XCTAssertTrue([userSettings shouldUseNotifications] == shouldUseNotificationsSetting, @"");
}

-(void)testThatSettingShouldUseNotificationsToNonBoolThenReturnsYESInGetter {
    // given
    NSInteger shouldUseNotificationsSetting = -1;
    
    // when
    [userSettings setShouldUseNotifications:shouldUseNotificationsSetting];
    
    // then
    XCTAssertTrue([userSettings shouldUseNotifications] == YES, @"");
}

#pragma mark - notifyOnFailedJobsOnly

-(void)testThat
{
    // given
#pragma message("TODO")
    // when
    
    // then
}

#pragma mark - reset

-(void)testThatResetResetsAllEntries {
    // given
    const NSInteger kDefaultJenkinsPollPeriodSeconds = (5 * 60);
    
    // when
    [userSettings reset];
    
    // then
    XCTAssertTrue([userSettings jenkinsPollPeriodSeconds] == kDefaultJenkinsPollPeriodSeconds, @"");
    XCTAssertTrue([userSettings totalNumberOfServerEntries] == 0, @"");
    XCTAssertNil([userSettings serverEntryAtIndex:0], @"");
    XCTAssertTrue([userSettings shouldUseNotifications] == NO, @"");
    XCTAssertFalse([userSettings notifyOnFailedJobsOnly], @"");
}



@end
