//
//  Created by Developer on 2014/03/02.
//  Copyright (c) 2014-2015 Michael May. All rights reserved.
//

@import XCTest;

#import <OCMock/OCMock.h>

#import "JNUserSettings+Accessors.h"
#import "JNUserSettingsPersistentStoreProtocolStandIn.h"

@import XCTest;

@interface JNUserSettings (PrivateMethodsExposedForTesting)
-(void)reset;
@end

@interface JNUserSettings_AccessorsTests : XCTestCase
@property (nonatomic, strong, readonly) JNUserSettings *userSettings;
@end

@implementation JNUserSettings_AccessorsTests

NSString * const JNUserSettings_AccessorsTestsKey = @"AnyKey";

-(void)setUp
{
    [super setUp];
    
    JNUserSettingsPersistentStoreProtocolStandIn *persistentStore = [JNUserSettingsPersistentStoreProtocolStandIn userSettingsPersistentStore];
    _userSettings = [[JNUserSettings alloc] initWithUserSettingsStore:persistentStore];
}

-(void)tearDown
{
    [[self userSettings] reset];
    [super tearDown];
}

#pragma mark - hasEntryForKey:

-(void)testThatHasEntryForKeyWithNilKeyReturnsNO
{
    // given
    id key = nil;
    
    // when
    BOOL hasEntry = [[self userSettings] hasEntryForKey:key];
    
    // then
    XCTAssertFalse(hasEntry, @"");
}

-(void)testThatHasEntryForKeyWithEmptyKeyReturnsNO
{
    // given
    id key = [[NSObject alloc] init];
    
    // when
    BOOL hasEntry = [[self userSettings] hasEntryForKey:key];
    
    // then
    XCTAssertFalse(hasEntry, @"");
}

-(void)testThatAnEmptyUserSettingsHasNoEntryForKey
{
    // given
    
    // when
    BOOL hasEntry = [[self userSettings] hasEntryForKey:JNUserSettings_AccessorsTestsKey];
    
    // then
    XCTAssertFalse(hasEntry, @"");
}

-(void)testThatHasEntryForKeyReturnsYESWhenKeyExists
{
    // given
    [[self userSettings] setBool:YES forKey:JNUserSettings_AccessorsTestsKey];
    
    // when
    BOOL hasEntry = [[self userSettings] hasEntryForKey:JNUserSettings_AccessorsTestsKey];
    
    // then
    XCTAssertTrue(hasEntry, @"");
}

-(void)testThatHasEntryForKeyReturnsNOForDifferentKey
{
    // given
    NSString *differentKey = [JNUserSettings_AccessorsTestsKey stringByAppendingString:@"Diff"];
    [[self userSettings] setBool:YES forKey:differentKey];
    
    // when
    BOOL hasEntry = [[self userSettings] hasEntryForKey:JNUserSettings_AccessorsTestsKey];
    
    // then
    XCTAssertFalse(hasEntry, @"");
}

#pragma mark - setBool:forKey

-(void)testThatSettingBoolForNilKeyHasNoEffect
{
    // given
    NSString *key = nil;
    
    // when
    [[self userSettings] setBool:YES forKey:key];
    
    // then
    BOOL hasEntry = [[self userSettings] hasEntryForKey:key];
    XCTAssertFalse(hasEntry, @"");
}

-(void)testSettingBoolWithEmptyKey
{
    // given
    NSString *key = [[NSString alloc] init];
    
    // when
    [[self userSettings] setBool:YES forKey:key];
    
    // then
    BOOL hasEntry = [[self userSettings] hasEntryForKey:key];
    XCTAssertTrue(hasEntry, @"");
}

-(void)testSettingBoolForKey
{
    // given
    NSString *key = @"Key";
    
    // when
    [[self userSettings] setBool:YES forKey:key];
    
    // then
    BOOL hasEntry = [[self userSettings] hasEntryForKey:key];
    XCTAssertTrue(hasEntry, @"");
}

#pragma mark - bool:forKey:

-(void)testThatBoolForKeyReturnsNoForNilKey
{
    // given
    NSString *nilKey = nil;
    
    // when
    BOOL value = [[self userSettings] boolForKey:nilKey];
    
    // then
    XCTAssertFalse(value, @"");
}

-(void)testThatBoolForKeyReturnsNoForEmptyKey
{
    // given
    NSString *nilKey = @"";
    
    // when
    BOOL value = [[self userSettings] boolForKey:nilKey];
    
    // then
    XCTAssertFalse(value, @"");
}

-(void)testThatBoolForKeyReturnsNoForUnknownKey
{
    // given
    NSString *unknownKey = @"UnknownKey";
    
    // when
    BOOL value = [[self userSettings] boolForKey:unknownKey];
    
    // then
    XCTAssertFalse(value, @"");
}

-(void)testBoolForKey
{
    // given
    NSString *key = @"UnknownKey";
    BOOL keyValue = YES;
    [[self userSettings] setBool:keyValue forKey:key];
    
    // when
    BOOL value = [[self userSettings] boolForKey:key];
    
    // then
    XCTAssertEqual(value, keyValue, @"");
}



@end
