//
//  Created by Michael May on 2015/12/08.
//  Copyright © 2015 Michael May. All rights reserved.
//

import XCTest
import Foundation

import JenkinsNotifier

class JNJenkinsAPIJobTests : XCTestCase {
    private var jobDescription : NSMutableDictionary = [:]
    
    private let jobName = "Jenkins"
    private let jobURL = "http://www.jenkinsci.org"
    
    override func setUp() {
        self.jobDescription = [
            JNJenkinsAPIJob.kNCTJenkinsAPIJobsNameKey : self.jobName ,
            JNJenkinsAPIJob.kNCTJenkinsAPIJobsURLKey : self.jobURL,
            JNJenkinsAPIJob.kNCTJenkinsAPIJobsColourKey : "red" ]
        
        super.setUp()
    }
    
    override func tearDown() {
        self.jobDescription = [:]
        
        super.tearDown()
    }
    
    private func failedJob() -> JNJenkinsAPIJob {
        return JNJenkinsAPIJob.init(dictionary: jobDescription as [NSObject : AnyObject])
    }

    private func failedAnimeJob() -> JNJenkinsAPIJob {
        self.jobDescription[JNJenkinsAPIJob.kNCTJenkinsAPIJobsColourKey] = "red_anime"
        
        return JNJenkinsAPIJob.init(dictionary: jobDescription as [NSObject : AnyObject])
    }
    
    private func successJob() -> JNJenkinsAPIJob {
        self.jobDescription[JNJenkinsAPIJob.kNCTJenkinsAPIJobsColourKey] = "blue"
        
        return JNJenkinsAPIJob.init(dictionary: jobDescription as [NSObject : AnyObject])
    }

    private func successAnimeJob() -> JNJenkinsAPIJob {
        self.jobDescription[JNJenkinsAPIJob.kNCTJenkinsAPIJobsColourKey] = "blue_anime"
        
        return JNJenkinsAPIJob.init(dictionary: jobDescription as [NSObject : AnyObject])
    }

    func testThatAJobInitedWithANilDictionaryReturnsAnEmptyObject() {
        // given
        
        // when
        let emptyJob = JNJenkinsAPIJob.init()
        
        // then
        XCTAssertNotNil(emptyJob, "")
        XCTAssertEqual(emptyJob.name, "")
        XCTAssertEqual(emptyJob.URL, "")
        XCTAssertEqual(emptyJob.status, JNJenkinsAPIJobStates.JNJenkinsAPIJobStateUnknown, "")
        XCTAssertFalse(emptyJob.hasFailedStatus(), "")
        XCTAssertTrue(emptyJob.hasSuccessStatus(), "")
    }

    func testThatAJobInitedWithAValidDictionaryIsValid() {
        // given
        
        // when
        let failedJob = self.failedJob()
        
        // then
        XCTAssertNotNil(failedJob)
    }

    func testThatAJobInitedWithAValidDictionaryHasTheJobNameFromTheDictionary() {
        // given
        
        // when
        let failedJob = self.failedJob()
        
        // then
        XCTAssertEqual(failedJob.name, self.jobName)
    }

    func testThatAJobInitedWithAValidDictionaryHasTheJobURLFromTheDictionary() {
        // given
        
        // when
        let failedJob = self.failedJob()
        
        // then
        XCTAssertEqual(failedJob.URL, self.jobURL)
    }

    func testThatAJobInitedWithAValidFailDictionaryReturnsYesForHasFailedStatus() {
        // given
        
        // when
        let failedJob = self.failedJob()
        
        // then
        XCTAssertTrue(failedJob.hasFailedStatus())
        XCTAssertFalse(failedJob.hasSuccessStatus())
    }

    func testThatAJobInitedWithAValidFailDictionaryReturnsFailedStatus() {
        // given
        
        // when
        let failedJob = self.failedJob()
        
        // then
        XCTAssertEqual(failedJob.status, JNJenkinsAPIJobStates.JNJenkinsAPIJobStateFailed)
    }

    func testThatAJobInitedWithAValidSuccessDictionaryReturnsYesForHasSuccessStatus() {
        // given
        
        // when
        let successJob = self.successJob()
        
        // then
        XCTAssertTrue(successJob.hasSuccessStatus())
        XCTAssertFalse(successJob.hasFailedStatus())
    }

    func testThatAJobInitedWithAValidSuccessDictionaryReturnsSuccessStatus() {
        // given
        
        // when
        let successJob = self.successJob()
        
        // then
        XCTAssertEqual(successJob.status, JNJenkinsAPIJobStates.JNJenkinsAPIJobStateSuccess);
    }

    func testThatAJobInitedWithAValidSuccessAnimeDictionaryReturnsSuccessStatus() {
        // given
        
        // when
        let successJob = self.successAnimeJob()
        
        // then
        XCTAssertEqual(successJob.status, JNJenkinsAPIJobStates.JNJenkinsAPIJobStateInProgress)
    }

    func testThatAJobInitedWithAValidFailedAnimeDictionaryReturnsSuccessStatus() {
        // given
        
        // when
        let successJob = self.failedAnimeJob()

        // then
        XCTAssertEqual(successJob.status, JNJenkinsAPIJobStates.JNJenkinsAPIJobStateInProgress)
    }

    func testThatAJobInitedWithAnEmptyStatusDictionaryReturnsThatState() {
        // given
        self.jobDescription[JNJenkinsAPIJob.kNCTJenkinsAPIJobsColourKey] = ""
        
        // when
        let successJob = JNJenkinsAPIJob.init(dictionary: self.jobDescription as [NSObject : AnyObject])
        
        // then
        XCTAssertEqual(successJob.status, JNJenkinsAPIJobStates.JNJenkinsAPIJobStateUnknown)
    }

    func testThatAJobInitedWithADisabledStatusDictionaryReturnsThatState() {
        // given
        self.jobDescription[JNJenkinsAPIJob.kNCTJenkinsAPIJobsColourKey] = "disabled"
        
        // when
        let successJob = JNJenkinsAPIJob.init(dictionary: self.jobDescription as [NSObject : AnyObject])
        
        // then
        XCTAssertEqual(successJob.status, JNJenkinsAPIJobStates.JNJenkinsAPIJobStateDisabled)
    }

    func testThatAJobInitedWithAnUnstableStatusDictionaryReturnsThatState() {
        // given
        self.jobDescription[JNJenkinsAPIJob.kNCTJenkinsAPIJobsColourKey] = "yellow"
        
        // when
        let successJob = JNJenkinsAPIJob.init(dictionary: self.jobDescription as [NSObject : AnyObject])
        
        // then
        XCTAssertEqual(successJob.status, JNJenkinsAPIJobStates.JNJenkinsAPIJobStateUnstable)
    }

    func testThatAJobInitedWithANotBuiltStatusDictionaryReturnsThatState() {
        // given
        self.jobDescription[JNJenkinsAPIJob.kNCTJenkinsAPIJobsColourKey] = "not_built"
        
        // when
        let successJob = JNJenkinsAPIJob.init(dictionary: self.jobDescription as [NSObject : AnyObject])
        
        // then
        XCTAssertEqual(successJob.status, JNJenkinsAPIJobStates.JNJenkinsAPIJobStateNotBuilt)
    }

    func testThatAJobInitedWithAGreyStatusDictionaryReturnsThatState() {
        // given
        self.jobDescription[JNJenkinsAPIJob.kNCTJenkinsAPIJobsColourKey] = "grey"

        // when
        let successJob = JNJenkinsAPIJob.init(dictionary: self.jobDescription as [NSObject : AnyObject])
        
        // then
        XCTAssertEqual(successJob.status, JNJenkinsAPIJobStates.JNJenkinsAPIJobStatePending)
    }

    func testThatAJobInitedWithAnAbortedStatusDictionaryReturnsThatState() {
        // given
        self.jobDescription[JNJenkinsAPIJob.kNCTJenkinsAPIJobsColourKey] = "aborted"
        
        // when
        let successJob = JNJenkinsAPIJob.init(dictionary: self.jobDescription as [NSObject : AnyObject])
        
        // then
        XCTAssertEqual(successJob.status, JNJenkinsAPIJobStates.JNJenkinsAPIJobStateAborted)
    }

    func testThatAJobInitedWithAnRedStatusDictionaryReturnsThatState() {
        // given
        self.jobDescription[JNJenkinsAPIJob.kNCTJenkinsAPIJobsColourKey] = "red"
        
        // when
        let successJob = JNJenkinsAPIJob.init(dictionary: self.jobDescription as [NSObject : AnyObject])
        
        // then
        XCTAssertEqual(successJob.status, JNJenkinsAPIJobStates.JNJenkinsAPIJobStateFailed)
    }

    func testThatAJobInitedWithAnRedAnimeStatusDictionaryReturnsThatState() {
        // given
        self.jobDescription[JNJenkinsAPIJob.kNCTJenkinsAPIJobsColourKey] = "red_anime"
        
        // when
        let successJob = JNJenkinsAPIJob.init(dictionary: self.jobDescription as [NSObject : AnyObject])
        
        // then
        XCTAssertEqual(successJob.status, JNJenkinsAPIJobStates.JNJenkinsAPIJobStateInProgress)
    }

    func testThatAJobInitedWithAGreyAnimeStatusDictionaryReturnsThatState() {
        // given
        self.jobDescription[JNJenkinsAPIJob.kNCTJenkinsAPIJobsColourKey] = "grey_anime"
        
        // when
        let successJob = JNJenkinsAPIJob.init(dictionary: self.jobDescription as [NSObject : AnyObject])
        
        // then
        XCTAssertEqual(successJob.status, JNJenkinsAPIJobStates.JNJenkinsAPIJobStateInProgress)
    }
    
    func testThatAJobInitedWithAYellowAnimeStatusDictionaryReturnsThatState() {
        // given
        self.jobDescription[JNJenkinsAPIJob.kNCTJenkinsAPIJobsColourKey] = "yellow_anime"
        
        // when
        let successJob = JNJenkinsAPIJob.init(dictionary: self.jobDescription as [NSObject : AnyObject])
        
        // then
        XCTAssertEqual(successJob.status, JNJenkinsAPIJobStates.JNJenkinsAPIJobStateInProgress)
    }

    func testThatAJobInitedWithAnAbortedAnimeStatusDictionaryReturnsThatState() {
        // given
        self.jobDescription[JNJenkinsAPIJob.kNCTJenkinsAPIJobsColourKey] = "aborted_anime"
        
        // when
        let successJob = JNJenkinsAPIJob.init(dictionary: self.jobDescription as [NSObject : AnyObject])
        
        // then
        XCTAssertEqual(successJob.status, JNJenkinsAPIJobStates.JNJenkinsAPIJobStateInProgress)
    }

    func testThatASuccessJobHasSuccessStatus() {
        // given
        let successJob = self.successJob()
        
        // when
        let hasSuccessStatus = successJob.hasSuccessStatus()
        
        // then
        XCTAssertTrue(hasSuccessStatus)
    }

    func testThatAFailedJobDoesNotHaveASuccessStatus() {
        // given
        let failedJob = self.failedJob()
        
        // when
        let hasSuccessStatus = failedJob.hasSuccessStatus()
        
        // then
        XCTAssertFalse(hasSuccessStatus)
    }

    func testThatAFailedJobHasAFailedStatus() {
        // given
        let failedJob = self.failedJob()

        // when
        let hasFailedStatus = failedJob.hasFailedStatus()
        
        // then
        XCTAssertTrue(hasFailedStatus)
    }

    func testThatASuccessJobDoesNotHaveAFailedStatus() {
        // given
        let successJob = self.successJob()
        
        // when
        let hasFailedStatus = successJob.hasFailedStatus()
        
        // then
        XCTAssertFalse(hasFailedStatus)
    }
    
    func testThatHasADifferentStatusReturnsNoForFailedStatusWithFailedJob() {
        // given
        let failedJob = self.failedJob()
        
        // when
        let hasDifferentStatus = failedJob.hasDifferentStatus(JNJenkinsAPIJobStates.JNJenkinsAPIJobStateFailed)
        
        // then
        XCTAssertFalse(hasDifferentStatus)
    }

    func testThatHasADifferentStatusReturnsYesForSuccessStatusWithFailedJob() {
        // given
        let failedJob = self.failedJob()

        // when
        let hasDifferentStatus = failedJob.hasDifferentStatus(JNJenkinsAPIJobStates.JNJenkinsAPIJobStateSuccess)
        
        // then
        XCTAssertTrue(hasDifferentStatus)
    }

    func testThatHasADifferentStatusReturnsYesForPendingStatusWithFailedJob() {
        // given
        let failedJob = self.failedJob()

        // when
        let hasDifferentStatus = failedJob.hasDifferentStatus(JNJenkinsAPIJobStates.JNJenkinsAPIJobStatePending)
        
        // then
        XCTAssertTrue(hasDifferentStatus)
    }

    private func checkJobStateFromJobStateStringReturnsCorrectState(stateString : String?, expectedState : JNJenkinsAPIJobStates) {
        // given
        
        // when
        let state = JNJenkinsAPIJobStates.init(jobStateString: stateString)
        
        // then
        XCTAssertEqual(state, expectedState)
    }

    func testThatJobStateForJobStateStringReturnsUnknownForNil() {
        // given
        
        // when
        
        // then
        self.checkJobStateFromJobStateStringReturnsCorrectState(nil, expectedState: JNJenkinsAPIJobStates.JNJenkinsAPIJobStateUnknown)
    }

    func testThatJobStateForJobStateStringReturnsUnknownForEmpty() {
        // given
        
        // when
        
        // then
        self.checkJobStateFromJobStateStringReturnsCorrectState("", expectedState: JNJenkinsAPIJobStates.JNJenkinsAPIJobStateUnknown)
    }

    func testThatJobStateForJobStateStringReturnsUnknownForHelloWorld() {
        // given
        
        // when
        
        // then
        self.checkJobStateFromJobStateStringReturnsCorrectState("HelloWorld", expectedState: JNJenkinsAPIJobStates.JNJenkinsAPIJobStateUnknown)
    }

    func testThatJobStateForJobStateStringReturnsCorrectStateForRed() {
        // given
        
        // when
        
        // then
        self.checkJobStateFromJobStateStringReturnsCorrectState("red", expectedState: JNJenkinsAPIJobStates.JNJenkinsAPIJobStateFailed)
    }

    func testThatJobStateForJobStateStringReturnsCorrectStateForYellow() {
        // given
        
        // when
        
        // then
        self.checkJobStateFromJobStateStringReturnsCorrectState("yellow", expectedState: JNJenkinsAPIJobStates.JNJenkinsAPIJobStateUnstable)
    }

    func testThatJobStateForJobStateStringReturnsCorrectStateForBlue() {
        // given
        
        // when
        
        // then
        self.checkJobStateFromJobStateStringReturnsCorrectState("blue", expectedState: JNJenkinsAPIJobStates.JNJenkinsAPIJobStateSuccess)
    }
}
