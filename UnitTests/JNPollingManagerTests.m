//
//  Created by Developer on 27/05/2013.
//  Copyright (c) 2013 Four Apps. All rights reserved.
//

/*
@import XCTest;

#import <OCMock/OCMock.h>

#import "JNPollingManager.h"
#import "JNJenkinsJobsLog.h"
#import "JNUserSettings.h"
#import "JNServerFilters.h"

#import "JNJobNotificationDelegate.h"


@interface JNPollingManagerTests : XCTestCase
@end

@implementation JNPollingManagerTests {
@private
    JNPollingManager *pollingManager;
    id mockFailedJobLog;
}

-(void)setUp {
    id mockMenuUpdateDelegate = [OCMockObject mockForProtocol:@protocol(JNMenuUpdateDelegate)];
    
    id mockJobNotificationsDelegate = [OCMockObject mockForProtocol:@protocol(JNJobNotificationDelegate)];
    [[mockJobNotificationsDelegate stub] deliverNotificationForJob:OCMOCK_ANY];
    [[mockJobNotificationsDelegate stub] clearNotification:OCMOCK_ANY];
    
    id mockUserSettings = [OCMockObject mockForClass:[JNUserSettings class]];
    [[[mockUserSettings stub] andReturnValue:@YES] shouldUseNotifications];
    
    id mockServerFilters = [OCMockObject niceMockForClass:[JNServerFilters class]];
    
    pollingManager = [JNPollingManager pollingManagerWithMenuUpdateDelegate:mockMenuUpdateDelegate
                                                    jobNotificationDelegate:mockJobNotificationsDelegate
                                                               userSettings:mockUserSettings
                                                              serverFilters:mockServerFilters];
    
    mockFailedJobLog = [OCMockObject mockForClass:[JNJenkinsJobsLog class]];
    [pollingManager setJobStatusLog:mockFailedJobLog];
}

-(void)tearDown {
    pollingManager = nil;
    mockFailedJobLog = nil;
}

-(JNJenkinsAPIJob *)createAFailedJobWhichMustBeUnique:(BOOL)unique {
    NSDictionary *jobDescriptionDictionary = nil;
    
    if(unique) {
        u_int32_t randomNumber = arc4random();
        
        jobDescriptionDictionary = @{kNCTJenkinsAPIJobsNameKey : @"Name",
                                kNCTJenkinsAPIJobsURLKey : [NSString stringWithFormat:@"http://www.test%d.com",randomNumber],
                                kNCTJenkinsAPIJobsColourKey : @"red"};
    } else {
        jobDescriptionDictionary = @{kNCTJenkinsAPIJobsNameKey : @"Name",
                                kNCTJenkinsAPIJobsURLKey : @"http://www.test.com",
                                kNCTJenkinsAPIJobsColourKey : @"red"};
    }
    
    JNJenkinsAPIJob *failedJob = [[JNJenkinsAPIJob alloc] initWithDictionary:jobDescriptionDictionary];
    
    return failedJob;
}

-(JNJenkinsAPIJob *)createASuccessJobWhichMustBeUnique:(BOOL)unique {
    NSDictionary *jobDescriptionDictionary = nil;
    
    if(unique) {
        u_int32_t randomNumber = arc4random();
        
        jobDescriptionDictionary = @{kNCTJenkinsAPIJobsNameKey : @"Name",
                                              kNCTJenkinsAPIJobsURLKey : [NSString stringWithFormat:@"http://www.test%d.com",randomNumber],
                                              kNCTJenkinsAPIJobsColourKey : @"green"};        
    } else {
        jobDescriptionDictionary = @{kNCTJenkinsAPIJobsNameKey : @"Name",
                                              kNCTJenkinsAPIJobsURLKey : @"http://www.test.com",
                                              kNCTJenkinsAPIJobsColourKey : @"green"};
    }
    
    JNJenkinsAPIJob *failedJob = [[JNJenkinsAPIJob alloc] initWithDictionary:jobDescriptionDictionary];
    
    return failedJob;
}

#pragma mark - 

-(void)testThatCallingLogAndDeliverNotificationsForJobsWithNilArrayHasNoEffect {
    // given
    NSArray *jobEntriesToLog = nil;
    
    // then
    
    // then
    // we expect the mock object to throw an exception for any calls made to it with no stub/expect
    XCTAssertNoThrow([pollingManager logAndDeliverNotificationsForFailedJobs:jobEntriesToLog], @"");
}

-(void)testThatCallingLogAndDeliverNotificationsForJobsWithEmptyArrayHasNoEffect {
    // given
    NSArray *jobEntriesToLog = [NSArray array];
    
    // then
    
    // then
    // we expect the mock object to throw an exception for any calls made to it with no stub/expect
    XCTAssertNoThrow([pollingManager logAndDeliverNotificationsForFailedJobs:jobEntriesToLog], @"");
}

-(void)testThatCallingLogAndDeliverNotificationsForSingleSuccessJobHasNoEffect {
    // given
    JNJenkinsAPIJob *job = [self createASuccessJobWhichMustBeUnique:NO];
    [[[mockFailedJobLog expect] andReturnValue:@NO] hasEntryForJob:job];
    
    // then
    NSArray *jobEntriesToLog = @[job];
    [pollingManager logAndDeliverNotificationsForFailedJobs:jobEntriesToLog];
    
    // then
    [mockFailedJobLog verify];
}

-(void)testThatCallingLogAndDeliverNotificationsForSameSuccessJobHasNoEffect {
    // given
    JNJenkinsAPIJob *job = [self createASuccessJobWhichMustBeUnique:NO];
    [[[mockFailedJobLog expect] andReturnValue:@NO] hasEntryForJob:job];

    JNJenkinsAPIJob *jobCopy = [self createASuccessJobWhichMustBeUnique:NO];
    [[[mockFailedJobLog expect] andReturnValue:@NO] hasEntryForJob:jobCopy];
    
    // then
    NSArray *jobEntriesToLog = @[job, jobCopy];
    [pollingManager logAndDeliverNotificationsForFailedJobs:jobEntriesToLog];
    
    // then
    [mockFailedJobLog verify];
}

-(void)testThatCallingLogAndDeliverNotificationsForDifferentSuccessJobsHasNoEffect {
    // given
    JNJenkinsAPIJob *job1 = [self createASuccessJobWhichMustBeUnique:YES];
    [[[mockFailedJobLog expect] andReturnValue:@NO] hasEntryForJob:job1];
    
    JNJenkinsAPIJob *job2 = [self createASuccessJobWhichMustBeUnique:YES];
    [[[mockFailedJobLog expect] andReturnValue:@NO] hasEntryForJob:job2];
    
    // then
    NSArray *jobEntriesToLog = @[job1, job2];
    [pollingManager logAndDeliverNotificationsForFailedJobs:jobEntriesToLog];
    
    // then
    [mockFailedJobLog verify];
}

-(void)testThatCallingLogAndDeliverNotificationsForSingleFailJobLogsThatJob {
    // given
    BOOL hasAnEntry = NO;
    [[[mockFailedJobLog expect] andReturnValue:OCMOCK_VALUE(hasAnEntry)] hasEntryForJob:OCMOCK_ANY];
    
    BOOL successReturn = YES;
    [[[mockFailedJobLog expect] andReturnValue:OCMOCK_VALUE(successReturn)] setUserInfo:OCMOCK_ANY forJob:OCMOCK_ANY];
    
    JNJenkinsAPIJob *job = [self createAFailedJobWhichMustBeUnique:NO];
    NSArray *jobEntriesToLog = @[job];
    
    // then
    [pollingManager logAndDeliverNotificationsForFailedJobs:jobEntriesToLog];
    
    // then
    [mockFailedJobLog verify];
}

-(void)testThatCallingLogAndDeliverNotificationsForSameFailJobMoreThanOnceOnlyLogsThatJobOnce {
    // given
    JNJenkinsAPIJob *job = [self createAFailedJobWhichMustBeUnique:NO];
    [[[mockFailedJobLog expect] andReturnValue:@NO] hasEntryForJob:job];
    [[[mockFailedJobLog expect] andReturnValue:@YES] setUserInfo:OCMOCK_ANY forJob:job];

    JNJenkinsAPIJob *jobCopy = [self createAFailedJobWhichMustBeUnique:NO];
    [[[mockFailedJobLog expect] andReturnValue:@YES] hasEntryForJob:jobCopy];
    
    // then
    NSArray *jobEntriesToLog = @[job, jobCopy];
    [pollingManager logAndDeliverNotificationsForFailedJobs:jobEntriesToLog];
    
    // then
    [mockFailedJobLog verify];
}

-(void)testThatCallingLogAndDeliverNotificationsForTwoDifferentFailJobsLogsBoth {
    // given    
    JNJenkinsAPIJob *job = [self createAFailedJobWhichMustBeUnique:YES];
    [[[mockFailedJobLog expect] andReturnValue:@NO] hasEntryForJob:job];
    [[[mockFailedJobLog expect] andReturnValue:@YES] setUserInfo:OCMOCK_ANY forJob:job];
    
    JNJenkinsAPIJob *job2 = [self createAFailedJobWhichMustBeUnique:YES];
    [[[mockFailedJobLog expect] andReturnValue:@NO] hasEntryForJob:job2];
    [[[mockFailedJobLog expect] andReturnValue:@YES] setUserInfo:OCMOCK_ANY forJob:job2];
    
    // then
    NSArray *jobEntriesToLog = @[job, job2];
    [pollingManager logAndDeliverNotificationsForFailedJobs:jobEntriesToLog];
    
    // then
    [mockFailedJobLog verify];
}

-(void)testThatCallingLogAndDeliverNotificationsForFailSuccessFailOfSameJobLogsBothFails {
    // given
    JNJenkinsAPIJob *jobWithFailStatus = [self createAFailedJobWhichMustBeUnique:NO];
    [[[mockFailedJobLog expect] andReturnValue:@NO] hasEntryForJob:jobWithFailStatus];
    [[[mockFailedJobLog expect] andReturnValue:@YES] setUserInfo:OCMOCK_ANY forJob:jobWithFailStatus];
    
    JNJenkinsAPIJob *jobWithSuccessStatus = [self createASuccessJobWhichMustBeUnique:NO];
    [[[mockFailedJobLog expect] andReturnValue:@YES] hasEntryForJob:jobWithSuccessStatus];
    [[[mockFailedJobLog expect] andReturn:nil] removeUserInfoForJob:jobWithSuccessStatus];

    JNJenkinsAPIJob *jobWithFailStatusAgain = [self createAFailedJobWhichMustBeUnique:NO];
    [[[mockFailedJobLog expect] andReturnValue:@NO] hasEntryForJob:jobWithFailStatusAgain];
    [[[mockFailedJobLog expect] andReturnValue:@YES] setUserInfo:OCMOCK_ANY forJob:jobWithFailStatusAgain];
    
    // then
    NSArray *jobEntriesToLog = @[jobWithFailStatus, jobWithSuccessStatus, jobWithFailStatusAgain];
    [pollingManager logAndDeliverNotificationsForFailedJobs:jobEntriesToLog];
    
    // then
    [mockFailedJobLog verify];
}

@end
*/