//
//  Created by Developer on 2014/03/05.
//  Copyright (c) 2014 Four Apps. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>

#import "JNJenkinsJobsLog.h"

@interface JNJenkinsJobsLogTests : SenTestCase
@property (nonatomic, strong, readonly) JNJenkinsJobsLog *log;
@end

@implementation JNJenkinsJobsLogTests

- (void)setUp
{
    [super setUp];

    _log = [JNJenkinsJobsLog jobsLog];
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class. 
    [super tearDown];
}

#pragma mark - utility

-(JNJenkinsAPIJob*)newJob
{
    NSURL *jobURL = [NSURL URLWithString:@"http://www.hello.com"];
    NSDictionary *jobDescription = @{kNCTJenkinsAPIJobsURLKey : jobURL};
    JNJenkinsAPIJob *job = [JNJenkinsAPIJob jenkinsAPIJobWithDictionary:jobDescription];
    
    return job;
}

-(JNJenkinsAPIJob*)newJobTwo
{
    NSURL *jobURL = [NSURL URLWithString:@"http://www.helloworld.com"];
    NSDictionary *jobDescription = @{kNCTJenkinsAPIJobsURLKey : jobURL};
    JNJenkinsAPIJob *job = [JNJenkinsAPIJob jenkinsAPIJobWithDictionary:jobDescription];
    
    return job;
}

-(JNJenkinsAPIJob*)insertNewJob
{
    JNJenkinsAPIJob *job = [self newJob];
    NSDictionary *userInfo = @{};
    
    BOOL success = [[self log] setUserInfo:userInfo forJob:job];
    STAssertTrue(success, @"");
    
    return job;
}

#pragma mark - factory

-(void)testThatFactoryReturnsNonNilObject
{
    // given
    // when
    // then
    STAssertNotNil([self log], @"");
}

#pragma mark - hasEntryForJob:

-(void)testThatHasEntryForJobReturnsNoForNilJob
{
    // given
    JNJenkinsAPIJob *job = nil;
    
    // when
    BOOL hasEntry = [[self log] hasEntryForJob:job];
    
    // then
    STAssertFalse(hasEntry, @"");
}

-(void)testThatHasEntryForJobReturnsNoForEmptyJob
{
    // given
    JNJenkinsAPIJob *job = [JNJenkinsAPIJob jenkinsAPIJobWithDictionary:@{}];
    
    // when
    BOOL hasEntry = [[self log] hasEntryForJob:job];
    
    // then
    STAssertFalse(hasEntry, @"");
}

-(void)testThatHasUserEntryForJobReturnsYesForPreExistingJob
{
    // given
    JNJenkinsAPIJob *job = [self insertNewJob];
    
    // when
    BOOL hasEntry = [[self log] hasEntryForJob:job];
    
    // then
    STAssertTrue(hasEntry, @"");
}

#pragma mark - setUserInfo:forJob:

-(void)testSetUserInfoForAJob
{
    // given
    JNJenkinsAPIJob *job = [self newJob];
    NSDictionary *userInfo = @{};
    
    // when
    BOOL success = [[self log] setUserInfo:userInfo forJob:job];
    
    // then
    STAssertTrue(success, @"");
}

-(void)testSetUserInfoForAJobFailsSecondTimeAroundWithURLKeyIsTheSame
{
    // given
    JNJenkinsAPIJob *job = [self newJob];
    NSDictionary *userInfo = @{};
    BOOL success = [[self log] setUserInfo:userInfo forJob:job];
    STAssertTrue(success, @"");
    
    // when
    JNJenkinsAPIJob *jobTwo = [self newJob];
    NSDictionary *userInfoTwo = @{};
    success = [[self log] setUserInfo:userInfoTwo forJob:jobTwo];
    
    // then
    STAssertFalse(success, @"");
}

-(void)testSetUserInfoForAJobSucceedsForTwoDifferentJobsWithDifferentKeys
{
    // given
    JNJenkinsAPIJob *job = [self newJob];
    NSDictionary *userInfo = @{};
    BOOL success = [[self log] setUserInfo:userInfo forJob:job];
    STAssertTrue(success, @"");
    
    // when
    JNJenkinsAPIJob *jobTwo = [self newJobTwo];
    NSDictionary *userInfoTwo = @{};
    success = [[self log] setUserInfo:userInfoTwo forJob:jobTwo];
    
    // then
    STAssertTrue(success, @"");
}

#pragma mark - removeUserInfoForJob::

-(void)testRemoveUserInfoForJobWithPreExistingJob
{
    // given
    JNJenkinsAPIJob *job = [self newJob];
    NSDictionary *userInfo = @{};
    BOOL success = [[self log] setUserInfo:userInfo forJob:job];
    STAssertTrue(success, @"");
    STAssertTrue([[self log] hasEntryForJob:job], @"");
    
    // when
    NSObject *returnedUserInfo = [[self log] removeUserInfoForJob:job];
    
    // then
    STAssertNotNil(returnedUserInfo, @"");
}

-(void)testRemoveUserInfoForJobWithNoExistingJob
{
    // given
    JNJenkinsAPIJob *job = [self newJob];
    STAssertFalse([[self log] hasEntryForJob:job], @"");
    
    // when
    NSObject *returnedUserInfo = [[self log] removeUserInfoForJob:job];
    
    // then
    STAssertNil(returnedUserInfo, @"");
}

-(void)testRemoveUserInfoForJobWithPreExistingJobsButNotTheJob
{
    // given
    NSDictionary *userInfo = @{};
    JNJenkinsAPIJob *jobOne = [self newJob];
    [[self log] setUserInfo:userInfo forJob:jobOne];
    JNJenkinsAPIJob *jobTwo = [self newJobTwo];
    
    // when
    NSObject *returnedUserInfo = [[self log] removeUserInfoForJob:jobTwo];
    
    // then
    STAssertNil(returnedUserInfo, @"");
}

-(void)testRemoveUserInfoForJobWithNilJob
{
    // given
    JNJenkinsAPIJob *job = nil;
    
    // when
    NSObject *returnedUserInfo = [[self log] removeUserInfoForJob:job];
    
    // then
    STAssertNil(returnedUserInfo, @"");
}

#pragma mark - userInfoForJob:

-(void)testThatUserInfoForJobReturnsNilForNilJob
{
    // given
    JNJenkinsAPIJob *job = nil;
    
    // when
    NSObject *userInfo = [[self log] userInfoForJob:job];
    
    // then
    STAssertNil(userInfo, @"");
}

-(void)testThatUserInfoForJobReturnsNilForUnknownJob
{
    // given
    JNJenkinsAPIJob *job = [self newJob];
    
    // when
    NSObject *userInfo = [[self log] userInfoForJob:job];
    
    // then
    STAssertNil(userInfo, @"");
}

-(void)testThatUserInfoForJobReturnsSameObjectAsSet
{
    // given
    JNJenkinsAPIJob *job = [self newJob];
    NSDictionary *userInfoSet = @{};
    BOOL setSuccess = [[self log] setUserInfo:userInfoSet forJob:job];
    STAssertTrue(setSuccess, @"");
    
    // when
    NSObject *userInfoRetrieved = [[self log] userInfoForJob:job];
    
    // then
    STAssertEqualObjects(userInfoSet, userInfoRetrieved, @"");
}

@end
