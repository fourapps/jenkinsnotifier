//
//  Created by Developer on 09/03/2013.
//  Copyright (c) 2013-2015 Four Apps. All rights reserved.
//

/*
@import XCTest;

#import "JNJenkinsAPIJob.h"
#import "JNJobStatusMappers.h"

@interface JNJobStatusMappersTests : XCTestCase
@end

@implementation JNJobStatusMappersTests

#pragma mark - iconImageNameForJobState:

-(void)doTestState:(JNJenkinsAPIJobStates)state matchesImageName:(NSString*)imageNameExpected {
    NSString *imageNameReturned = [JNJobStatusMappers iconImageNameForJobState:state];
    
    XCTAssertTrue([imageNameReturned isEqualToString:imageNameExpected], @"state: %d, imageNameExpected: %@", state, imageNameExpected);
}

-(void)testThatAnUnknownStateReturnsStatusBarImageName {
    [self doTestState:JNJenkinsAPIJobStateUnknown matchesImageName:@"menu-statusbar16"];
}

-(void)testThatAnAbortedStateReturnsDisabledImageName {
    [self doTestState:JNJenkinsAPIJobStateAborted matchesImageName:@"build-disabled16"];
}

-(void)testThatAnPendingStateReturnsDisabledImageName {
    [self doTestState:JNJenkinsAPIJobStatePending matchesImageName:@"build-ongoing16"];
}

-(void)testThatANotBuiltStateReturnsDisabledImageName {
    [self doTestState:JNJenkinsAPIJobStateNotBuilt matchesImageName:@"build-disabled16"];
}

-(void)testThatADisabledStateReturnsDisabledImageName {
    [self doTestState:JNJenkinsAPIJobStateDisabled matchesImageName:@"build-disabled16"];
}

-(void)testThatASuccessStateReturnsSuccessBarImageName {
    [self doTestState:JNJenkinsAPIJobStateSuccess matchesImageName:@"build-success16"];
}

-(void)testThatAFailedStateReturnsFailedImageName {
    [self doTestState:JNJenkinsAPIJobStateFailed matchesImageName:@"build-failed16"];
}

-(void)testThatAnInProgressStateReturnsOnGoingImageName {
    [self doTestState:JNJenkinsAPIJobStateInProgress matchesImageName:@"build-ongoing16"];
}

-(void)testThatAnUnstableStateReturnsUnstableImageName {
    [self doTestState:JNJenkinsAPIJobStateUnstable matchesImageName:@"build-unstable16"];
}

-(void)testThatAConnectionFailedStateReturnsConnectonFailedImageName {
    [self doTestState:JNJenkinsAPIJobStateConnectionFailure matchesImageName:@"connection-failed16"];
}

-(void)testThatAnInvalidStateReturnsNilImageName {
    short invalidState = 65000;
    
    NSString *imageNameReturned = [JNJobStatusMappers iconImageNameForJobState:invalidState];
    
    XCTAssertNil(imageNameReturned, @"");
}

#pragma mark - jobImageimageForState:

// See ApplicationUnitTests

#pragma mark -  toolTipTextForJobState:

-(void)testThatToolTipTextForJobStateReturnsAStringForEveryState
{
    // given
    // when
    // then
    for(JNJenkinsAPIJobStates state = 0; state < JNJenkinsAPIJobStateCount; state++) {
        NSString *toolTip = [JNJobStatusMappers toolTipTextForJobState:state];
        XCTAssertNotNil(toolTip, @"");
        XCTAssertFalse([toolTip isEqualToString:@""], @"");
    }
}

#pragma mark -  notificiationTextForJobState:

-(void)testThatNotificationTextForJobStateReturnsAStringForEveryState
{
    // given
    // when
    // then
    for(JNJenkinsAPIJobStates state = 0; state < JNJenkinsAPIJobStateCount; state++) {
        NSString *notificationText = [JNJobStatusMappers notificiationTextForJobState:state];
        XCTAssertNotNil(notificationText, @"");
        XCTAssertFalse([notificationText isEqualToString:@""], @"");
    }
}

#pragma mark - mostImportantState

-(void)testThatMostImportantStateReturnsFailedWhenChoiceIsFailedOrSuccess {
    // given
    JNJenkinsAPIJobStates state1 = JNJenkinsAPIJobStateFailed;
    JNJenkinsAPIJobStates state2 = JNJenkinsAPIJobStateSuccess;
    
    // when
    JNJenkinsAPIJobStates mostImportantStateResult = mostImportantState(state1, state2);
    
    // then
    XCTAssertEqual(mostImportantStateResult, JNJenkinsAPIJobStateFailed, @"");
}

-(void)testThatMostImportantStateReturnsSuccessWhenChoiceIsSuccessOfSuccess {
    // given
    JNJenkinsAPIJobStates state1 = JNJenkinsAPIJobStateSuccess;
    JNJenkinsAPIJobStates state2 = JNJenkinsAPIJobStateSuccess;
    
    // when
    JNJenkinsAPIJobStates mostImportantStateResult = mostImportantState(state1, state2);
    
    // then
    XCTAssertEqual(mostImportantStateResult, JNJenkinsAPIJobStateSuccess, @"");
}

-(void)testThatMostImportantStateReturnsSuccessWhenChoiceIsSuccessOrUnknown {
    // given
    JNJenkinsAPIJobStates state1 = JNJenkinsAPIJobStateUnknown;
    JNJenkinsAPIJobStates state2 = JNJenkinsAPIJobStateSuccess;
    
    // when
    JNJenkinsAPIJobStates mostImportantStateResult = mostImportantState(state1, state2);
    
    // then
    XCTAssertEqual(mostImportantStateResult, state2, @"");
}

-(void)testThatMostImportantStateReturnsConnectionFailureWhenChoiceIsConnectionFailureOrJobFailure {
    // given
    JNJenkinsAPIJobStates state1 = JNJenkinsAPIJobStateConnectionFailure;
    JNJenkinsAPIJobStates state2 = JNJenkinsAPIJobStateFailed;
    
    // when
    JNJenkinsAPIJobStates mostImportantStateResult = mostImportantState(state1, state2);
    
    // then
    XCTAssertEqual(mostImportantStateResult, state1, @"");
}

@end
*/
