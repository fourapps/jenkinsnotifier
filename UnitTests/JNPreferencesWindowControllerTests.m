//
//  Created by Developer on 09/03/2013.
//  Copyright (c) 2013-2015 Four Apps. All rights reserved.
//

@import XCTest;

#import <OCMock/OCMock.h>

#import "JNPreferencesWindowController.h"
#import "JNUserSettings.h"

#import "JNJenkinsServerEntry.h"
#import "JNJenkinsServerEntries.h"
#import "JNServerFilters.h"

@interface JNPreferencesWindowController (ExposedForUnitTestingOnly)
-(IBAction)userDidChangeNotifyOnlyOnFailedJobsOnlySwitch:(id)sender;
-(IBAction)userDidSelectTableActionSegmentedControllerButton:(id)sender;
@property (nonatomic, weak, readonly) IBOutlet NSButton *notifyOnFailedJobsOnlyCheckBoxButton;
@end

@interface JNPreferencesWindowControllerTests : XCTestCase
@end


@implementation JNPreferencesWindowControllerTests {
@private
    JNPreferencesWindowController *SUT;
    NSUInteger mockJenkingPollPeriodReturn;
    BOOL shouldUseNotifications;
    BOOL notifyOnFailedJobsOnly;
    
    JNJenkinsServerEntries *serverList;
    id serverFilterList;
    
    id mockSegmentedControlWithAdd;
    id mockSegmentedControlWithSubtract;
}

-(void)setUp {
    mockJenkingPollPeriodReturn = 60;
    shouldUseNotifications = NO;
    notifyOnFailedJobsOnly = YES;
    
    serverList = [JNJenkinsServerEntries serverEntries];
    
    serverFilterList = [OCMockObject niceMockForClass:[JNServerFilters class]];
    
    SUT = [JNPreferencesWindowController preferencesWindowWithPollPeriod:mockJenkingPollPeriodReturn
                                                        useNotifications:shouldUseNotifications
                                                  notifyOnFailedJobsOnly:notifyOnFailedJobsOnly
                                                           serverURLList:serverList
                                                           serverFilters:serverFilterList];
    
    mockSegmentedControlWithAdd = [OCMockObject mockForClass:[NSSegmentedControl class]];
    NSInteger returnValueForAddButton = 0;
    [[[mockSegmentedControlWithAdd expect] andReturnValue:OCMOCK_VALUE(returnValueForAddButton)] selectedSegment];
    
    mockSegmentedControlWithSubtract = [OCMockObject mockForClass:[NSSegmentedControl class]];
    NSInteger returnValueForSubtractButton = 1;
    [[[mockSegmentedControlWithSubtract expect] andReturnValue:OCMOCK_VALUE(returnValueForSubtractButton)] selectedSegment];    
}

-(void)testThatWhenThePreferencesWindowIsDisplayedItGrabsTheLatestValuesFromTheUserSettings
{
    // given
    
    // when
    [SUT loadWindow];
    [SUT windowDidLoad];
    
    // then
    XCTAssertEqual([SUT pollPeriodSeconds], mockJenkingPollPeriodReturn, @"");
    XCTAssertEqual([SUT useNotifications], shouldUseNotifications, @"");
    XCTAssertTrue([[SUT serverEntries] isEqualTo:serverList], @"");
}

-(void)testThatWhenUpdatePollPeriodSecondsIsCalledTheNumberOfSecondsIsIncreased {
    // given
    
    // when
    [SUT updatePollPeriodSeconds:(mockJenkingPollPeriodReturn+1)];
    
    // then
    XCTAssertEqual([SUT pollPeriodSeconds], mockJenkingPollPeriodReturn+1, @"");
}

-(void)testThatWhenUserDidSelectTableActionSegmentedControllerButtonIsCalledWithAddActionNewEntryIsMadeInServerURLList {
    NSUInteger serverURListCount = [[SUT serverEntries] count];
    
    // given

    // when
    [SUT userDidSelectTableActionSegmentedControllerButton:mockSegmentedControlWithAdd];
    
    // then
    [mockSegmentedControlWithAdd verify];
    
    JNJenkinsServerEntries *serverURLList = [SUT serverEntries];
    ++serverURListCount;
    XCTAssertEqual([serverURLList count], serverURListCount, @"");
}

-(void)testThatWhenUserDidSelectTableActionSegmentedControllerButtonIsCalledWithAddActionValidEntryIsMadeInServerURLList {
    // given
    
    // when
    [SUT userDidSelectTableActionSegmentedControllerButton:mockSegmentedControlWithAdd];
    
    // then
    [mockSegmentedControlWithAdd verify];
    
    JNJenkinsServerEntries *serverURLList = [SUT serverEntries];
    JNJenkinsServerEntry *serverEntry = [serverURLList lastObject];
    
    XCTAssertTrue([[serverEntry URLString] isEqualToString:NSLocalizedString(@"PollURLPlaceholder", @"")], @"");
}

-(void)testThatWhenUserDidSelectTableActionSegmentedControllerButtonIsCalledTwiceWithAddActionOnlyOneNewEntryIsMadeInServerURLList {
    // given
    NSUInteger serverURListCount = [[SUT serverEntries] count];
    NSInteger returnValueForAddButton = 0;
    [[[mockSegmentedControlWithAdd expect] andReturnValue:OCMOCK_VALUE(returnValueForAddButton)] selectedSegment];

    // when
    [SUT userDidSelectTableActionSegmentedControllerButton:mockSegmentedControlWithAdd];
    ++serverURListCount;
    // we don't accept duplicates anymore
    [SUT userDidSelectTableActionSegmentedControllerButton:mockSegmentedControlWithAdd];
    
    // then
    [mockSegmentedControlWithAdd verify];
    
    JNJenkinsServerEntries *serverURLList = [SUT serverEntries];
    XCTAssertEqual([serverURLList count], serverURListCount, @"");
}


-(void)testThatWhenUserDidSelectTableActionSegmentedControllerButtonIsCalledOnAnExistingServerListAnotherNewEntryIsAdded
{
    // given
    NSUInteger serverURListCount = [[SUT serverEntries] count];
//    NSInteger returnValueForAddButton = 0;
//    [[[mockSegmentedControlWithAdd expect] andReturnValue:OCMOCK_VALUE(returnValueForAddButton)] selectedSegment];
    
    [[SUT serverEntries] addServerWithURLString:@"https://myjenkinsserver:8080"];
    ++serverURListCount;
    
    // when
    [SUT userDidSelectTableActionSegmentedControllerButton:mockSegmentedControlWithAdd];
    
    // then
    
    JNJenkinsServerEntries *serverURLList = [SUT serverEntries];
    ++serverURListCount;
    XCTAssertEqual([serverURLList count], serverURListCount, @"");
}

-(void)testThatWhenUserDidSelectTableActionSegmentedControllerButtonIsCalledWithSubtractActionNewEntryIsMadeThenRemovedInServerURLList {
    // given
    JNJenkinsServerEntries *serverEntries = [SUT serverEntries];
    NSUInteger serverURListCount = [serverEntries count];
    
    // when
    [SUT userDidSelectTableActionSegmentedControllerButton:mockSegmentedControlWithAdd];
    ++serverURListCount;
    [SUT userDidSelectTableActionSegmentedControllerButton:mockSegmentedControlWithSubtract];
    --serverURListCount;
    
    // then
    [mockSegmentedControlWithAdd verify];
    
    XCTAssertEqual([serverEntries count], serverURListCount, @"");
}


#pragma mark - debugging apple reject 

-(void)testThatWhenPreferencesWindowIsPassedNilServerURLListOnStartupThatItStillReturnsAnEmptyServerList {
    // given and when
    SUT = [JNPreferencesWindowController preferencesWindowWithPollPeriod:mockJenkingPollPeriodReturn
                                                        useNotifications:shouldUseNotifications
                                                  notifyOnFailedJobsOnly:notifyOnFailedJobsOnly
                                                           serverURLList:nil
                                                           serverFilters:nil];
    
    // then
    JNJenkinsServerEntries *serverEntries = [SUT serverEntries];
    XCTAssertNotNil(serverEntries, @"");
}

-(void)testThatWhenPreferencesWindowIsPassedNilServerURLListOnStartupThatAddNewServerStillWorks {
    // given
    NSInteger serverURListCount = [[SUT serverEntries] count];
    
    SUT = [JNPreferencesWindowController preferencesWindowWithPollPeriod:mockJenkingPollPeriodReturn
                                                   useNotifications:shouldUseNotifications
                                                  notifyOnFailedJobsOnly:notifyOnFailedJobsOnly
                                                      serverURLList:nil
                                                      serverFilters:nil];
    
    // when
    [SUT userDidSelectTableActionSegmentedControllerButton:mockSegmentedControlWithAdd];
    
    // then
    [mockSegmentedControlWithAdd verify];
    
    JNJenkinsServerEntries *serverURLList = [SUT serverEntries];
    ++serverURListCount;
    XCTAssertTrue([serverURLList count] == serverURListCount, @"");
}

#pragma mark - 

-(void)testThatNotifyOnFailedJobsOnlyIsPassedBackCorrectlyFromThePreferencesWindows
{
    // given
    SUT = [JNPreferencesWindowController preferencesWindowWithPollPeriod:mockJenkingPollPeriodReturn
                                                        useNotifications:shouldUseNotifications
                                                  notifyOnFailedJobsOnly:notifyOnFailedJobsOnly
                                                           serverURLList:nil
                                                           serverFilters:nil];

    [SUT loadWindow];
    
    NSButton *notifyOnFailedJobsOnlyButton = [SUT notifyOnFailedJobsOnlyCheckBoxButton];
    [notifyOnFailedJobsOnlyButton setState:NSOnState];
    
    // when
    [SUT userDidChangeNotifyOnlyOnFailedJobsOnlySwitch:nil];
    
    // then
    XCTAssertTrue([SUT notifyOnFailedJobsOnly], @"");
}

@end
