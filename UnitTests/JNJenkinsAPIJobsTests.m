//
//  Created by Developer on 2014/01/19.
//  Copyright (c) 2014 Four Apps. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>

#import "JNJenkinsServerEntry.h"
#import "JNJenkinsAPIJobs.h"

@interface JNJenkinsAPIJobsTests : SenTestCase
@end

@implementation JNJenkinsAPIJobsTests {
@private
    JNJenkinsAPIJobs *jobs;
    JNJenkinsServerEntry *serverEntry;
}

- (void)setUp
{
    [super setUp];
    
    serverEntry = [[JNJenkinsServerEntry alloc] init];
    
    jobs = [JNJenkinsAPIJobs jenkinsAPIJobForServer:serverEntry];
}

#pragma mark - 

-(void)testThatWhenNoAuthDetailsAreSetupThereAreAddedToURLRequests
{
    // given
    
    // when
    NSURLRequest *request = [jobs request];
    
    // then
    STAssertNil([[request allHTTPHeaderFields] objectForKey:@"Authorization"], @"We must support basic auth");
}

-(void)testThatWhenAuthDetailsAreSetupTheyAreAddedToURLRequests
{
    // given
    [serverEntry setUsername:@"testusername"];
    [serverEntry setPassword:@"testpassword"];
    
    jobs = [JNJenkinsAPIJobs jenkinsAPIJobForServer:serverEntry];
    
    // when
    NSURLRequest *request = [jobs request];
    
    // then
    STAssertNotNil([[request allHTTPHeaderFields] objectForKey:@"Authorization"], @"We must support basic auth");
}

#pragma mark - test no caching

-(void)testThatWhenAURLRequestIsCreatedCachingIsDisabled
{
    // given

    // when
    NSURLRequest *request = [jobs request];
    
    // then
    STAssertEquals([request cachePolicy], (NSUInteger)NSURLCacheStorageNotAllowed, @"We must not cache at all");
}

#pragma mark - timeout

-(void)testThatWhenAURLRequestIsCreatedATimeoutIsSet
{
    // given
    
    // when
    NSURLRequest *request = [jobs request];
    
    // then
    STAssertEquals([request timeoutInterval], JNJenkinsAPIJobTimeOut, @"We must always timeout quickly");
}


@end
