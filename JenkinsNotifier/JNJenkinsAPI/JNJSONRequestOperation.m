//
//  Created by Developer on 27/05/2013.
//  Copyright (c) 2013 Four Apps. All rights reserved.
//

#import "MMDebugMacros.h"

#import "JNJSONRequestOperation.h"

@implementation JNJSONRequestOperation

- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]) {
        [super connection:connection willSendRequestForAuthenticationChallenge:challenge];
    } else {
        [[challenge sender] continueWithoutCredentialForAuthenticationChallenge:challenge];
    }
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [super connection:connection didReceiveResponse:response];
    
    DLog(@"connection:didReceiveResponse: %@ (%ld)\n%@", response, (long)[(NSHTTPURLResponse*)response statusCode], [(NSHTTPURLResponse*)response allHeaderFields]);
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [super connection:connection didFailWithError:error];
    
    NSAlert *alert = [NSAlert alertWithError:error];
    [alert runModal];
    
    NSLog(@"connection:didFailWithError: %@", error);
}

-(NSError*)error {
    NSError *error = nil;
    
    if(super.error) {
        NSMutableDictionary *userInfo = [NSMutableDictionary dictionary];
        [userInfo setValue:self.responseString forKey:NSLocalizedRecoverySuggestionErrorKey];
        [userInfo setValue:[self.request URL] forKey:NSURLErrorFailingURLErrorKey];
        
        NSString *errorMessage = [NSHTTPURLResponse localizedStringForStatusCode:[super.response statusCode]];
        [userInfo setValue:errorMessage forKey:NSLocalizedDescriptionKey];
        
        error = [NSError errorWithDomain:@"JNJSONRequestOperationDomain"
                                    code:super.response.statusCode
                                userInfo:userInfo];
    }
    
    return error;
}

@end
