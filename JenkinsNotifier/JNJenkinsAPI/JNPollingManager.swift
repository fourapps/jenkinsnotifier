//
//  Created by Developer on 2015/12/10.
//  Copyright © 2015 Michael May. All rights reserved.
//

import Foundation

@objc public class JNPollingManager: NSObject {
    private let menuUpdateDelegate : JNMenuUpdateDelegate
    private let jobNotificationDelegate : JNJobNotificationDelegate
    private let userSettings : JNUserSettings
    private let serverFilters : JNServerFilters
    private let jobStatusLog : JNJenkinsJobsLog
    private var pollingTimer : NSTimer? = nil
    
    @objc public func beginJenkinsAPIPolling() {
        self.cancelJenkinsAPIPolling()
        
        let jenkinsPollPeriodSeconds : NSTimeInterval = (NSTimeInterval)(self.userSettings.jenkinsPollPeriodSeconds)
        let hasValidPollInterval = (jenkinsPollPeriodSeconds > 0)
        
        let totalNumberOfServerEntries = self.userSettings.totalNumberOfServerEntries()
        let hasServerEntries = (totalNumberOfServerEntries > 0)
        
        if (hasValidPollInterval && hasServerEntries) {
            let pollingTimer = NSTimer(timeInterval: jenkinsPollPeriodSeconds,
                target: self,
                selector: #selector(JNPollingManager.performJenkinsAPICheckNow(_:)),
                userInfo: nil,
                repeats: true)
            
            self.pollingTimer = pollingTimer
            
            NSRunLoop.mainRunLoop().addTimer(pollingTimer, forMode: NSDefaultRunLoopMode)
            
            pollingTimer.fire()
        }
    }
    
    private func logAndDeliverNotificationsForFailedJobs(jobs : [ JNJenkinsAPIJob ]) {
        SDLog("")
        
        if (self.userSettings.shouldUseNotifications) {
            let jobStatusLog = self.jobStatusLog
            let jobNotificationDelegate = self.jobNotificationDelegate
            
            for job in jobs {
                if jobStatusLog.hasEntryForJob(job) {
                    // we have an existing logged job
                    // has the job changed status?
                    if job.hasSuccessStatus() {
                        if let notification = jobStatusLog.removeUserInfoForJob(job) as! NSUserNotification? {
                            jobNotificationDelegate.clearNotification(notification)
                        }
                    }
                } else {
                    if job.hasFailedStatus() {
                        let notification = jobNotificationDelegate.deliverNotificationForJob(job)
                        
                        jobStatusLog.setUserInfo(notification, forJob: job)
                    }
                }
            }
        }
        
        SDLog("exiting")
    }
    
    private func logAndDeliverNotificationsForChangingStatusJobs(jobs : [ JNJenkinsAPIJob]) {
        SDLog("")
        
        if (self.userSettings.shouldUseNotifications) {
            let jobStatusLog = self.jobStatusLog
            let jobNotificationDelegate = self.jobNotificationDelegate
            
            for job in jobs {
                let existingEntryForJobExists = jobStatusLog.hasEntryForJob(job)
                var mustDeliverNotificationForJob = (existingEntryForJobExists == false)
                
                if (existingEntryForJobExists) {
                    if let existingNotification = jobStatusLog.userInfoForJob(job) as? NSUserNotification? {
                        let notificationUserInfo = existingNotification?.userInfo
                        let lastJobStatusRawValue = jobNotificationDelegate.jobStatusFromUserInfo(notificationUserInfo)
                        
                        if let lastJobStatus = JNJenkinsAPIJobStates(rawValue: lastJobStatusRawValue) {
                            mustDeliverNotificationForJob = job.hasDifferentStatus(lastJobStatus)
                        } else {
                            print("found an expected job status: (lastJobStatusRawValue)")
                            mustDeliverNotificationForJob = false
                        }
                        
                        if (mustDeliverNotificationForJob) {
                            jobStatusLog.removeUserInfoForJob(job)
                            jobNotificationDelegate.clearNotification(existingNotification)
                        }
                    }
                }
                
                if (mustDeliverNotificationForJob) {
                    let notification = jobNotificationDelegate.deliverNotificationForJob(job)
                    jobStatusLog.setUserInfo(notification, forJob: job)
                }
            }
        }
    }
    
    // todo: rmeove AnyObject array type
    private func filterJenkinsServer(server : JNJenkinsServerEntry, jobs : [ JNJenkinsAPIJob ]) -> [JNJenkinsAPIJob] {
        guard let predicateString = self.serverFilters.predicateForFilterWithIdentifier(server.predicateIdentifier) else { return jobs }
        guard predicateString.isNotEmpty else { return jobs }
        
        let filterPredicate = NSPredicate.init(format: predicateString)
        let filteredJobsArray = jobs as NSArray
        
        return filteredJobsArray.filteredArrayUsingPredicate(filterPredicate) as! [JNJenkinsAPIJob]
    }
    
    @objc public func cancelJenkinsAPIPolling() {
        self.pollingTimer?.invalidate()
        self.pollingTimer = nil
    }
    
    @objc public func pollServersNow() {
        self.pollingTimer?.fire()
    }
    
    @objc public init(menuUpdateDelegate: JNMenuUpdateDelegate,
        jobNotificationDelegate: JNJobNotificationDelegate,
        userSettings: JNUserSettings,
        serverFilters: JNServerFilters) {
            self.menuUpdateDelegate = menuUpdateDelegate
            self.jobNotificationDelegate = jobNotificationDelegate
            self.userSettings = userSettings
            self.serverFilters = serverFilters
            self.jobStatusLog = JNJenkinsJobsLog()
            
            super.init()
    }
}

extension JNPollingManager {
    @objc private func performJenkinsAPICheckNow(timer : NSTimer) {
        let jenkinsPollEntryCount = self.userSettings.totalNumberOfServerEntries()
        
        if (jenkinsPollEntryCount > 0) {
            let timeout = self.userSettings.serverTimeoutSeconds()
            
            var serverEntry : JNJenkinsServerEntry
            
            for entryIndex in 0..<jenkinsPollEntryCount {
                serverEntry = self.userSettings.serverEntryAtIndex(entryIndex)
                
                let getJobsOperation = JNJenkinsAPIJobs.init(forServer: serverEntry, timeoutSeconds: timeout)
                getJobsOperation.delegate = self
                
                SDLog("scheduling: (getJobsOperation)");
                
                getJobsOperation.addOperation()
            }
        }
        
    }
}

extension JNPollingManager : NCTJenkinsAPIJobDelegate {
    public func APICallToJenkinsServer(server: JNJenkinsServerEntry, didReturnJobs jobs: [JNJenkinsAPIJob]) {
        let filteredJobs = self.filterJenkinsServer(server, jobs: jobs)
        
        self.menuUpdateDelegate.updateMenuForServerURL(server.URLString, jobs: filteredJobs)
        
        if self.userSettings.notifyOnFailedJobsOnly() {
            self.logAndDeliverNotificationsForFailedJobs(filteredJobs)
        } else {
            self.logAndDeliverNotificationsForChangingStatusJobs(filteredJobs)
        }
    }
    
    public func APICallToJenkinsServer(server: JNJenkinsServerEntry, didFailWithError error: NSError) {
        self.menuUpdateDelegate.updateMenuForServerURL(server.URLString, error: error)
    }
}
