//
//  Created by Developer on 2015/12/10.
//  Copyright © 2015 Michael May. All rights reserved.
//

#import <AFNetworking.h>

@interface JNAFHTTPClient : AFHTTPClient

@property (readwrite, nonatomic, strong) NSMutableDictionary *defaultHeaders;

@end
