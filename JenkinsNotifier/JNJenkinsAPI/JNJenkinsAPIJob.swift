//
//  Created by Developer on 2015/12/10.
//  Copyright © 2015 Michael May. All rights reserved.
//

import Foundation

// TODO: rename frpm JNJenkinsAPIJobStateUnknown to Unknown
@objc public enum JNJenkinsAPIJobStates : Int {
    struct JNJenkinsAPIJobColorValues {
        static let JNAPIInProgressStatusSuffix = "_anime"
        
        // RED("red",Messages._BallColor_Failed(), ColorPalette.RED)
        // RED_ANIME("red_anime",Messages._BallColor_InProgress(), ColorPalette.RED)
        static let kNCTJenkinsAPIJobsRedValue = "red"
        static let kNCTJenkinsAPIJobsRedAnimeValue = "red_anime"
        
        // BLUE("blue",Messages._BallColor_Success(), ColorPalette.BLUE)
        // BLUE_ANIME("blue_anime",Messages._BallColor_InProgress(), ColorPalette.BLUE)
        static let kNCTJenkinsAPIJobsBlueValue = "blue"
        static let kNCTJenkinsAPIJobsBlueAnimeValue = "blue_anime"
        
        // DISABLED("grey",Messages._BallColor_Disabled(), ColorPalette.GREY)
        // DISABLED_ANIME("grey_anime",Messages._BallColor_InProgress(), ColorPalette.GREY)
        static let kNCTJenkinsAPIJobsDisabledValue = "disabled"
        static let kNCTJenkinsAPIJobsDisabledAnimeValue = "disabled_anime"
        
        // NOTBUILT("grey",Messages._BallColor_NotBuilt(), ColorPalette.GREY)
        // NOTBUILT_ANIME("grey_anime",Messages._BallColor_InProgress(), ColorPalette.GREY)
        static let kNCTJenkinsAPIJobsNotBuiltValue = "not_built"
        static let kNCTJenkinsAPIJobsNotBuiltAnimeValue = "not_built_anime"
        
        // GREY("grey",Messages._BallColor_Pending(), ColorPalette.GREY)
        // GREY_ANIME("grey_anime",Messages._BallColor_InProgress(), ColorPalette.GREY)
        static let kNCTJenkinsAPIJobsGreyValue = "grey"
        static let kNCTJenkinsAPIJobsGreyAnimeValue = "grey_anime"
        
        // YELLOW("yellow",Messages._BallColor_Unstable(), ColorPalette.YELLOW)
        // YELLOW_ANIME("yellow_anime",Messages._BallColor_InProgress(), ColorPalette.YELLOW)
        static let kNCTJenkinsAPIJobsYellowValue = "yellow"
        static let kNCTJenkinsAPIJobsYellowAnimeValue = "yellow_anime"
        
        // ABORTED("grey",Messages._BallColor_Aborted(), ColorPalette.GREY),
        // ABORTED_ANIME("grey_anime",Messages._BallColor_InProgress(), ColorPalette.GREY)
        static let kNCTJenkinsAPIJobsAbortedValue = "aborted"
        static let kNCTJenkinsAPIJobsAbortedAnimeValue = "aborted_anime"
    }
    
    // the ordering here is very important, they are in priority order
    // so the fail status is more important to show the user than the building
    case JNJenkinsAPIJobStateUnknown
    case JNJenkinsAPIJobStateDisabled
    case JNJenkinsAPIJobStateSuccess
    case JNJenkinsAPIJobStateUnstable
    case JNJenkinsAPIJobStateNotBuilt
    case JNJenkinsAPIJobStatePending
    case JNJenkinsAPIJobStateInProgress
    case JNJenkinsAPIJobStateAborted
    case JNJenkinsAPIJobStateFailed
    // not real statuses from the API but failures before we hit the API
    case JNJenkinsAPIJobStateConnectionFailure
    // used for iteration, not for use
    case JNJenkinsAPIJobStateCount
  
    public init(jobStateString : String?) {
        guard (jobStateString != nil) else { self = JNJenkinsAPIJobStateUnknown ; return }
        
        let map : [String : JNJenkinsAPIJobStates] = [
            JNJenkinsAPIJobColorValues.kNCTJenkinsAPIJobsRedValue : .JNJenkinsAPIJobStateFailed,
            JNJenkinsAPIJobColorValues.kNCTJenkinsAPIJobsRedAnimeValue : .JNJenkinsAPIJobStateInProgress,
            
            JNJenkinsAPIJobColorValues.kNCTJenkinsAPIJobsYellowValue : .JNJenkinsAPIJobStateUnstable,
            JNJenkinsAPIJobColorValues.kNCTJenkinsAPIJobsYellowAnimeValue : .JNJenkinsAPIJobStateInProgress,
            
            JNJenkinsAPIJobColorValues.kNCTJenkinsAPIJobsBlueValue : .JNJenkinsAPIJobStateSuccess,
            JNJenkinsAPIJobColorValues.kNCTJenkinsAPIJobsBlueAnimeValue : .JNJenkinsAPIJobStateInProgress,
            
            JNJenkinsAPIJobColorValues.kNCTJenkinsAPIJobsGreyValue : .JNJenkinsAPIJobStatePending,
            JNJenkinsAPIJobColorValues.kNCTJenkinsAPIJobsGreyAnimeValue : .JNJenkinsAPIJobStateInProgress,
            
            JNJenkinsAPIJobColorValues.kNCTJenkinsAPIJobsDisabledValue : .JNJenkinsAPIJobStateDisabled,
            JNJenkinsAPIJobColorValues.kNCTJenkinsAPIJobsDisabledAnimeValue : .JNJenkinsAPIJobStateInProgress,
            
            JNJenkinsAPIJobColorValues.kNCTJenkinsAPIJobsAbortedValue : .JNJenkinsAPIJobStateAborted,
            JNJenkinsAPIJobColorValues.kNCTJenkinsAPIJobsAbortedAnimeValue : .JNJenkinsAPIJobStateInProgress,
            
            JNJenkinsAPIJobColorValues.kNCTJenkinsAPIJobsNotBuiltValue : .JNJenkinsAPIJobStateNotBuilt,
            JNJenkinsAPIJobColorValues.kNCTJenkinsAPIJobsNotBuiltAnimeValue : .JNJenkinsAPIJobStateInProgress,
        ]
        
        let loweredCaseJobStateString = jobStateString!.lowercaseString
        if let mapped = map[loweredCaseJobStateString] {
            self = mapped
        } else {
            self = .JNJenkinsAPIJobStateUnknown
        }
    }
}

@objc public class JNJenkinsAPIJob : NSObject {
    public static let kNCTJenkinsAPIJobsNameKey = "name"
    public static let kNCTJenkinsAPIJobsURLKey = "url"
    public static let kNCTJenkinsAPIJobsColourKey = "color"

    public let name : String
    public let URL : String
    public let status : JNJenkinsAPIJobStates
    
    private func hasStatus(status : JNJenkinsAPIJobStates) -> Bool {
        return (self.status == status)
    }
    
    public func hasFailedStatus() -> Bool {
        return (self.hasStatus(.JNJenkinsAPIJobStateFailed))
        // return ((rand() % 2) == 0)    // useful for testing
    }
    
    public func hasSuccessStatus() -> Bool {
        return (self.hasFailedStatus() == false)
    }
    
    public func hasDifferentStatus(status : JNJenkinsAPIJobStates) -> Bool {
        return (self.hasStatus(status) == false)
    }
    
    public init(dictionary: [String : AnyObject]) {
        self.name = (dictionary[JNJenkinsAPIJob.kNCTJenkinsAPIJobsNameKey] ?? "") as! String
    
        self.URL = (dictionary[JNJenkinsAPIJob.kNCTJenkinsAPIJobsURLKey] ?? "") as! String
        
        let jobStateString = dictionary[JNJenkinsAPIJob.kNCTJenkinsAPIJobsColourKey] as? String
        self.status = JNJenkinsAPIJobStates(jobStateString: jobStateString)
        
        super.init()
    }
    
    public override convenience init() {
        self.init(dictionary : [:])
    }
}
