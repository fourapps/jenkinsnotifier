//
//  Created by Developer on 2015/12/10.
//  Copyright © 2015-2016 Michael May. All rights reserved.
//

import Foundation

@objc public protocol NCTJenkinsAPIJobDelegate {
    func APICallToJenkinsServer(server: JNJenkinsServerEntry, didReturnJobs jobs: [JNJenkinsAPIJob])
    func APICallToJenkinsServer(server: JNJenkinsServerEntry, didFailWithError error: NSError)
}

@objc public class JNJenkinsAPIJobs : NSObject {
    private let HTTPRequestMethod = "GET"
    private let JenkinsJSONAPIJobsPath = "api/json"
    
    private let HTTPClient : JNAFHTTPClient
    private let server : JNJenkinsServerEntry
    private let timeoutSeconds : NSTimeInterval
    
    public var delegate : NCTJenkinsAPIJobDelegate?    // weak?
    
    private func parseJobJSONObject(jobsArray : [AnyObject]) {
        let jobs = jobsArray.map({ (job : AnyObject) -> AnyObject in
            guard let jobDictionary = job as? [String : AnyObject] else { return NSNull() }
            
            return JNJenkinsAPIJob.init(dictionary: jobDictionary) ?? NSNull()
        }).filter {
            $0.isKindOfClass(JNJenkinsAPIJob)
        } as! [JNJenkinsAPIJob]
        
        self.delegate?.APICallToJenkinsServer(self.server, didReturnJobs:jobs)
    }
    
    public func addOperation() {
        // note: each JSJenkinsAPIJobs object has it's own queue, so this works fine with other objects
        self.HTTPClient.cancelAllHTTPOperationsWithMethod(self.HTTPRequestMethod, path: self.JenkinsJSONAPIJobsPath)

        let request = self.request()

        typealias SuccessBlockClosure = ((NSURLRequest!, NSHTTPURLResponse!, AnyObject!) -> Void)!
        let success : SuccessBlockClosure = { request, response, JSON in
            guard let JSONDictionary = JSON as? [String : AnyObject] else { return } // TODO: report an error
            
            if let jobsArray = JSONDictionary["jobs"] as? [AnyObject] {
                self.parseJobJSONObject(jobsArray)
            }
        }
        
        typealias FailureBlockClosure = ((NSURLRequest!, NSHTTPURLResponse!, NSError!, AnyObject!) -> Void)!
        let failure : FailureBlockClosure = { request, response, error, JSON in
            self.delegate?.APICallToJenkinsServer(self.server, didFailWithError: error)
        }
        
        let requestOperation = JNJSONRequestOperation.init(request: request,
            success: success,
            failure: failure)

        requestOperation.successCallbackQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0x00)
        requestOperation.failureCallbackQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0x00)
        requestOperation.allowsInvalidSSLCertificate = true;

        self.HTTPClient.enqueueHTTPRequestOperation(requestOperation)
    }
    
    public func request() -> NSURLRequest {
        guard let serverURL = self.server.serverURLWithAdditionalPathComponents(self.JenkinsJSONAPIJobsPath) else {
            loggingPrint("serverURL was nil after call to serverURLWithAdditionalPathComponents")
            return NSURLRequest()
        }
        
        let request = NSMutableURLRequest(URL: serverURL,
            cachePolicy: NSURLRequestCachePolicy.ReloadIgnoringCacheData,
            timeoutInterval: self.timeoutSeconds)
        
        // Swift does not like coercing NSMutableDictionary to Dictionary, you need to coerce it to the immutable NSDictionary first
        let allHeaders = (self.HTTPClient.defaultHeaders as NSDictionary) as? [String : String]
        request.allHTTPHeaderFields = allHeaders
        request.HTTPMethod = self.HTTPRequestMethod
        
        return request
    }
    
    public init(forServer server : JNJenkinsServerEntry, timeoutSeconds : NSTimeInterval) {
        let emptyServerURL = NSURL(string: "")
        
        self.HTTPClient = JNAFHTTPClient.init(baseURL: emptyServerURL)
        self.HTTPClient.allowsInvalidSSLCertificate = true
        
        self.server = server
        
        self.timeoutSeconds = timeoutSeconds
        
        if server.hasAuthenticationDetails() {
            self.HTTPClient.setAuthorizationHeaderWithUsername(server.username, password: server.password)
        }
        
        super.init()
    }
}
