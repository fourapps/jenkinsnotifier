//
//  Created by Developer on 2016/10/05.
//  Copyright © 2016 Michael May. All rights reserved.
//

import Foundation

public extension String {
    var isNotEmpty: Bool {
        return !self.isEmpty
    }
}
