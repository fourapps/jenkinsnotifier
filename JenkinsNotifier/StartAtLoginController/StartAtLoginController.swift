//
//  Created by Developer on 2016/02/21.
//  Copyright © 2016 Michael May. All rights reserved.
//

import Foundation
import ServiceManagement

@objc public class StartAtLoginController : NSObject {
    private let bundleIdentifier : String
    
    public init(bundleIdentifier:String) {
        self.bundleIdentifier = bundleIdentifier
        
        self.startAtLogin = self.dynamicType.loadStartAtLoginState()
        
        super.init()
    }
    
    public var startAtLogin : Bool {
        didSet {
            self.dynamicType.updateHelper(self.bundleIdentifier, startAtLoginState: startAtLogin)
            self.dynamicType.saveStartAtloginState(startAtLogin)
        }
    }
    
    private static let StartAtLoginKey = "StartAtLoginKey"
    
    private static func loadStartAtLoginState() -> Bool {
        return NSUserDefaults.standardUserDefaults().boolForKey(StartAtLoginKey)
    }
    
    private static func saveStartAtloginState(startAtLoginState : Bool) {
        NSUserDefaults.standardUserDefaults().setBool(startAtLoginState, forKey: StartAtLoginKey)
    }
    
    private static func updateHelper(bundleIdentifier: String, startAtLoginState : Bool) {
        if SMLoginItemSetEnabled(bundleIdentifier, startAtLoginState) == false {
            NSLog("Error: couldn't \(startAtLoginState ? "add" : "remove") \(bundleIdentifier) app to launch at login item list.")
        } else {
            NSLog("Success: \(startAtLoginState ? "added" : "removed") \(bundleIdentifier) app to launch at login item list.")
        }
    }
}
