//
//  Created by Michael May on 14/10/2012.
//  Copyright (c) 2012 Michael May. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class JNServerFilters;
@class JNJenkinsServerEntries;

@interface JNPreferencesWindowController : NSWindowController <
    NSTableViewDelegate,
    NSTableViewDataSource,
    NSControlTextEditingDelegate,
    NSComboBoxCellDataSource,
    NSComboBoxDelegate,
    NSWindowDelegate>

@property (nonatomic, assign, readonly) NSUInteger pollPeriodSeconds;
@property (nonatomic, assign, readonly) BOOL useNotifications;
@property (nonatomic, assign, readonly) BOOL notifyOnFailedJobsOnly;

@property (nonatomic, strong, readonly) JNJenkinsServerEntries* serverEntries;

@property (nonatomic, assign) id<NSWindowDelegate> delegate;

#pragma mark - Exposed for testing

-(void)updatePollPeriodSeconds:(NSInteger)seconds;

#pragma mark - 

+(instancetype)preferencesWindowWithPollPeriod:(NSUInteger)pollPeriodSeconds
                              useNotifications:(BOOL)useNotifications
                        notifyOnFailedJobsOnly:(BOOL)notifyOnFailedJobsOnly
                                 serverURLList:(JNJenkinsServerEntries*)serverURLList
                                 serverFilters:(JNServerFilters*)serverFilters;

@end
