//
//  Created by Michael May on 14/10/2012.
//  Copyright (c) 2012 Michael May. All rights reserved.
//

#import "JNPreferencesWindowController.h"
#import "MMDebugMacros.h"
#import "JNUserSettings.h"
#import "JNJenkinsServerEntry.h"

#import "JNJenkinsServerEntries.h"
#import "JNServerFilters.h"

#import "NSArray+ObjectAtIndexIfNotOutOfBounds.h"
#import "NSBundle+VersionExtensions.h"

#import "JenkinsNotifier-Swift.h"

@interface JNPreferencesWindowController ()
// data model
@property (nonatomic, assign) BOOL userDidMakeChanges;
@property (nonatomic, assign) NSUInteger pollPeriodSeconds;
@property (nonatomic, assign) BOOL useNotifications;
@property (nonatomic, assign) BOOL notifyOnFailedJobsOnly;

@property (nonatomic, strong) JNJenkinsServerEntries* serverEntries;
@property (nonatomic, strong) JNServerFilters *serverFilters;
@property (nonatomic, strong) StartAtLoginController *startAtLoginController;

// user interface
@property (nonatomic, weak) IBOutlet NSTextField *pollPeriodTextField;
@property (nonatomic, weak) IBOutlet NSButton *useNotificationsCheckBoxButton;
@property (nonatomic, weak) IBOutlet NSButton *notifyOnFailedJobsOnlyCheckBoxButton;

@property (nonatomic, weak) IBOutlet NSTableView *pollServerListTableView;
@property (nonatomic, weak) IBOutlet NSButton *autoDiscoveryButton;

@property (nonatomic, weak) IBOutlet NSTextField *pollPeriodTitle;
@property (nonatomic, weak) IBOutlet NSTextField *pollPeriodGuidanceText;
@property (nonatomic, weak) IBOutlet NSStepper *pollPeriodStepper;

@property (nonatomic, weak) IBOutlet NSTextField *useNotificationsSubtext;

@property (nonatomic, weak) IBOutlet NSSegmentedControl *pollServerURLListActions;

@property (nonatomic, weak) IBOutlet NSTextField *iconCreditText;
@property (nonatomic, weak) IBOutlet NSButton *saveButton;

@property (nonatomic, weak) IBOutlet NSButton *startOnLoginButton;

@end

@implementation JNPreferencesWindowController

#pragma mark - 

+(instancetype)preferencesWindowWithPollPeriod:(NSUInteger)pollPeriodSeconds
                              useNotifications:(BOOL)useNotifications
                        notifyOnFailedJobsOnly:(BOOL)notifyOnFailedJobsOnly
                                 serverURLList:(JNJenkinsServerEntries*)serverEntries
                                 serverFilters:(JNServerFilters*)serverFilters
{
    JNJenkinsServerEntries *entries;
    
    if(serverEntries) {
        entries = [serverEntries mutableCopy];
    } else {
        entries = [JNJenkinsServerEntries serverEntries];
    }
    
    StartAtLoginController *startAtLoginController = [[StartAtLoginController alloc] initWithBundleIdentifier:SharedValues.HelperApplicatioBundleIdentifier];
    
    return [[self alloc] initWithPollPeriod:pollPeriodSeconds
                           useNotifications:useNotifications
                     notifyOnFailedJobsOnly:notifyOnFailedJobsOnly
                              serverURLList:entries
                              serverFilters:serverFilters
                     startAtLoginController:startAtLoginController];
}

#pragma mark -

-(id)initWithPollPeriod:(NSUInteger)pollPeriodSeconds
       useNotifications:(BOOL)useNotifications
 notifyOnFailedJobsOnly:(BOOL)notifyOnFailedJobsOnly
          serverURLList:(JNJenkinsServerEntries*)serverEntries
          serverFilters:(JNServerFilters*)serverFilters
 startAtLoginController:(StartAtLoginController *)startAtLoginController
{
    self = [super initWithWindowNibName:@"JNPreferencesWindowController" owner:self];
    
    if(self) {
        _pollPeriodSeconds = pollPeriodSeconds;
        _useNotifications = useNotifications;
        _notifyOnFailedJobsOnly = notifyOnFailedJobsOnly;

        _serverEntries = serverEntries;
        
        _serverFilters = serverFilters;
        
        _startAtLoginController = startAtLoginController;
        
        NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
        [defaultCenter addObserver:self
                          selector:@selector(serverListTextDidEndEditingWithNotification:)
                              name:NSControlTextDidEndEditingNotification
                            object:nil];
    }
    
    return self;
}

-(void)dealloc {
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];

    [defaultCenter removeObserver:self
                             name:NSControlTextDidEndEditingNotification
                           object:nil];
}

#pragma mark -

typedef NS_ENUM(NSUInteger, JNPreferencesTableColumnIndexes) {
    JNPreferencesTableColumnIndexServerURL = 0,
    JNPreferencesTableColumnIndexUsername = 1,
    JNPreferencesTableColumnIndexPassword = 2,
    JNPreferencesTableColumnIndexFilter = 3
};

-(void)setColumnName:(NSString*)name
             atIndex:(NSInteger)index
{
    __strong NSTableView *pollServerListTableView = [self pollServerListTableView];
    NSArray *tableColumns = [pollServerListTableView tableColumns];
    NSTableColumn *serverColumn = nil;
    id headerCell= nil;
    
    serverColumn = [tableColumns objectAtIndexIfNotOutOfBounds:JNPreferencesTableColumnIndexServerURL];
    headerCell = [serverColumn headerCell];
    [headerCell setStringValue:NSLocalizedString(@"PollURLTitle", @"")];
    [[pollServerListTableView headerView] setNeedsDisplay:YES];
}

-(void)setColumnNames
{
    [self setColumnName:NSLocalizedString(@"PollURLTitle", @"")
                atIndex:JNPreferencesTableColumnIndexServerURL];
    
    [self setColumnName:NSLocalizedString(@"UsernameTitle", @"")
                atIndex:JNPreferencesTableColumnIndexUsername];

    [self setColumnName:NSLocalizedString(@"PasswordTitle", @"")
                atIndex:JNPreferencesTableColumnIndexPassword];

    [self setColumnName:NSLocalizedString(@"PredicateTitle", @"")
                atIndex:JNPreferencesTableColumnIndexFilter];    
}

#pragma mark -

- (void)windowDidLoad {
    [super windowDidLoad];
        
    [self setColumnNames];
    
    __strong NSTextField *pollPeriodTitle = [self pollPeriodTitle];
    [pollPeriodTitle setStringValue:NSLocalizedString(@"PollPeriodTitle", @"")];
    
    __strong NSTextField *pollPeriodGuidanceText = [self pollPeriodGuidanceText];
    [pollPeriodGuidanceText setStringValue:NSLocalizedString(@"PollPeriodGuidanceText", @"")];
        
    __strong NSTextField *useNotificationsSubtext = [self useNotificationsSubtext];
    [useNotificationsSubtext setStringValue:NSLocalizedString(@"UseNotificationsSubtextTitle", @"")];
    
    __strong NSButton *saveButton = [self saveButton];
    [saveButton setTitle:NSLocalizedString(@"DoneButtonText", @"")];
    
    NSInteger pollSeconds = [self pollPeriodSeconds];
    __strong NSTextField *pollPeriodTextField = [self pollPeriodTextField];
    [pollPeriodTextField setIntegerValue:pollSeconds];
    
    __strong NSStepper *pollPeriodStepper = [self pollPeriodStepper];
    [pollPeriodStepper setIntegerValue:pollSeconds];

    {
        __strong NSButton *useNotificationsCheckBoxButton = [self useNotificationsCheckBoxButton];
        [useNotificationsCheckBoxButton setTitle:NSLocalizedString(@"UseNotificationsTitle", @"")];    
        
        BOOL shouldUseNotifications = [self useNotifications];
        [useNotificationsCheckBoxButton setState:(shouldUseNotifications == NO) ? NSOffState : NSOnState];
    }
    
    {
        __strong NSButton *notifyOnFailedJobsOnlyCheckBoxButton = [self notifyOnFailedJobsOnlyCheckBoxButton];
        [notifyOnFailedJobsOnlyCheckBoxButton setTitle:NSLocalizedString(@"NotifyOnlyOnFailureTitle", @"")];

        BOOL notifyOnFailedJobsOnly = [self notifyOnFailedJobsOnly];
        [notifyOnFailedJobsOnlyCheckBoxButton setState:(notifyOnFailedJobsOnly == NO) ? NSOffState : NSOnState];
    }
    
    {
        __strong NSButton *startOnLoginButton = self.startOnLoginButton;
        startOnLoginButton.state = (self.startAtLoginController.startAtLogin) ? NSOnState : NSOffState;
    }
    
    
    NSString *text = NSLocalizedString(@"AboutAppTitle", @"");
    text = [[NSBundle mainBundle] stringWithByReplacingVersionPlaceholders:text];
    
    __strong NSTextField *titleText = [self iconCreditText];
    [titleText setStringValue:text];
    
    [self updateServerURLListActionButtonsForState];
}

- (void)windowWillClose:(NSNotification *)notification
{
    [[self delegate] windowWillClose:notification];
}

#pragma mark - NSTableViewDelegate

-(void)userDidEditServerURLTo:(NSString*)URLString atRow:(NSInteger)row
{
    if(URLString == nil || [URLString length] == 0) return;
       
    JNJenkinsServerEntry *currentServerEntry = [self.serverEntries entryAtIndex:row];
    
    if([currentServerEntry.URLString isEqualToString:URLString] == NO) {
        JNJenkinsServerEntry *updatedEntry = [currentServerEntry copyWithURLString:URLString];
        [self.serverEntries setServerEntry:updatedEntry atIndex:row];
    }
}

-(void)userDidEditUsernameTo:(NSString*)username atRow:(NSInteger)row
{
    if(username == nil) return;
    
    JNJenkinsServerEntry *currentServerEntry = [self.serverEntries entryAtIndex:row];
    
    if([currentServerEntry.username isEqualToString:username] == NO) {
        JNJenkinsServerEntry *updatedEntry = [currentServerEntry copyWithUsername:username];
        [self.serverEntries setServerEntry:updatedEntry atIndex:row];
    }
}

-(void)userDidEditPasswordTo:(NSString*)password atRow:(NSInteger)row
{
    if(password == nil) return;
    
    JNJenkinsServerEntry *currentServerEntry = [self.serverEntries entryAtIndex:row];
    
    if([currentServerEntry.password isEqualToString:password] == NO) {
        JNJenkinsServerEntry *updatedEntry = [currentServerEntry copyWithPassword:password];
        [self.serverEntries setServerEntry:updatedEntry atIndex:row];
    }
}

-(void)userDidChangePredicateTo:(NSString*)predicate atRow:(NSInteger)row
{
    if(predicate == nil) return;
    
    JNJenkinsServerEntry *currentServerEntry = [[self serverEntries] entryAtIndex:row];
    
    if([currentServerEntry.predicateIdentifier isEqualToString:predicate] == NO) {
        NSInteger indexOfFilter = [self.serverFilters indexOfFilterWithTitle:predicate];
        NSString *predicateIdentifier = [self.serverFilters identifierForFilterAtIndex:indexOfFilter];
        
        JNJenkinsServerEntry *updatedEntry = [currentServerEntry copyWithPredicateIdentifier:predicateIdentifier];
        [self.serverEntries setServerEntry:updatedEntry atIndex:row];
    }
}

#pragma mark - NSTableViewDataSource

- (NSInteger)numberOfRowsInTableView:(NSTableView *)aTableView {
    return [[self serverEntries] count];
}

- (id)tableView:(NSTableView *)aTableView
objectValueForTableColumn:(NSTableColumn *)aTableColumn
            row:(NSInteger)rowIndex {
    JNJenkinsServerEntry *currentServerEntry = [[self serverEntries] entryAtIndex:rowIndex];
    
    if([[aTableColumn identifier] isEqualToString:@"ServerURLColumn"]) {
        return [currentServerEntry URLString];
    }
    
    if([[aTableColumn identifier] isEqualToString:@"UsernameColumn"]) {
        return [currentServerEntry username];
    }    
    
    if([[aTableColumn identifier] isEqualToString:@"PasswordColumn"]) {
        return [currentServerEntry password];
    }

    if([[aTableColumn identifier] isEqualToString:@"PredicateColumn"]) {
        NSString *predicateIdentifier = [currentServerEntry predicateIdentifier];
        NSString *predicateTitle = [[self serverFilters] titleForFilterWithIdentifier:predicateIdentifier];
        
        return predicateTitle;
    }    
    
    return @"";
}

#pragma mark - NSComboBoxCellDataSource 

- (NSInteger)numberOfItemsInComboBoxCell:(NSComboBoxCell *)aComboBoxCell
{
    return [[self serverFilters] numberOfEntries];
}

- (id)comboBoxCell:(NSComboBoxCell *)aComboBoxCell objectValueForItemAtIndex:(NSInteger)index
{
    [aComboBoxCell setAction:@selector(comboBoxSelectionDidChange:)];
    [aComboBoxCell setTarget:self];
    
    return [[self serverFilters] titleForFilterAtIndex:index];
}

- (NSUInteger)comboBoxCell:(NSComboBoxCell *)aComboBoxCell indexOfItemWithStringValue:(NSString *)aString
{
    return [[self serverFilters] indexOfFilterWithTitle:aString];
}

#pragma mark -

- (void)comboBoxSelectionDidChange:(id)sender
{
    if([sender isKindOfClass:[NSTableView class]]) {
        NSTableView *tableView = (NSTableView*)sender;
        NSComboBoxCell *comboBoxCell = [tableView selectedCell];
        
        NSInteger indexOfSelectedItem = [comboBoxCell indexOfSelectedItem];
        id value = [[comboBoxCell dataSource] comboBoxCell:comboBoxCell objectValueForItemAtIndex:indexOfSelectedItem];
        
        NSInteger row = [tableView selectedRow];
        [self userDidChangePredicateTo:value atRow:row];
    }
    
    DLog(@"%@", sender);
}

#pragma mark -

- (void)serverListTextDidEndEditingWithNotification:(NSNotification *)notification {
    NSObject *object = [notification object];
    
    if(object == [self pollServerListTableView]) {
        NSDictionary *userInfo = [notification userInfo];
        NSTextView *textView = [userInfo objectForKey:@"NSFieldEditor"];
        
        __strong NSTableView *pollServerListTableView = [self pollServerListTableView];
        NSInteger editedRow = [pollServerListTableView editedRow];
        
        if(editedRow >= 0 && editedRow < [[self serverEntries] count]) {
            NSInteger editedColumn = [pollServerListTableView editedColumn];
            
            NSString *editedText = [[textView textStorage] string];
            
            if(editedColumn == JNPreferencesTableColumnIndexServerURL) {
                [self userDidEditServerURLTo:editedText atRow:editedRow];
            } else if(editedColumn == JNPreferencesTableColumnIndexUsername) {
                [self userDidEditUsernameTo:editedText atRow:editedRow];
            } else if(editedColumn == JNPreferencesTableColumnIndexPassword) {
                [self userDidEditPasswordTo:editedText atRow:editedRow];
            }
            
            [pollServerListTableView reloadDataForRowIndexes:[NSIndexSet indexSetWithIndex:editedRow]
                                               columnIndexes:[NSIndexSet indexSetWithIndex:editedColumn]];
        }
    }
}

#pragma mark - Table Buttons

typedef NS_ENUM(NSInteger, NTableActionButtons) {
    JNTableActionAddSegmentedControlButton = 0,
    JNTableActionDeleteSegmentedControlButton = 1
};

-(void)updateServerURLListActionButtonsForState {
    BOOL serverURLListHasEntries = [[self serverEntries] hasEntries];
    
    __strong NSSegmentedControl *pollServerURLListActions = [self pollServerURLListActions];
    [pollServerURLListActions setEnabled:serverURLListHasEntries forSegment:JNTableActionDeleteSegmentedControlButton];
}

-(IBAction)userDidSelectTableActionSegmentedControllerButton:(id)sender {
    NSSegmentedControl *control = (NSSegmentedControl*)sender;
    NSInteger selectedAction = [control selectedSegment];
    
    __strong NSTableView *pollServerListTableView = [self pollServerListTableView];
    
    if(selectedAction == JNTableActionAddSegmentedControlButton) {
        NSString *placeholderURL = NSLocalizedString(@"PollURLPlaceholder", @"");
        [[self serverEntries] addServerWithURLString:placeholderURL];
    } else if(selectedAction == JNTableActionDeleteSegmentedControlButton) {
        NSInteger selectedRowIndex = [pollServerListTableView selectedRow];
        [[self serverEntries] removeServerAtIndex:selectedRowIndex];
    } else {
        NSAssert(FALSE, @"More segments than we current handle - coding error?");
    }
    
    [pollServerListTableView reloadData]; // could just reload rows?
    
    [self updateServerURLListActionButtonsForState];
}

#pragma mark - Poll Period

-(void)updatePollPeriodSeconds:(NSInteger)seconds {
    __strong NSStepper *pollPeriodStepper = [self pollPeriodStepper];
    [pollPeriodStepper setIntegerValue:seconds];
    
    __strong NSTextField *pollPeriodTextField = [self pollPeriodTextField];
    [pollPeriodTextField setIntegerValue:seconds];
    [self setPollPeriodSeconds:seconds];
}

-(IBAction)userDidChangePollPeriodValue:(id)sender {
    __strong NSTextField *pollPeriodTextField = [self pollPeriodTextField];
    NSInteger pollSeconds = [pollPeriodTextField integerValue];
    
    [self updatePollPeriodSeconds:pollSeconds];
}

#pragma mark - NSStepper Value Changes

- (IBAction)valueChangedInPollPeriodStepper:(NSStepper *)sender {
    NSInteger pollSeconds = [sender integerValue];
    [self updatePollPeriodSeconds:pollSeconds];
}

#pragma mark - Use Notifications

-(IBAction)userDidChangeUseNotificationsSwitch:(id)sender {
    __strong NSButton *useNotificationsCheckBoxButton = [self useNotificationsCheckBoxButton];
    BOOL shouldUseNotifications = ([useNotificationsCheckBoxButton state] == NSOnState);
    
    [self setUseNotifications:shouldUseNotifications];
    
    [self updateNotifyOnlyOnFailureSwitchForNotificationsSwitchState];
}

#pragma mark - Notify Only On Fail

-(IBAction)userDidChangeNotifyOnlyOnFailedJobsOnlySwitch:(id)sender {
    __strong NSButton *notifyOnFailedJobsOnlyCheckBoxButton = [self notifyOnFailedJobsOnlyCheckBoxButton];
    BOOL notifyOnlyOnFailure = ([notifyOnFailedJobsOnlyCheckBoxButton state] == NSOnState);
    
    [self setNotifyOnFailedJobsOnly:notifyOnlyOnFailure];
}

-(void)updateNotifyOnlyOnFailureSwitchForNotificationsSwitchState
{
    __strong NSButton *useNotificationsCheckBoxButton = [self useNotificationsCheckBoxButton];
    BOOL shouldUseNotifications = ([useNotificationsCheckBoxButton state] == NSOnState);
    
    __strong NSButton *notifyOnFailedJobsOnlyCheckBoxButton = [self notifyOnFailedJobsOnlyCheckBoxButton];
    [notifyOnFailedJobsOnlyCheckBoxButton setEnabled:shouldUseNotifications];
}

#pragma mark - start on login

-(IBAction)userDidChangeStartOnLoginSwitch:(NSButton*)sender {
    __strong NSButton *startOnLoginButton = self.startOnLoginButton;
    self.startAtLoginController.startAtLogin = (startOnLoginButton.state == NSOnState);
}

/*
#pragma mark - Auto Discovery

-(IBAction)userDidSelectAutoDiscoveryButton:(id)sender
{
    [[self autodiscovery] startDiscovery];
}

-(void)setAutoDiscoveryButtonSearching
{
    NSButton *autoDiscoveryButton = [self autoDiscoveryButton];
    [autoDiscoveryButton setEnabled:NO];
    [autoDiscoveryButton setTitle:NSLocalizedString(@"SearchingText", @"")];
}

-(void)setAutoDiscoveryButtonDefault
{
    NSButton *autoDiscoveryButton = [self autoDiscoveryButton];
    [autoDiscoveryButton setEnabled:YES];
    [autoDiscoveryButton setTitle:NSLocalizedString(@"AutoDiscoverText", @"")];
}

#pragma mark JNJenkinsAutoDiscoveryProtocol

-(void)autoDiscoveryDidBegin
{
    [self setAutoDiscoveryButtonSearching];
}

-(void)autoDiscoveryDidSucceedWithURL:(NSString *)URLString
{
    if([[self serverEntries] addServerWithURLString:URLString]) {
        __strong NSTableView *pollServerListTableView = [self pollServerListTableView];
        [pollServerListTableView reloadData]; // could just reload rows?
        
        [self updateServerURLListActionButtonsForState];
    }
}

-(void)autoDiscoveryDidFailWithError:(NSError *)error
{
    [self setAutoDiscoveryButtonDefault];
    NSLog(@"auto discovery failed with error: %@", error);
}

-(void)autoDiscoveryDidEnd
{
    [self setAutoDiscoveryButtonDefault];
}
*/

@end
