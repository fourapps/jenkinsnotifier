//
//  Created by Michael May on 12/10/2012.
//  Copyright (c) 2012 Michael May. All rights reserved.
//

#import "JenkinsNotifier-Swift.h"

#import "JNAppDelegate+AboutAndSettingsWindow.h"

#import "JNUserSettings.h"

#import "JNMainMenuController.h"
#import "JNLocalNotificationManager.h"

#import "JNJenkinsServerEntries.h"

@interface JNAppDelegate ()
@property (nonatomic, strong, readonly) id<JNMenuUpdateDelegate> menuUpdateController;
@property (nonatomic, strong, readonly) id<JNJobNotificationDelegate> localNotificationManager;
@property (nonatomic, strong) JNPreferencesWindowController *preferencesWindowController;
@property (nonatomic, strong, readonly) JNUserSettings *userSettings;
@property (nonatomic, strong, readonly) JNServerFilters *serverFilters;
@end

@implementation JNAppDelegate (AboutAndSettingsWindow)

#pragma mark -

// tiny bit hacky
-(void)windowWillClose:(NSNotification *)notification
{
    JNPreferencesWindowController *preferencesWindowController = [self preferencesWindowController];
    
    if(preferencesWindowController) {
        JNUserSettings *userSettings = [self userSettings];
        BOOL userDidChangeSettings = NO;
        
        NSUInteger pollPeriodSeconds = [userSettings jenkinsPollPeriodSeconds];
        NSUInteger newPollPeriodSeconds = [preferencesWindowController pollPeriodSeconds];
        if(pollPeriodSeconds != newPollPeriodSeconds) {
            userDidChangeSettings = YES;
            [userSettings setJenkinsPollPeriodSeconds:newPollPeriodSeconds];
        }
        
        BOOL shouldUseNotifications = [userSettings shouldUseNotifications];
        BOOL newShouldUseNotifications = [preferencesWindowController useNotifications];
        if(shouldUseNotifications != newShouldUseNotifications) {
            userDidChangeSettings = YES;
            [userSettings setShouldUseNotifications:newShouldUseNotifications];
        }

        {
            BOOL notifyOnFailedJobsOnly = [preferencesWindowController notifyOnFailedJobsOnly];
            [userSettings setNotifyOnFailedJobsOnly:notifyOnFailedJobsOnly];
        }
        
        JNJenkinsServerEntries *newServerList = [preferencesWindowController serverEntries];
        JNJenkinsServerEntries *currentServerList = [userSettings allServerEntries];
        if([newServerList isEqualTo:currentServerList] == NO) {
            userDidChangeSettings = YES;
            [userSettings setServerEntries:newServerList];
        }
        
        if(userDidChangeSettings) {
            [[self localNotificationManager] clearAllNotifications];
            [[self menuUpdateController] clearMenuAndJobsList];
            
            [self.pollingManager beginJenkinsAPIPolling];
        }
        
        [self setPreferencesWindowController:nil];        
    }
}

-(IBAction)userDidSelectPreferencesMenuItem:(id)sender {
    JNPreferencesWindowController *preferencesWindowController = [self preferencesWindowController];
    
    if(preferencesWindowController) {
        [preferencesWindowController close];
        [self setPreferencesWindowController:nil];
        preferencesWindowController = nil;
    }
    
    NSUInteger pollPeriodSeconds = [[self userSettings] jenkinsPollPeriodSeconds];
    BOOL shouldUseNotifications = [[self userSettings] shouldUseNotifications];
    BOOL notifyOnFailedJobsOnly = [[self userSettings] notifyOnFailedJobsOnly];
    
    JNJenkinsServerEntries *serverURLList = [[self userSettings] allServerEntries];
    preferencesWindowController = [JNPreferencesWindowController  preferencesWindowWithPollPeriod:pollPeriodSeconds
                                                                                 useNotifications:shouldUseNotifications
                                                                           notifyOnFailedJobsOnly:notifyOnFailedJobsOnly
                                                                                    serverURLList:serverURLList
                                                                                    serverFilters:[self serverFilters]];
    [preferencesWindowController setDelegate:self];
    [self setPreferencesWindowController:preferencesWindowController];
    
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        [NSApp activateIgnoringOtherApps:YES];
        [preferencesWindowController.window makeKeyAndOrderFront:self];
    });
}


@end
