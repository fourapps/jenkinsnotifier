//
//  Created by Developer on 08/09/2013.
//  Copyright (c) 2013 Four Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol JNMenuUpdateDelegate

-(void)clearMenuAndJobsList;

-(void)clearMenu;

-(void)updateMenuForServerURL:(NSString*)server
                         jobs:(NSArray*)jobs;

-(void)updateMenuForServerURL:(NSString*)server
                        error:(NSError*)error;

@end


@interface JNMainMenuController : NSObject <JNMenuUpdateDelegate>
@end
