//
//  Created by Developer on 21/09/2013.
//  Copyright (c) 2013 Four Apps. All rights reserved.
//

#import "JNApplication.h"

#import <AppKit/AppKit.h>

#import "NSEvent+KeyboardEventHelpers.h"

// because we are sending these messages to nil to force sendAction to
// find the first responder we will get warnings about not knowing anything
// about these selectors, but that's okay in this instance, so we will
// supress the warnings for this one methods

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wundeclared-selector"
#pragma clang diagnostic ignored "-Wselector"

@implementation JNApplication

- (void)sendEvent:(NSEvent *)event
{
    if ([event isKeyDownEvent]) {
        if ([event isCommandKeyModifierEvent]) {
            if ([event isCutKeyDown]) {
                if ([self sendAction:@selector(cut:) to:nil from:self]) {
                    return;
                }
            } else if ([event isCopyKeyDown]) {
                if ([self sendAction:@selector(copy:) to:nil from:self]) {
                    return;
                }
            }
            else if ([event isPasteKeyDown]) {
                if ([self sendAction:@selector(paste:) to:nil from:self]) {
                    return;
                }
            }
            else if ([event isUndoKeyDown]) {
                if ([self sendAction:@selector(undo:) to:nil from:self]) {
                    return;
                }
            }
            else if ([event isSelectAllKeyDown]) {
                if ([self sendAction:@selector(selectAll:) to:nil from:self]) {
                    return;
                }
            }
        }
        else if([event isCommandShiftKeyModifierEvent]) {
            if ([event isRedoKeyDown]) {
                if ([self sendAction:@selector(redo:) to:nil from:self]) {
                    return;
                }
            }
        }
    }
    
    [super sendEvent:event];
}

#pragma clang diagnostic pop


@end
