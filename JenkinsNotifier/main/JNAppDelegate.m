//
//  Created by Michael May on 05/10/2012.
//  Copyright (c) 2012 Michael May. All rights reserved.
//

#import "JNAppDelegate.h"

#import "MMDebugMacros.h"
#import "MMGCDMacros.h"

#import "JNMainMenuController.h"
#import "JNLocalNotificationManager.h"

#import "JNUserSettings.h"
#import "JNServerFilters+Builder.h"

#import "JNJenkinsServerEntry.h"
#import "JNUserSettingsPersistentStore.h"

#import "JNJobStatusMappers.h"

#import "JNJenkinsJobsLog.h"

#import "JNJenkinsServerEntry.h"

#import "JenkinsNotifier-Swift.h"

@interface JNAppDelegate (AboutAndSettingsWindow)
@end

@interface JNAppDelegate ()
@property (nonatomic, strong) IBOutlet id<JNMenuUpdateDelegate> menuUpdateController;
@property (nonatomic, strong) JNPollingManager *pollingManager;
@property (nonatomic, strong) id<JNJobNotificationDelegate> localNotificationManager;

@property (nonatomic, strong) JNPreferencesWindowController *preferencesWindowController;
@property (nonatomic, weak) IBOutlet NSMenu *dummyEditMenuForNSTextFields;
@property (nonatomic, strong) JNUserSettings *userSettings;
@property (nonatomic, strong, readonly) JNServerFilters *serverFilters;
@end

@implementation JNAppDelegate

#pragma mark - Application Lifecycle

-(void)setup
{
    _localNotificationManager = [JNLocalNotificationManager localNotificationManager];
    
    _pollingManager = [[JNPollingManager alloc] initWithMenuUpdateDelegate:self.menuUpdateController
                                                   jobNotificationDelegate:_localNotificationManager
                                                              userSettings:self.userSettings
                                                             serverFilters:self.serverFilters];
    
    [self.pollingManager beginJenkinsAPIPolling];
}

-(void)teardown
{
    [self.pollingManager cancelJenkinsAPIPolling];
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    [self setup];
}

-(void)applicationWillTerminate:(NSNotification *)notification
{
    [self teardown];
}

#pragma mark -

-(instancetype)init {
    self = [super init];
    
    if(self) {
        NSObject<JNUserSettingsPersistentStoreProtocol> *userSettingsStore = [JNUserSettingsPersistentStore userSettingsPersistentStore];
        
        _userSettings = [[JNUserSettings alloc] initWithUserSettingsStore:userSettingsStore];
        
        _serverFilters = [JNServerFilters serverFilters];
        
        NSURLCache *nonCachingCache = [[NSURLCache alloc] initWithMemoryCapacity:0 diskCapacity:0 diskPath:nil];
        [NSURLCache setSharedURLCache:nonCachingCache];
    }
    
    return self;
}

-(void)dealloc {
    [self teardown];
}

@end
