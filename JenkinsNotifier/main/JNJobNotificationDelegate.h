//
//  Created by Developer on 08/09/2013.
//  Copyright (c) 2013 Four Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@class JNJenkinsAPIJob;

@protocol JNJobNotificationDelegate
@required
-(NSInteger)jobStatusFromUserInfo:(NSDictionary*)userInfo;
- (NSUserNotification *)deliverNotificationForJob:(JNJenkinsAPIJob*)job;
-(void)clearNotification:(NSUserNotification*)notification;
-(void)clearAllNotifications;
@end
