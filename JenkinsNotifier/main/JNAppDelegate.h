//
//  Created by Michael May on 05/10/2012.
//  Copyright (c) 2012 Michael May. All rights reserved.
//

// http://ci.jruby.org/
// https://builds.apache.org/
// testy/testy
// local machine: admin / admin

#import <Cocoa/Cocoa.h>

#import "JNUserSettings.h"
#import "JNPreferencesWindowController.h"
#import "JNJobNotificationDelegate.h"

@class JNPollingManager;
@protocol JNJobNotificationDelegate;
@protocol JNMenuUpdateDelegate;

@interface JNAppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate>

// needed so that the JNMainMenuController can action the Refresh menu option
@property (nonatomic, strong, readonly) JNPollingManager *pollingManager;

-(instancetype)init NS_DESIGNATED_INITIALIZER;

@end
