//
//  Created by Developer on 08/09/2013.
//  Copyright (c) 2013 Four Apps. All rights reserved.
//

#import "JenkinsNotifier-Swift.h"

#import "MMDebugMacros.h"

#import "JNMainMenuController.h"

#import "JNAppDelegate.h"
#import "JNJobStatusMappers.h"

@interface JNMainMenuController ()
@property (nonatomic, strong) IBOutlet NSMenu* menu;
@property (nonatomic, strong) IBOutlet NSMenuItem* applicationMenuItem;
@property (nonatomic, strong) IBOutlet NSMenuItem* refreshMenuItem;

@property (nonatomic, strong) NSStatusItem* statusItem;
@property (nonatomic, strong, readonly) NSMutableDictionary* jobs;
@property (nonatomic, strong, readonly) NSDateFormatter *dateFormatter;
@end

@implementation JNMainMenuController

#pragma mark - utility

-(NSAttributedString*)menuEntryWithTitle:(NSString*)title
                               timestamp:(NSDate*)timestamp
{
    return [JNMainMenuController menuEntryWithTitle:title
                                          timestamp:timestamp
                                      dateFormatter:[self dateFormatter]];
}

+(NSAttributedString*)menuEntryWithTitle:(NSString*)title
                               timestamp:(NSDate*)timestamp
                           dateFormatter:(NSDateFormatter*)dateFormatter
{
    NSMutableAttributedString *titleAttributed = [[NSMutableAttributedString alloc] init];
    
    if(title) {
        NSDictionary *serverURLAttributes = @{NSFontAttributeName: [NSFont menuBarFontOfSize:14.0f]};
        NSAttributedString *serverURLAttributedString = [[NSAttributedString alloc] initWithString:title
                                                                                        attributes:serverURLAttributes];
        
        NSAttributedString *newLine = [[NSAttributedString alloc] initWithString:@"\n"];
        
        
        [titleAttributed appendAttributedString:serverURLAttributedString];
        [titleAttributed appendAttributedString:newLine];
    }
    
    if(timestamp && dateFormatter) {
        NSString *formattedTimeStamp = [dateFormatter stringFromDate:timestamp];
        
        NSDictionary *timestampAttributes = @{NSFontAttributeName : [NSFont systemFontOfSize:10.0f],
                                              NSForegroundColorAttributeName : [NSColor darkGrayColor]};
        NSAttributedString *timestampAttributedString = [[NSAttributedString alloc] initWithString:formattedTimeStamp
                                                                                        attributes:timestampAttributes];
        
        [titleAttributed appendAttributedString:timestampAttributedString];
    }
    
    return titleAttributed;
}

#pragma mark -

-(void)setMenu:(NSMenu *)menu
{
    _menu = menu;
    
    NSStatusItem *statusItem = [[NSStatusBar systemStatusBar] statusItemWithLength:NSVariableStatusItemLength];
    [statusItem setMenu:_menu];
    [statusItem setHighlightMode:YES];
    [self setStatusItem:statusItem];
    
    [self setMainMenuImageForState:JNJenkinsAPIJobStatesJNJenkinsAPIJobStateUnknown];
}

#pragma mark -

-(void)setMainMenuImageForState:(JNJenkinsAPIJobStates)state
{
    NSStatusItem *statusItem = [self statusItem];
    
    NSImage *menuImage = [JNJobStatusMappers jobImageimageForState:state];
    [statusItem setImage:menuImage];
    
    NSString *menuToolTip = [JNJobStatusMappers toolTipTextForJobState:state];
    [[self statusItem] setToolTip:menuToolTip];
}

#pragma mark - Servers & Jobs Submenus

static const NSInteger kJobsMenuEntryForSubJobsTag = 100;

-(NSMenuItem *)serversMenuSeparator
{
    NSMenuItem *serversMenuSeparator = [[self menu] itemWithTag:kJobsMenuEntryForSubJobsTag];
    
    return serversMenuSeparator;
}

-(NSMenuItem *)addJobMenuItemWithTitle:(NSString*)title
                                 state:(JNJenkinsAPIJobStates)state
                                toMenu:(NSMenu*)menu
                      indentationLevel:(NSInteger)indentationLevel
                     representedObject:(id)representedObject
{
    NSMenuItem *jobMenuItem = [menu addItemWithTitle:title
                                              action:nil
                                       keyEquivalent:@""];
    
    NSImage *menuImage = [JNJobStatusMappers jobImageimageForState:state];
    if(menuImage) {
        [jobMenuItem setImage:menuImage];
    }
    
    [jobMenuItem setIndentationLevel:indentationLevel];
    
    [jobMenuItem setRepresentedObject:representedObject];
    
    [jobMenuItem setTarget:self];
    [jobMenuItem setAction:@selector(userDidSelectServerOrJobsMenuItem:)];
    
    return jobMenuItem;
}

+(NSAttributedString*)serverMenuEntryFromURLString:(NSString*)URLString
{
    NSAttributedString *menuEntry = nil;
    
    if(URLString) {
        NSDictionary *serverURLAttributes = @{NSFontAttributeName: [NSFont boldSystemFontOfSize:14.0f]};
        
        menuEntry = [[NSAttributedString alloc] initWithString:URLString
                                                    attributes:serverURLAttributes];
        
    } else {
        menuEntry = [[NSMutableAttributedString alloc] init];
    }
    
    return menuEntry;
}

-(NSMenuItem *)addServerMenuItemWithURL:(NSString*)URLString
                              timeStamp:(NSDate*)timestamp
{
    NSMenuItem *serverMenuItem = [self addJobMenuItemWithTitle:@""
                                                         state:JNJenkinsAPIJobStatesJNJenkinsAPIJobStateUnknown
                                                        toMenu:[self menu]
                                              indentationLevel:0
                                             representedObject:URLString];
    
    NSAttributedString *serverMenuTitle = [JNMainMenuController serverMenuEntryFromURLString:URLString];
    
    [serverMenuItem setAttributedTitle:serverMenuTitle];
    
    return serverMenuItem;
}

-(void)clearServersMenu
{
    NSMenuItem *serversMenuSeparator = [self serversMenuSeparator];
    DAssert(serversMenuSeparator, @"");
    
    NSInteger indexOfServersSeparator = [[self menu] indexOfItem:serversMenuSeparator];
    indexOfServersSeparator += 1;
    
    while([[self menu] numberOfItems] > indexOfServersSeparator) {
        [[self menu] removeItemAtIndex:indexOfServersSeparator];
    }
}

-(void)clearMenu
{
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        [self clearServersMenu];
        [self setMainMenuImageForState:JNJenkinsAPIJobStatesJNJenkinsAPIJobStateUnknown];
    });
}

-(void)clearMenuAndJobsList
{
    [self clearMenu];
    [[self jobs] removeAllObjects];
}

-(void)updateMenuForServerURL:(NSString*)server
                       result:(NSObject*)result
{
    DLog(@"");
    
    // by dispatching onto the serial main thread we know we wont' be
    // updating the menu at the same time from different API completion callbacks
    // as well as ensuring we are on the main thread when we do UI work
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        [self clearServersMenu];
        
        [[self jobs] setObject:result forKey:server];
        
        JNJenkinsAPIJobStates statusBarMenuItemState = JNJenkinsAPIJobStatesJNJenkinsAPIJobStateUnknown;
        JNJenkinsAPIJobStates serverEntryItemState = JNJenkinsAPIJobStatesJNJenkinsAPIJobStateUnknown;
        
        NSMenuItem *serverMenuItem = nil;
        
        NSArray *jobs = nil;
        
        NSArray *serverURLs = [[self jobs] allKeys];
        for(NSString *serverURL in serverURLs) {
            serverEntryItemState = JNJenkinsAPIJobStatesJNJenkinsAPIJobStateUnknown;
            
            NSObject *statusObjectForServerURL = [[self jobs] objectForKey:serverURL];
            serverMenuItem = [self addServerMenuItemWithURL:serverURL
                                                  timeStamp:[NSDate date]];
            
            if([statusObjectForServerURL isKindOfClass:[NSError class]] == NO) {
                DAssertClass([NSArray class], statusObjectForServerURL);
                
                jobs = (NSArray*)statusObjectForServerURL;
                
                for(JNJenkinsAPIJob *job in jobs) {
                    [self addJobMenuItemWithTitle:[job name]
                                            state:[job status]
                                           toMenu:[self menu]
                                 indentationLevel:1
                                representedObject:[job URL]];
                    
                    serverEntryItemState = mostImportantState(serverEntryItemState, [job status]);
                }
            } else {
                NSError *error = (NSError*)statusObjectForServerURL;
                NSString *errorText = [error localizedDescription];
                
                [self addJobMenuItemWithTitle:errorText
                                        state:JNJenkinsAPIJobStatesJNJenkinsAPIJobStateConnectionFailure
                                       toMenu:[self menu]
                             indentationLevel:1
                            representedObject:nil];
                
                serverEntryItemState = mostImportantState(serverEntryItemState, JNJenkinsAPIJobStatesJNJenkinsAPIJobStateConnectionFailure);
            }
            
            NSImage *serverEntryItemMenuImage = [JNJobStatusMappers jobImageimageForState:serverEntryItemState];
            [serverMenuItem setImage:serverEntryItemMenuImage];
            
            statusBarMenuItemState = mostImportantState(statusBarMenuItemState, serverEntryItemState);
        }
        
        [self setMainMenuImageForState:statusBarMenuItemState];
        
        [self updateMenuForTimestamp:[NSDate date]];
    });
}

-(void)updateMenuForTimestamp:(NSDate*)date
{
    NSString *refreshText = NSLocalizedString(@"RefreshMenuText", @"");
    NSAttributedString *newRefreshMenuText = [self menuEntryWithTitle:refreshText timestamp:date];
    [[self refreshMenuItem] setAttributedTitle:newRefreshMenuText];
}

// be good to refactor the general method to know what to do in each case
-(void)updateMenuForServerURL:(NSString*)server
                         jobs:(NSArray *)jobs
{
    [self updateMenuForServerURL:server result:jobs];
}

-(void)updateMenuForServerURL:(NSString*)server
                        error:(NSError *)error
{
    [self updateMenuForServerURL:server result:error];
}

#pragma mark - menu actions

-(IBAction)userDidSelectServerOrJobsMenuItem:(id)sender
{
    NSMenuItem *selectedMenuItem = (NSMenuItem*)sender;
    id representedObject = [selectedMenuItem representedObject];
    
    if([representedObject isKindOfClass:[NSString class]]) {
        NSString *serverURL = (NSString*)representedObject;
        
        NSURL *URL = [NSURL URLWithString:serverURL];
        [[NSWorkspace sharedWorkspace] openURL:URL];
    }
}

-(IBAction)userDidSelectReviewThisAppMenuItem:(id)sender
{
    static NSString *JNReviewThisAppURL = @"http://appstore.com/mac/jenkinsnotifier";
    
    NSURL *URL = [NSURL URLWithString:JNReviewThisAppURL];
    [[NSWorkspace sharedWorkspace] openURL:URL];
}

-(IBAction)userDidSelectCreditsMenuItem:(id)sender
{
    static NSString *JNCreditsURL = @"http://codermay.co.uk/blog/jenkins-notifier/credits/";
    
    NSURL *URL = [NSURL URLWithString:JNCreditsURL];
    [[NSWorkspace sharedWorkspace] openURL:URL];
}

-(IBAction)userDidSelectTroubleshootingItem:(id)sender
{
    static NSString *JNToubleshootingWordPressBlogURL = @"http://codermay.co.uk/blog/jenkins-notifier/troubleshooting/";
    
    NSURL *URL = [NSURL URLWithString:JNToubleshootingWordPressBlogURL];
    [[NSWorkspace sharedWorkspace] openURL:URL];
}

-(IBAction)userDidSelectRefreshMenuItem:(id)sender
{
    JNAppDelegate* delegate = (JNAppDelegate *)([NSApplication sharedApplication].delegate);
    
    [delegate.pollingManager pollServersNow];
}

#pragma mark -

-(instancetype)init
{
    self = [super init];
    
    if(self) {
        _jobs = [NSMutableDictionary new];
        
        _dateFormatter = [[NSDateFormatter alloc] init];
        [_dateFormatter setDateStyle:NSDateFormatterShortStyle];
        [_dateFormatter setTimeStyle:NSDateFormatterShortStyle];
    }
    
    return self;
}

@end
