//
//  Created by Michael May on 12/10/2012.
//  Copyright (c) 2012 Michael May. All rights reserved.
//

#import "JNAppDelegate.h"

@interface JNAppDelegate (AboutAndSettingsWindow) <NSWindowDelegate>

-(IBAction)userDidSelectPreferencesMenuItem:(id)sender;

@end
