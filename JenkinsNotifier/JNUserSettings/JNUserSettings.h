//
//  Created by Michael May on 12/10/2012.
//  Copyright (c) 2012 Michael May. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol JNUserSettingsPersistentStoreProtocol;
@class JNJenkinsServerEntry;
@class JNJenkinsServerEntries;

extern const NSTimeInterval JNJenkinsAPIJobTimeOutDefault;

@interface JNUserSettings : NSObject

@property (nonatomic, assign) NSInteger jenkinsPollPeriodSeconds;
@property (nonatomic, assign) BOOL shouldUseNotifications;

#pragma mark -

-(id)initWithUserSettingsStore:(id<JNUserSettingsPersistentStoreProtocol>)settingsStore;

#pragma mark - Jenkings Server Entries

// an array of JNJenkinsServerEntry objects
-(NSInteger)totalNumberOfServerEntries;
-(JNJenkinsServerEntry*)serverEntryAtIndex:(NSInteger)index;

-(JNJenkinsServerEntries*)allServerEntries;
-(void)setServerEntries:(JNJenkinsServerEntries*)serverEntries;

-(NSTimeInterval)serverTimeoutSeconds;

-(BOOL)notifyOnFailedJobsOnly;
-(void)setNotifyOnFailedJobsOnly:(BOOL)notifyOnFailedJobsOnly;

@end
