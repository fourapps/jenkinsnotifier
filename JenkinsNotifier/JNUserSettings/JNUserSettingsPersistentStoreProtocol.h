//
//  Created by Michael May on 02/04/2013.
//  Copyright (c) 2013 Four Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol JNUserSettingsPersistentStoreProtocol <NSObject>
@required

- (id)objectForKey:(NSString *)defaultName;
- (void)setObject:(id)value forKey:(NSString *)defaultName;
- (void)removeObjectForKey:(NSString *)defaultName;
- (NSArray *)arrayForKey:(NSString *)defaultName;

+(instancetype)userSettingsPersistentStore;

@end
