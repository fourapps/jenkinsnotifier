//
//  Created by Developer on 2014/02/28.
//  Copyright (c) 2014 Four Apps. All rights reserved.
//

#import "JNUserSettings+Accessors.h"
#import "JNUserSettingsPersistentStoreProtocol.h"

@interface JNUserSettings ()
@property (nonatomic, strong, readonly) id<JNUserSettingsPersistentStoreProtocol> userSettingsStore;
@end

@implementation JNUserSettings (Accessors)

-(BOOL)hasEntryForKey:(id)key
{
    if(key == nil) return NO;
    
    NSObject *valueObject = [[self userSettingsStore] objectForKey:key];
    
    return (valueObject != nil);
}

#pragma mark - 

-(void)setBool:(BOOL)value forKey:(id)key
{
    if(key == nil) return;
    
    NSNumber *boolValueObject = @(value);
    
    [[self userSettingsStore] setObject:boolValueObject forKey:key];
}

-(BOOL)boolForKey:(id)key
{
    if(key == nil) return NO;
    
    BOOL boolValue = NO;
    NSNumber *boolValueObject = [[self userSettingsStore] objectForKey:key];
    
    if(boolValueObject) {
        boolValue = [boolValueObject boolValue];
    }
    
    return boolValue;
}

@end
