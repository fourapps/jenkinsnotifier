//
//  Created by Developer on 2014/02/28.
//  Copyright (c) 2014 Four Apps. All rights reserved.
//

#import "JNUserSettings.h"

@interface JNUserSettings (Accessors)

-(BOOL)hasEntryForKey:(id)key;

-(void)setBool:(BOOL)value forKey:(id)key;
-(BOOL)boolForKey:(id)key;

@end
