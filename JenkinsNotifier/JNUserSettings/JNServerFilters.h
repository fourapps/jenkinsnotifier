//
//  Created by Developer on 02/09/2013.
//  Copyright (c) 2013 Four Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JNServerFilters : NSObject

extern NSString *JNServerFiltersIdentifierKey;
extern NSString *JNServerFiltersTitleKey;
extern NSString *JNServerFiltersPredicateKey;

-(NSString*)identifierForFilterAtIndex:(NSUInteger)index;
-(NSString*)titleForFilterAtIndex:(NSUInteger)index;
-(NSString*)predicateForFilterAtIndex:(NSUInteger)index;

-(NSUInteger)numberOfEntries;

// returns NSNotFound on error
-(NSInteger)indexOfFilterWithTitle:(NSString*)title;
-(NSInteger)indexOfFiterWithIdentifier:(NSString*)identifier;

-(NSString*)titleForFilterWithIdentifier:(NSString*)identifier;
-(NSString*)predicateForFilterWithIdentifier:(NSString*)identifier;

#pragma mark - mostly for testing

-(instancetype)initWithDefaultFilters:(NSDictionary*)defaultFilters
                          userFilters:(NSDictionary*)userFilters;

@end
