//
//  Created by Developer on 2014/01/01.
//  Copyright (c) 2014 Four Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@class JNJenkinsServerEntry;

@interface JNJenkinsServerEntries : NSObject <NSFastEnumeration>

-(nullable JNJenkinsServerEntry*)entryAtIndex:(NSInteger)index;
-(nullable JNJenkinsServerEntry*)lastObject;
-(nullable JNJenkinsServerEntry*)firstObject;

-(NSUInteger)count;

-(BOOL)hasEntries;

+(nonnull instancetype)serverEntries;

@end

#pragma mark - 

@interface JNJenkinsServerEntries (Mutation)

-(void)removeServerAtIndex:(NSInteger)index;

-(void)addServerEntry:(nonnull JNJenkinsServerEntry*)entry;

-(void)setServerEntry:(nonnull JNJenkinsServerEntry*)entry atIndex:(NSInteger)index;

// return YES on add, NO If already exists and no added again
-(BOOL)addServerWithURLString:(nonnull NSString *)URLString;

@end
