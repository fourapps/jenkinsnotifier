//
//  Created by Developer on 2014/01/01.
//  Copyright (c) 2014 Four Apps. All rights reserved.
//

#import "JNJenkinsServerEntries.h"
#import "JNJenkinsServerEntry.h"

#import "NSArray+ObjectAtIndexIfNotOutOfBounds.h"

@interface JNJenkinsServerEntries ()
@property (nonatomic, strong, readonly) NSMutableArray<JNJenkinsServerEntry*> *entries;
@end

@implementation JNJenkinsServerEntries

#pragma mark -

-(JNJenkinsServerEntry*)entryAtIndex:(NSInteger)index
{
    JNJenkinsServerEntry *serverEntry = [[self entries] objectAtIndexIfNotOutOfBounds:index];
    return serverEntry;
}

-(JNJenkinsServerEntry*)lastObject
{
    return [[self entries] lastObject];
}

-(JNJenkinsServerEntry*)firstObject
{
    return [self entryAtIndex:0];
}

#pragma mark -

-(void)removeServerAtIndex:(NSInteger)index
{
    if(index >= 0 && index < [self count]) {
        [[self entries] removeObjectAtIndex:index];
    }
}

-(void)addServerEntry:(JNJenkinsServerEntry*)entry
{
    [[self entries] addObject:entry];
}

-(void)setServerEntry:(JNJenkinsServerEntry*)entry atIndex:(NSInteger)index
{
    if(index >= 0 && index < [self count]) {
        self.entries[index] = entry;
    }
}

#pragma mark -

-(BOOL)addServerWithURLString:(NSString *)URLString
{
    if([self containsServerWithURL:URLString] == NO) {
        JNJenkinsServerEntry *newEntry = [[JNJenkinsServerEntry alloc] initWithURLString:URLString];
        
        [self addServerEntry:newEntry];
        
        return YES;
    }
    
    return NO;
}

#pragma mark -

-(BOOL)hasEntries
{
    return ([self count] > 0);
}

-(BOOL)containsServerWithURL:(NSString*)serverURLString
{
    for(JNJenkinsServerEntry* entry in [self entries]) {
        if([[entry URLString] caseInsensitiveCompare:serverURLString] == NSOrderedSame) {
            return YES;
        }
    }
    
    return NO;
}

#pragma mark - NSFastEnumeration

- (NSUInteger)countByEnumeratingWithState:(NSFastEnumerationState *)state objects:(id __unsafe_unretained [])buffer count:(NSUInteger)len
{
    return [[self entries] countByEnumeratingWithState:state
                                               objects:buffer
                                                 count:len];
}

#pragma mark -

-(NSUInteger)count
{
    return [[self entries] count];
}

-(JNJenkinsServerEntries*)mutableCopy
{
    NSMutableArray *entriesCopy = [[self entries] mutableCopy];
    return [[[self class] alloc] initWithEntries:entriesCopy];
}

-(BOOL)isEqualTo:(id)object
{
    return [[self entries] isEqualToArray:[object entries]];
}

#pragma mark - 

-(id)initWithEntries:(NSMutableArray*)entries
{
    self = [super init];
    
    if(self) {
        _entries = entries;
    }
    
    return self;
}

-(id)init
{
    NSMutableArray *entries = [[NSMutableArray alloc] init];
    return [self initWithEntries:entries];
}

#pragma mark -

+(instancetype)serverEntries
{
    return [[self alloc] init];
}

@end
