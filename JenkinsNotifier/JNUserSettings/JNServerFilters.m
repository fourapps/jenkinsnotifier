//
//  Created by Developer on 02/09/2013.
//  Copyright (c) 2013 Four Apps. All rights reserved.
//

#import "MMDebugMacros.h"

#import "JNServerFilters.h"

@interface JNServerFilters ()
@property (nonatomic, strong) NSDictionary *jobFiltersOptions;
@end

@implementation JNServerFilters

NSString *JNServerFiltersIdentifierKey = @"identifier";
NSString *JNServerFiltersTitleKey = @"title";
NSString *JNServerFiltersPredicateKey = @"predicate";

-(NSString*)keyForEntryAtIndex:(NSUInteger)index
{
    if(index == NSNotFound) return nil;

    NSArray *allkeys = [[self jobFiltersOptions] allKeys];
    if(index >= [allkeys count]) return nil;
    
    return [allkeys objectAtIndex:index];
}

-(NSDictionary*)filterEntryAtIndex:(NSUInteger)index
{
    NSString *entryKey = [self keyForEntryAtIndex:index];
    NSDictionary *entry = [[self jobFiltersOptions] objectForKey:entryKey];
    
    return entry;
}

-(id)filterEntryValueForKey:(NSString*)key atIndex:(NSUInteger)index
{
    NSDictionary *entry = [self filterEntryAtIndex:index];
    NSObject *entryValue = [entry objectForKey:key];
    
    return entryValue;
}

-(NSString*)identifierForFilterAtIndex:(NSUInteger)index
{
    return (NSString*)[self filterEntryValueForKey:JNServerFiltersIdentifierKey
                                           atIndex:index];
}

-(NSString*)titleForFilterAtIndex:(NSUInteger)index
{
    return (NSString*)[self filterEntryValueForKey:JNServerFiltersTitleKey
                                           atIndex:index];
}

-(NSString*)predicateForFilterAtIndex:(NSUInteger)index
{
    return (NSString*)[self filterEntryValueForKey:JNServerFiltersPredicateKey
                                           atIndex:index];
}

-(NSUInteger)numberOfEntries
{
    return [[[self jobFiltersOptions] allKeys] count];
}

typedef NSString*(^TextForIndexBlock)(NSUInteger);

-(NSInteger)indexOfItemWithMatchingText:(NSString*)text
                     usingIndexBlock:(TextForIndexBlock)indexBlock
{
    NSUInteger numberOfEntries = [self numberOfEntries];
    
    for(NSUInteger index = 0; index < numberOfEntries; index++) {
        NSString *textAtIndex = indexBlock(index);
        
        if([textAtIndex caseInsensitiveCompare:text] == NSOrderedSame) {
            return index;
        }
    }
    
    return NSNotFound;
}

-(NSInteger)indexOfFilterWithTitle:(NSString*)title
{
    return [self indexOfItemWithMatchingText:title
                        usingIndexBlock:^NSString*(NSUInteger index) {
                            return [self titleForFilterAtIndex:index];
                        }];
}

-(NSInteger)indexOfFiterWithIdentifier:(NSString*)identifier
{
    return [self indexOfItemWithMatchingText:identifier
                             usingIndexBlock:^NSString*(NSUInteger index) {
                                 return [self identifierForFilterAtIndex:index];
                             }];
}

#pragma mark - 

-(NSString*)titleForFilterWithIdentifier:(NSString*)identifier
{
    NSInteger index = [self indexOfFiterWithIdentifier:identifier];
    NSString *predicateTitle = [self titleForFilterAtIndex:index];
    
    return predicateTitle;
}

-(NSString*)predicateForFilterWithIdentifier:(NSString*)identifier
{
    NSInteger index = [self indexOfFiterWithIdentifier:identifier];
    NSString *predicateTitle = [self predicateForFilterAtIndex:index];
    
    return predicateTitle;
}

#pragma mark -

-(id)initWithDefaultFilters:(NSDictionary*)defaultFilters
                userFilters:(NSDictionary*)userFilters
{
    self = [super init];
    
    if(self) {
        NSMutableDictionary *jobFiltersOptions = [defaultFilters mutableCopy];
    
        for(NSObject *entry in defaultFilters) {
            NSLog(@"entry: %@", entry);
        }
        
        if(userFilters) {
            [jobFiltersOptions addEntriesFromDictionary:userFilters];
        }
        
        _jobFiltersOptions = jobFiltersOptions;
    }
    
    return self;
}

@end
