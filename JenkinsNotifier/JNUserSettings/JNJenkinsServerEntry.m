//
//  Created by Developer on 20/04/2013.
//  Copyright (c) 2013 Four Apps. All rights reserved.
//

#import "JNJenkinsServerEntry.h"

#import "NSArray+ObjectAtIndexIfNotOutOfBounds.h"

@interface JNJenkinsServerEntry ()
@property (nonatomic, copy, readwrite, nonnull) NSString *URLString;
@property (nonatomic, copy, readwrite, nonnull) NSString *username;
@property (nonatomic, copy, readwrite, nonnull) NSString *password;
@property (nonatomic, copy, readwrite, nonnull) NSString *predicateIdentifier;
@end

@implementation JNJenkinsServerEntry

#pragma mark - username/password/url concatenation/seperation for keychain

static NSString *JNUserSettingsSeperatorForKeychain = @"|";
static NSString *JNJenkinsServerEntryDefaultNoPredicateIdentifier = @"0";

// do not fuck with the order here
typedef NS_ENUM(NSUInteger, JNEncodedServerSettingsIndexes) {
    JNEncodedServerSettingsIndexUsername = 0,
    JNEncodedServerSettingsIndexPassword = 1,
    JNEncodedServerSettingsIndexURL = 2,
    JNEncodedServerSettingsIndexPredicate = 3
};


#pragma mark - 

-(instancetype)copyWithZone:(NSZone *)zone
{
    JNJenkinsServerEntry *copy = [[[self class] allocWithZone:zone] init];

    copy->_username = [[self username] copy];
    copy->_password = [[self password] copy];
    copy->_URLString = [[self URLString] copy];
    copy->_predicateIdentifier = [[self predicateIdentifier] copy];
    
    return copy;
}

#pragma mark -

-(NSString*)encodedString:(NSString*)string {
    NSString *encodedString = [string stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    if(encodedString == nil) encodedString = @"";
    return encodedString;
}

-(NSString*)encodedAsString {
    NSString *encodedUsername = [self encodedString:_username];
    NSString *encodedPassword = [self encodedString:_password];
    NSString *encodedURL = [self encodedString:_URLString];
    NSString *encodedPredicate = [self encodedString:_predicateIdentifier];
    
    NSArray *concatenatedElements = @[encodedUsername, encodedPassword, encodedURL, encodedPredicate];
    NSString *encodedString = [concatenatedElements componentsJoinedByString:JNUserSettingsSeperatorForKeychain];
        
    return encodedString;
}

-(NSURL*)baseURL {
    NSURL *baseURL = nil;
    
    if([[self URLString] length] > 0) {
        baseURL = [NSURL URLWithString:[self URLString]];
    }
    
    return baseURL;
}

-(NSURL*)serverURLWithAdditionalPathComponents:(NSString*)pathComponents
{
    NSURL *serverURL = [self baseURL];
    
    if([pathComponents length] > 0) {
        if([[serverURL absoluteString] hasSuffix:pathComponents] == NO) {
            serverURL = [serverURL URLByAppendingPathComponent:pathComponents];
        }
    }
    
    return serverURL;
}

NS_INLINE BOOL isNil(NSObject* object) {
    return (object == nil);
}

-(BOOL)hasURLString:(NSString*)URLString {
    return ((isNil([self URLString]) && isNil(URLString)) || [[self URLString] isEqualToString:URLString]);
}

-(BOOL)hasUsername:(NSString*)username {
    return ((isNil([self username]) && isNil(username)) || [[self username] isEqualToString:username]);
}

-(BOOL)hasPassword:(NSString*)password {
    return ((isNil([self password]) && isNil(password)) || [[self password] isEqualToString:password]);
}

-(BOOL)hasPredicate:(NSString*)predicate {
    return ((isNil([self predicateIdentifier]) && isNil(predicate)) || [[self predicateIdentifier] isEqualToString:predicate]);
}

-(BOOL)isEqual:(JNJenkinsServerEntry*)object {
    BOOL isEqual =  [self hasURLString:[object URLString]] &&
                    [self hasUsername:[object username]] &&
                    [self hasPassword:[object password]] &&
                    [self hasPredicate:[object predicateIdentifier]];
    
    return isEqual;
}

-(BOOL)isEqualTo:(id)object
{
    return [self isEqual:object];
}

-(BOOL)hasAuthenticationDetails {
    return ([[self username] length] > 0) || ([[self password] length] > 0);
}

-(NSString*)description {
    return [NSString stringWithFormat:@"URL:%@, username:%@, password:%@, predicate: %@", [self URLString], [self username], [self password], [self predicateIdentifier]];
}

@end


@implementation JNJenkinsServerEntry (Creation)

+(instancetype)serverEntryWithEncodedString:(NSString*)string
{
    return [[self alloc] initWithEncodedString:string];
}

-(id)init
{
    self = [super init];
    
    [self setUsername:@""];
    [self setURLString:@""];
    [self setPassword:@""];
    [self setPredicateIdentifier:JNJenkinsServerEntryDefaultNoPredicateIdentifier];
    
    return self;
}

-(id)initWithEncodedString:(NSString *)string
{
    self = [self init];
    
    if(string) {
        NSArray *separatedElements = [string componentsSeparatedByString:JNUserSettingsSeperatorForKeychain];
        
        NSString *encodedUsername = [separatedElements objectAtIndexIfNotOutOfBounds:JNEncodedServerSettingsIndexUsername];
        if(encodedUsername) {
            encodedUsername = [encodedUsername stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            [self setUsername:encodedUsername];
        }
        
        NSString *encodedPassword = [separatedElements objectAtIndexIfNotOutOfBounds:JNEncodedServerSettingsIndexPassword];
        encodedPassword = [encodedPassword stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        if(encodedPassword) {
            [self setPassword:encodedPassword];
        }
        
        NSString *encodedURL = [separatedElements objectAtIndexIfNotOutOfBounds:JNEncodedServerSettingsIndexURL];
        encodedURL = [encodedURL stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        if(encodedURL) {
            [self setURLString:encodedURL];
        }
        
        NSString *encodedPredicate = [separatedElements objectAtIndexIfNotOutOfBounds:JNEncodedServerSettingsIndexPredicate];
        encodedPredicate = [encodedPredicate stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        if(encodedPredicate == nil) {
            encodedPredicate = JNJenkinsServerEntryDefaultNoPredicateIdentifier;
        }
        if(encodedPredicate) {
            [self setPredicateIdentifier:encodedPredicate];
        }
    }
    
    return self;
}

-(nonnull instancetype)initWithURLString:(nonnull NSString*)URLString {
    self = [self init];
    if (self == nil) { return nil; }
    
    self.URLString = [URLString copy];
    
    return self;
    
}

@end


@implementation JNJenkinsServerEntry (Copying)

-(nonnull instancetype)copyWithURLString:(nonnull NSString*)URLString {
    JNJenkinsServerEntry *copy = [self copy];
    
    copy.URLString = URLString;
    
    return copy;
}

-(nonnull instancetype)copyWithUsername:(nonnull NSString*)username {
    JNJenkinsServerEntry *copy = [self copy];
    
    copy.username = username;
    
    return copy;
}

-(nonnull instancetype)copyWithPassword:(nonnull NSString*)password {
    JNJenkinsServerEntry *copy = [self copy];
    
    copy.password = password;
    
    return copy;
}

-(nonnull instancetype)copyWithPredicateIdentifier:(nonnull NSString*)identifier {
    JNJenkinsServerEntry *copy = [self copy];
    
    copy.predicateIdentifier = identifier;
    
    return copy;
}


@end