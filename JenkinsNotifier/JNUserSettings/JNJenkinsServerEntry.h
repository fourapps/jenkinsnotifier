//
//  Created by Developer on 20/04/2013.
//  Copyright (c) 2013 Four Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JNJenkinsServerEntry : NSObject 

@property (nonatomic, copy, readonly, nonnull) NSString *URLString;
@property (nonatomic, copy, readonly, nonnull) NSString *username;
@property (nonatomic, copy, readonly, nonnull) NSString *password;
@property (nonatomic, copy, readonly, nonnull) NSString *predicateIdentifier;

-(nonnull NSString*)encodedAsString;

-(nullable NSURL*)baseURL;

-(nullable NSURL*)serverURLWithAdditionalPathComponents:(nonnull NSString*)pathComponents;

-(BOOL)isEqual:(nullable id)object;

-(BOOL)hasAuthenticationDetails;

@end


@interface JNJenkinsServerEntry (Creation)

-(nonnull instancetype)initWithEncodedString:(nonnull NSString *)string;

-(nonnull instancetype)initWithURLString:(nonnull NSString*)URLString;

+(nonnull instancetype)serverEntryWithEncodedString:(nonnull NSString*)string;

@end

@interface JNJenkinsServerEntry (Copying)

-(nonnull instancetype)copyWithURLString:(nonnull NSString*)URLString;
-(nonnull instancetype)copyWithUsername:(nonnull NSString*)username;
-(nonnull instancetype)copyWithPassword:(nonnull NSString*)password;
-(nonnull instancetype)copyWithPredicateIdentifier:(nonnull NSString*)dentifier;

@end