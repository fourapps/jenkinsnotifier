//
//  Created by Michael May on 12/10/2012.
//  Copyright (c) 2012 Michael May. All rights reserved.
//

#import "JNUserSettings.h"
#import "JNUserSettings+Accessors.h"
#import "JNUserSettingsPersistentStoreProtocol.h"

#import "JNJenkinsServerEntries.h"
#import "JNJenkinsServerEntry.h"

#import "SSKeychain.h"
#import "NSArray+ObjectAtIndexIfNotOutOfBounds.h"

@interface JNUserSettings ()
@property (nonatomic, strong, readonly) id<JNUserSettingsPersistentStoreProtocol> userSettingsStore;
@end

@implementation JNUserSettings

const NSTimeInterval JNJenkinsAPIJobTimeOutDefault = 5;
const NSInteger kDefaultJenkinsPollPeriodSeconds = (5 * 60);
NSString* const kJenkinsPollPeriodSecondsKey = @"JENKINS_POLL_PERIOD_SECONDS";
NSString* const kJenkinsPollURLKey = @"JENKINS_POLL_URL";
NSString* const kShouldUseNotificationsKey =@"SHOULD_USE_NOTIFICATIONS";

static NSString *JNUserSettingsServiceNameForKeychain = @"com.wordpress.fourapps.JenkinsNotifier";

@dynamic jenkinsPollPeriodSeconds;
@dynamic shouldUseNotifications;

#pragma mark - Migration

-(NSArray*)jenkinsPollURLArrayInUserSettings {
    NSArray *jenkinsPollURLArray = [[self userSettingsStore]  arrayForKey:(NSString*)kJenkinsPollURLKey];
    return jenkinsPollURLArray;
}

-(void)removeJenkinsPollURLArrayInUserSettings {
    [[self userSettingsStore]  removeObjectForKey:(NSString*)kJenkinsPollURLKey];
}

-(void)migrateFromUserSettingsToKeychain {
    NSArray *serversURLsInUserSettings = [self jenkinsPollURLArrayInUserSettings];
    
    for(NSString *jenkinsPolllURL in serversURLsInUserSettings) {
        JNJenkinsServerEntry *serverEntry = [[JNJenkinsServerEntry alloc] initWithURLString:jenkinsPolllURL];
        
        [self addServerEntry:serverEntry];
    }
    
    [self removeJenkinsPollURLArrayInUserSettings];
}

-(void)migrateSingleJenkinsPollURLToArray {
    NSObject *URLStore = [[self userSettingsStore]  objectForKey:(NSString*)kJenkinsPollURLKey];
    
    if([URLStore isKindOfClass:[NSString class]]) {
        [[self userSettingsStore]  removeObjectForKey:(NSString*)kJenkinsPollURLKey];
        
        NSArray *newArrayWithURLStore = [NSArray arrayWithObject:URLStore];
        
        [[self userSettingsStore]  setObject:newArrayWithURLStore forKey:(NSString*)kJenkinsPollURLKey];
    }
}

#pragma mark - Keychain

//  notes:
//  service = com.wordpress.fourapps.JenkinsNotifier
//  account = jenkind poll url
//  password = username:password:filterid packed together

-(NSString*)accountForIndex:(NSInteger)index {
    return [NSString stringWithFormat:@"%ld", (long)index];
}

-(NSInteger)nextIndex {
    NSArray *accounts = [self allKeychainEntriesForApplication];
    return [accounts count];
}

#pragma mark - 

-(NSArray*)allKeychainEntriesForApplication {
    NSArray *allAccounts = [SSKeychain accountsForService:JNUserSettingsServiceNameForKeychain];

    return allAccounts;
}

-(JNJenkinsServerEntries*)allServerEntries {
    NSArray *allAccounts = [self allKeychainEntriesForApplication];
    JNJenkinsServerEntries *allServerEntries = [JNJenkinsServerEntries serverEntries];

    for(NSDictionary *keychainEntry in allAccounts) {
        NSString *account = [keychainEntry objectForKey:kSSKeychainAccountKey];
        NSString *serverEntryEncodedAsAString = [SSKeychain passwordForService:JNUserSettingsServiceNameForKeychain account:account];
        
        JNJenkinsServerEntry *entryForAccount = [JNJenkinsServerEntry serverEntryWithEncodedString:serverEntryEncodedAsAString];
        
        [allServerEntries addServerEntry:entryForAccount];
    }
    
    return allServerEntries;
}

-(void)setServerEntries:(JNJenkinsServerEntries*)serverEntries {
    [self deleteAllServerEntries];
    
    for(JNJenkinsServerEntry *serverEntry in serverEntries) {
        [self addServerEntry:serverEntry];
    }
}

-(JNJenkinsServerEntry*)serverEntryAtIndex:(NSInteger)index {
    NSArray *allAccounts = [self allKeychainEntriesForApplication];
    NSDictionary *keychainEntry = [allAccounts objectAtIndexIfNotOutOfBounds:index];
    
    if(keychainEntry) {
        NSString *account = [keychainEntry objectForKey:kSSKeychainAccountKey];
        NSString *serverEntryEncodedAsAString = [SSKeychain passwordForService:JNUserSettingsServiceNameForKeychain account:account];
        
        JNJenkinsServerEntry *entryAtIndex = [JNJenkinsServerEntry serverEntryWithEncodedString:serverEntryEncodedAsAString];
        
        return entryAtIndex;
    }
    
    return nil;
}

-(void)setSetServerEntry:(JNJenkinsServerEntry*)entry atIndex:(NSInteger)index
{
    NSString *account = [self accountForIndex:index];
    NSString *serverEntryEncodedAsAString = [entry encodedAsString];
    
    [SSKeychain setPassword:serverEntryEncodedAsAString
                 forService:JNUserSettingsServiceNameForKeychain
                    account:account];
}

-(void)addServerEntry:(JNJenkinsServerEntry*)entry {
    NSInteger nextEntryIndex = [self nextIndex];
    
    [self setSetServerEntry:entry atIndex:nextEntryIndex];
}

-(NSInteger)totalNumberOfServerEntries {
    return [[self allKeychainEntriesForApplication] count];
}

-(void)deleteAllServerEntries {
    NSArray *allAccounts = [self allKeychainEntriesForApplication];

    for(NSDictionary *account in allAccounts) {
        [SSKeychain deletePasswordForService:JNUserSettingsServiceNameForKeychain account:[account objectForKey:kSSKeychainAccountKey]];
    }
}

#pragma mark - Poll URLs

-(NSUInteger)numberOfJenkinsPollURLEntries {
    return [self totalNumberOfServerEntries];
}

-(NSString*)jenkinsPollURLAtIndex:(NSInteger)index {
    JNJenkinsServerEntry *entryAtIndex = [self serverEntryAtIndex:index];
    return [entryAtIndex URLString];
}

-(void)setJenkinsPollURL:(NSString *)jenkinsPollURL atIndex:(NSInteger)index {
    if(jenkinsPollURL) {
        JNJenkinsServerEntry *entryAtIndex = [self serverEntryAtIndex:index];
        
        if(entryAtIndex) {
            JNJenkinsServerEntry *updatedEntry = [entryAtIndex copyWithURLString:jenkinsPollURL];
            [self setSetServerEntry:updatedEntry atIndex:index];
        }
    }
}

-(void)addJenkinsPollURL:(NSString*)jenkinsPollURL {
    if(jenkinsPollURL) {
        JNJenkinsServerEntry *newEntry = [[JNJenkinsServerEntry alloc] initWithURLString:jenkinsPollURL];

        [self addServerEntry:newEntry];
    }
}

-(NSArray*)jenkinsPollURLArray {
    JNJenkinsServerEntries* allServerEntries = [self allServerEntries];
    NSMutableArray *allPollURLEntries = [[NSMutableArray alloc] initWithCapacity:[allServerEntries count]];
    
    for(JNJenkinsServerEntry *entry in allServerEntries) {
        [allPollURLEntries addObject:[entry URLString]];
    }

    return allPollURLEntries;
}

-(void)setJenkinsPollURLArray:(NSArray*)serverPollURLs {
    [self deleteAllServerEntries];

    if(serverPollURLs) {
        for(NSString* serverURL in serverPollURLs) {
            JNJenkinsServerEntry *serverEntry = [[JNJenkinsServerEntry alloc] initWithURLString:serverURL];

            [self addServerEntry:serverEntry];
        }
    }
}

#pragma mark -

-(NSInteger)jenkinsPollPeriodSeconds {
    NSNumber *jenkinsPollPeriodNumber = [[self userSettingsStore]  objectForKey:(NSString*)kJenkinsPollPeriodSecondsKey];
    NSInteger jenkinsPollPeriodInteger = kDefaultJenkinsPollPeriodSeconds;
    
    if(jenkinsPollPeriodNumber == nil) {
        [self setJenkinsPollPeriodSeconds:kDefaultJenkinsPollPeriodSeconds];
        jenkinsPollPeriodInteger = kDefaultJenkinsPollPeriodSeconds;
    } else {
        jenkinsPollPeriodInteger = [jenkinsPollPeriodNumber integerValue];
    }
    
    return jenkinsPollPeriodInteger;
}

-(void)setJenkinsPollPeriodSeconds:(NSInteger)jenkinsPollPeriodSeconds {
    if(jenkinsPollPeriodSeconds < 0) {
        jenkinsPollPeriodSeconds = 0;
    }
    
    NSNumber *jenkinsPollPeriodNumber = [NSNumber numberWithInteger:jenkinsPollPeriodSeconds];
    
    [[self userSettingsStore]  setObject:jenkinsPollPeriodNumber forKey:(NSString*)kJenkinsPollPeriodSecondsKey];
}

#pragma mark - 

-(BOOL)shouldUseNotifications {
    NSNumber *shouldUseNotificationsObject = [[self userSettingsStore]  objectForKey:(NSString*)kShouldUseNotificationsKey];
    BOOL shouldUseNotifications = NO;   // apple demands the user choose to use notifications to be approved
    
    if(shouldUseNotificationsObject) {
        shouldUseNotifications = [shouldUseNotificationsObject boolValue];
    }
    
    return shouldUseNotifications;
}

-(void)setShouldUseNotifications:(BOOL)shouldUseNotifications {
    NSNumber *shouldUseNotificationsObject = [NSNumber numberWithBool:shouldUseNotifications];
    
    [[self userSettingsStore]  setObject:shouldUseNotificationsObject forKey:(NSString*)kShouldUseNotificationsKey];
}

#pragma mark - -(NSTimeInterval)serverTimeoutSeconds;

// Set: defaults write com.wordpress.fourapps.JenkinsNotifier serverTimeout -int 10
// Delete: defaults delete com.wordpress.fourapps.JenkinsNotifier serverTimeout
-(NSTimeInterval)serverTimeoutSeconds
{
    NSTimeInterval timeout = JNJenkinsAPIJobTimeOutDefault;

    NSNumber *serverTimeoutSecondsNumber = [[self userSettingsStore]  objectForKey:@"serverTimeout"];
    if(serverTimeoutSecondsNumber) {
        timeout = [serverTimeoutSecondsNumber integerValue];
        NSLog(@"Using custom com.wordpress.fourapps.JenkinsNotifier serverTimeout of %f seconds", timeout);
    }
    
    return timeout;
}

#pragma mark - notifify on fail only

NSString* const kNotifyOnFailedJobsOnlyKey = @"NOTIFY_ON_FAILED_JOBS_ONLY";

-(void)setNotifyOnFailedJobsDefault
{
    if([self hasEntryForKey:kNotifyOnFailedJobsOnlyKey] == NO) {
        [self setBool:YES forKey:kNotifyOnFailedJobsOnlyKey];
    }
}

-(void)setNotifyOnFailedJobsOnly:(BOOL)notifyOnFailedJobsOnly
{
    [self setBool:notifyOnFailedJobsOnly forKey:kNotifyOnFailedJobsOnlyKey];
}

-(BOOL)notifyOnFailedJobsOnly
{
    BOOL notifyOnFailedJobsOnly = [self boolForKey:kNotifyOnFailedJobsOnlyKey];
    
    return notifyOnFailedJobsOnly;
}

#pragma mark -

-(void)reset {
    [self deleteAllServerEntries];
    
    [[self userSettingsStore] removeObjectForKey:kJenkinsPollPeriodSecondsKey];
    [[self userSettingsStore] removeObjectForKey:kJenkinsPollURLKey];
    [[self userSettingsStore] removeObjectForKey:kShouldUseNotificationsKey];
    [[self userSettingsStore] removeObjectForKey:kNotifyOnFailedJobsOnlyKey];
}

#pragma mark -

-(id)initWithUserSettingsStore:(id<JNUserSettingsPersistentStoreProtocol>)settingsStore {
    self = [super init];
    
    if(self) {
        _userSettingsStore = settingsStore;
        [self migrateSingleJenkinsPollURLToArray];
        [self setNotifyOnFailedJobsDefault];
        
//#define RESET_ALL_FOR_FRESH_INSTALL_EMULATION
#ifdef RESET_ALL_FOR_FRESH_INSTALL_EMULATION
#pragma message("resetting all use defaults at startup for testing")        
        [self reset];
#endif
    }
    
    return self;
}

@end
