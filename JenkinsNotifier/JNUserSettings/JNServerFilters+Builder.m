//
//  Created by Developer on 2014/02/16.
//  Copyright (c) 2014 Four Apps. All rights reserved.
//

#import "MMDebugMacros.h"

#import "JNServerFilters+Builder.h"

@implementation JNServerFilters (Builder)

#pragma mark - loading filters

+(NSDictionary *)defaultFilters
{
    NSURL *filtersPListBundleURL = [[NSBundle mainBundle] URLForResource:@"DefaultFilters" withExtension:@"plist"];
    DAssert(filtersPListBundleURL, @"default filters should not be nil");
    
    NSDictionary *jobFiltersOptions = [NSDictionary dictionaryWithContentsOfURL:filtersPListBundleURL];
    DAssert(jobFiltersOptions, @"found a DefaultFilters.plist file, but it could not be loaded");
    
    return jobFiltersOptions;
}

+(NSDictionary*)usersFilters
{
    NSMutableDictionary *userJobFilters = nil;
    NSError *error = nil;
    NSURL *userFiltersPathURL = [[NSFileManager defaultManager] URLForDirectory:NSApplicationSupportDirectory
                                                                       inDomain:NSUserDomainMask
                                                              appropriateForURL:nil
                                                                         create:NO
                                                                          error:&error];
    
    NSURL *userFiltersPlistPathURL = [userFiltersPathURL URLByAppendingPathComponent:@"UserFilters.plist" isDirectory:NO];
    
    if(userFiltersPlistPathURL) {
        userJobFilters = [NSMutableDictionary dictionaryWithContentsOfURL:userFiltersPlistPathURL];
    }
    
    return userJobFilters;
}

#pragma mark -

+(instancetype)serverFilters
{
    NSDictionary *defaultJobFilters = [self defaultFilters];
    NSDictionary *userJobFilters = [self usersFilters];
    
    return [[self alloc] initWithDefaultFilters:defaultJobFilters
                                    userFilters:userJobFilters];
}

@end
