//
//  Created by Developer on 08/09/2013.
//  Copyright (c) 2013 Four Apps. All rights reserved.
//

#import "JenkinsNotifier-Swift.h"

#import "JNLocalNotificationManager.h"
#import "JNJobStatusMappers.h"

@implementation JNLocalNotificationManager

#pragma mark - NSUserNotificationCenter

const NSString* kJNNotificationUserInfoURLKey = @"JNNotificationUserInfoURLKey";
const NSString* kJNNotificationUserInfoLastStatusKey = @"JNNotificationUserInfoLastStatusKey";

// Sent to the delegate when a user clicks on a user notification presented by the user notification center.
- (void)userNotificationCenter:(NSUserNotificationCenter *)center
       didActivateNotification:(NSUserNotification *)notification
{
    NSDictionary *userInfo = [notification userInfo];
    NSString *URLString = [userInfo objectForKey:kJNNotificationUserInfoURLKey];
    
    if(URLString) {
        NSURL *URL = [NSURL URLWithString:URLString];
        [[NSWorkspace sharedWorkspace] openURL:URL];
    }
}

- (BOOL)userNotificationCenter:(NSUserNotificationCenter *)center
     shouldPresentNotification:(NSUserNotification *)notification
{
    return YES;
}

- (NSUserNotification *)deliverNotificationForJob:(JNJenkinsAPIJob*)job
{
    NSUserNotification *notification = [[NSUserNotification alloc] init];
    
    notification.title = [job name];
    
    NSString *informativeText = [[self class] informativeTextForNotificationFromJob:job];
    notification.informativeText = informativeText;
    
    notification.soundName = NSUserNotificationDefaultSoundName;
    
    NSDictionary *userInfo = [[self  class] userInfoForNotificationFromJob:job];
    notification.userInfo = userInfo;
    
    [[NSUserNotificationCenter defaultUserNotificationCenter] deliverNotification:notification];
    
    return notification;
}

-(void)clearNotification:(NSUserNotification*)notification
{
    if(notification) {
        if([notification isPresented]) {
            [[NSUserNotificationCenter defaultUserNotificationCenter] removeDeliveredNotification:notification];
        } else {
            [[NSUserNotificationCenter defaultUserNotificationCenter] removeScheduledNotification:notification];
        }
    }
}

-(void)clearAllNotifications
{
    [[NSUserNotificationCenter defaultUserNotificationCenter] removeAllDeliveredNotifications];
}

#pragma mark - utility

+(NSString*)informativeTextForNotificationFromJob:(JNJenkinsAPIJob*)job
{
    return [JNJobStatusMappers notificiationTextForJobState:[job status]];
}

+(NSDictionary*)userInfoForNotificationFromJob:(JNJenkinsAPIJob*)job
{
    NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] init];
    
    NSString *URL = [job URL];
    [userInfo setObject:URL forKey:kJNNotificationUserInfoURLKey];
    
    NSNumber *jobStatusNumber = [NSNumber numberWithInteger:[job status]];
    [userInfo setObject:jobStatusNumber forKey:kJNNotificationUserInfoLastStatusKey];
    
    return userInfo;
}

-(NSInteger)jobStatusFromUserInfo:(NSDictionary*)userInfo
{
    NSInteger jobStatus = NSNotFound;
    NSNumber *jobStatusNumber = [userInfo objectForKey:kJNNotificationUserInfoLastStatusKey];
    
    if(jobStatusNumber) {
        jobStatus = [jobStatusNumber integerValue];
    }
    
    return jobStatus;
}

#pragma mark - 

-(id)init
{
    self = [super init];
    
    if(self) {
        [[NSUserNotificationCenter defaultUserNotificationCenter] setDelegate:self];
        
        [self clearAllNotifications];
    }
    
    return self;
}

-(void)dealloc
{
    [self clearAllNotifications];
    
    [[NSUserNotificationCenter defaultUserNotificationCenter] setDelegate:nil];
}

#pragma mark - 

+(instancetype)localNotificationManager
{
    return [[self alloc] init];
}

@end
