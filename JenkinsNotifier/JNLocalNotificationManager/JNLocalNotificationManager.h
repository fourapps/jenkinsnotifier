//
//  Created by Developer on 08/09/2013.
//  Copyright (c) 2013 Four Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "JNJobNotificationDelegate.h"

@interface JNLocalNotificationManager : NSObject <NSUserNotificationCenterDelegate, JNJobNotificationDelegate>

+(instancetype)localNotificationManager;

@end
