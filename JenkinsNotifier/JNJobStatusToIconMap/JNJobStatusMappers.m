//
//  Created by Michael May on 09/03/2013.
//  Copyright (c) 2013 Four Apps. All rights reserved.
//

#import "JenkinsNotifier-Swift.h"

#import "NSApplication+MMExtensions.h"

#import "JNJobStatusMappers.h"

@implementation JNJobStatusMappers

static NSDictionary *JNJobStatusToIconMapDictionary;
static NSDictionary *JNJobStatusToToolTipMapDictionary;
static NSDictionary *JNJobStatusToNotificationTextMapDictionary;

+(void)initialize
{
    JNJobStatusToIconMapDictionary = @{
            @(JNJenkinsAPIJobStatesJNJenkinsAPIJobStateUnknown) : @"menu-statusbar16",
            [NSNumber numberWithInteger:JNJenkinsAPIJobStatesJNJenkinsAPIJobStateDisabled]: @"build-disabled16",
            [NSNumber numberWithInteger:JNJenkinsAPIJobStatesJNJenkinsAPIJobStateSuccess]: @"build-success16",
            [NSNumber numberWithInteger:JNJenkinsAPIJobStatesJNJenkinsAPIJobStateUnstable]: @"build-unstable16",
            [NSNumber numberWithInteger:JNJenkinsAPIJobStatesJNJenkinsAPIJobStatePending]: @"build-ongoing16",
            [NSNumber numberWithInteger:JNJenkinsAPIJobStatesJNJenkinsAPIJobStateNotBuilt]: @"build-disabled16",
            [NSNumber numberWithInteger:JNJenkinsAPIJobStatesJNJenkinsAPIJobStateInProgress]: @"build-ongoing16",
            [NSNumber numberWithInteger:JNJenkinsAPIJobStatesJNJenkinsAPIJobStateAborted]: @"build-disabled16",
            [NSNumber numberWithInteger:JNJenkinsAPIJobStatesJNJenkinsAPIJobStateFailed]: @"build-failed16",
            [NSNumber numberWithInteger:JNJenkinsAPIJobStatesJNJenkinsAPIJobStateConnectionFailure]: @"connection-failed16",
            };
    
    JNJobStatusToToolTipMapDictionary = @{
           [NSNumber numberWithInteger:JNJenkinsAPIJobStatesJNJenkinsAPIJobStateUnknown]: NSLocalizedString(@"JNJenkinsAPIJobStateUnknown", @""),
           [NSNumber numberWithInteger:JNJenkinsAPIJobStatesJNJenkinsAPIJobStateDisabled]: NSLocalizedString(@"JNJenkinsAPIJobStateDisabled", @""),
           [NSNumber numberWithInteger:JNJenkinsAPIJobStatesJNJenkinsAPIJobStateSuccess]: NSLocalizedString(@"JNJenkinsAPIJobStateSuccess", @""),
           [NSNumber numberWithInteger:JNJenkinsAPIJobStatesJNJenkinsAPIJobStateUnstable]: NSLocalizedString(@"JNJenkinsAPIJobStateUnstable", @""),
           [NSNumber numberWithInteger:JNJenkinsAPIJobStatesJNJenkinsAPIJobStatePending]: NSLocalizedString(@"JNJenkinsAPIJobStatePending", @""),
           [NSNumber numberWithInteger:JNJenkinsAPIJobStatesJNJenkinsAPIJobStateNotBuilt]: NSLocalizedString(@"JNJenkinsAPIJobStateNotBuilt", @""),
           [NSNumber numberWithInteger:JNJenkinsAPIJobStatesJNJenkinsAPIJobStateInProgress]: NSLocalizedString(@"JNJenkinsAPIJobStateInProgress", @""),
           [NSNumber numberWithInteger:JNJenkinsAPIJobStatesJNJenkinsAPIJobStateAborted]: NSLocalizedString(@"JNJenkinsAPIJobStateAborted", @""),
           [NSNumber numberWithInteger:JNJenkinsAPIJobStatesJNJenkinsAPIJobStateFailed]: NSLocalizedString(@"JNJenkinsAPIJobStateFailed", @""),
           [NSNumber numberWithInteger:JNJenkinsAPIJobStatesJNJenkinsAPIJobStateConnectionFailure]: NSLocalizedString(@"BuildConnectionFailureText", @""),
           };
    
    JNJobStatusToNotificationTextMapDictionary = @{
       [NSNumber numberWithInteger:JNJenkinsAPIJobStatesJNJenkinsAPIJobStateUnknown]: NSLocalizedString(@"BuildUnknownText", @""),
       [NSNumber numberWithInteger:JNJenkinsAPIJobStatesJNJenkinsAPIJobStateDisabled]: NSLocalizedString(@"BuildDisabledText", @""),
       [NSNumber numberWithInteger:JNJenkinsAPIJobStatesJNJenkinsAPIJobStateSuccess]: NSLocalizedString(@"BuildSucceededText", @""),
       [NSNumber numberWithInteger:JNJenkinsAPIJobStatesJNJenkinsAPIJobStateUnstable]: NSLocalizedString(@"BuildUnstableText", @""),
       [NSNumber numberWithInteger:JNJenkinsAPIJobStatesJNJenkinsAPIJobStatePending]: NSLocalizedString(@"BuildPendingText", @""),
       [NSNumber numberWithInteger:JNJenkinsAPIJobStatesJNJenkinsAPIJobStateNotBuilt]: NSLocalizedString(@"BuildNotBuiltText", @""),
       [NSNumber numberWithInteger:JNJenkinsAPIJobStatesJNJenkinsAPIJobStateInProgress]: NSLocalizedString(@"BuildInProgressText", @""),
       [NSNumber numberWithInteger:JNJenkinsAPIJobStatesJNJenkinsAPIJobStateAborted]: NSLocalizedString(@"BuildAbortedText", @""),
       [NSNumber numberWithInteger:JNJenkinsAPIJobStatesJNJenkinsAPIJobStateFailed]: NSLocalizedString(@"BuildFailedText", @""),
       [NSNumber numberWithInteger:JNJenkinsAPIJobStatesJNJenkinsAPIJobStateConnectionFailure]: NSLocalizedString(@"JNJenkinsAPIJobStateConnectionFailure", @""),
    };
}

+(NSString*)iconImageNameForJobState:(JNJenkinsAPIJobStates)state
{
    NSString *imageName = [JNJobStatusToIconMapDictionary objectForKey:[NSNumber numberWithInteger:state]];

    return imageName;
}

+(NSImage*)jobImageimageForState:(JNJenkinsAPIJobStates)state
{
    NSImage *jobImage = nil;
    
    NSString *imageName = [JNJobStatusMappers iconImageNameForJobState:state];
    if(imageName) {
        jobImage = [NSImage imageNamed:imageName];
    }
    
    return jobImage;
}

+(NSString*)toolTipTextForJobState:(JNJenkinsAPIJobStates)state
{
    NSString *stateName = [JNJobStatusToToolTipMapDictionary objectForKey:[NSNumber numberWithInteger:state]];
    NSString *appname = [NSApplication appName];
    NSString *appVersion = [NSApplication appVersionString];
    
    NSString *toolTip = [NSString stringWithFormat:@"%@ %@\n%@:%@",
                         appname,
                         appVersion,
                         NSLocalizedString(@"LastState", @""),
                         stateName];
    
    return toolTip;
}

+(NSString*)notificiationTextForJobState:(JNJenkinsAPIJobStates)state
{
    NSString *notificationText = [JNJobStatusToNotificationTextMapDictionary objectForKey:[NSNumber numberWithInteger:state]];
    return notificationText;
}
@end


JNJenkinsAPIJobStates mostImportantState(JNJenkinsAPIJobStates currentState, JNJenkinsAPIJobStates newState)
{
    if(newState > currentState) {
        return newState;
    }
    
    return currentState;
}
