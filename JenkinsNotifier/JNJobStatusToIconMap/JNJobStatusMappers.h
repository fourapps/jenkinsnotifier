//
//  Created by Michael May on 09/03/2013.
//  Copyright (c) 2013 Four Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, JNJenkinsAPIJobStates);

@interface JNJobStatusMappers : NSObject

+(NSString*)iconImageNameForJobState:(JNJenkinsAPIJobStates)state;
+(NSImage*)jobImageimageForState:(JNJenkinsAPIJobStates)state;

+(NSString*)toolTipTextForJobState:(JNJenkinsAPIJobStates)state;
+(NSString*)notificiationTextForJobState:(JNJenkinsAPIJobStates)state;

@end

extern JNJenkinsAPIJobStates mostImportantState(JNJenkinsAPIJobStates currentState, JNJenkinsAPIJobStates newState);

