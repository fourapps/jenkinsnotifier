//
//  Created by Developer on 2013/12/30.
//  Copyright (c) 2013 Four Apps. All rights reserved.
//

#import "JNJenkinsAutoDiscovery.h"

@interface JNJenkinsAutoDiscovery (Builder)

+(instancetype)jenkinsAutoDiscoveryWithDelegate:(id<JNJenkinsAutoDiscoveryProtocol>)delegate;

@end
