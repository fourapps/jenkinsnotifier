//
//  Created by Michael May on 2013/12/29.
//  Copyright (c) 2013 Four Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GCDAsyncUdpSocket;

@protocol JNJenkinsAutoDiscoveryProtocol
@required
-(void)autoDiscoveryDidBegin;
-(void)autoDiscoveryDidSucceedWithURL:(NSString*)URLString;
-(void)autoDiscoveryDidFailWithError:(NSError*)error;
-(void)autoDiscoveryDidEnd;
@end

@interface JNJenkinsAutoDiscovery : NSObject

-(void)startDiscovery;

-(instancetype)initWithDelegate:(id<JNJenkinsAutoDiscoveryProtocol>)delegate
                      UDPSocket:(GCDAsyncUdpSocket*)UDPSocket;

@end


