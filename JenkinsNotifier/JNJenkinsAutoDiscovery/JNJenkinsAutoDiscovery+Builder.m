//
//  Created by Developer on 2013/12/30.
//  Copyright (c) 2013 Four Apps. All rights reserved.
//

#import "JNJenkinsAutoDiscovery+Builder.h"

#import "JNJenkinsAutoDiscovery.h"
#import "GCDAsyncUdpSocket.h"

@implementation JNJenkinsAutoDiscovery (Builder)

+(instancetype)jenkinsAutoDiscoveryWithDelegate:(id<JNJenkinsAutoDiscoveryProtocol>)delegate
{
    dispatch_queue_t queue = dispatch_queue_create("com.wordpress.fourapps.jenkinsautodiscovery", DISPATCH_QUEUE_SERIAL);
    GCDAsyncUdpSocket *UDPSocket = [[GCDAsyncUdpSocket alloc] initWithDelegate:nil delegateQueue:queue];

    JNJenkinsAutoDiscovery *instance = [[self alloc] initWithDelegate:delegate UDPSocket:UDPSocket];
    [UDPSocket setDelegate:instance];
    
    return instance;
}

@end
