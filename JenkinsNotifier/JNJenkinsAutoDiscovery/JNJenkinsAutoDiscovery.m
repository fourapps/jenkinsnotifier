//
//  Created by Michael May on 2013/12/29.
//  Copyright (c) 2013 Four Apps. All rights reserved.
//

#import "JNJenkinsAutoDiscovery.h"

#import "GCDAsyncUdpSocket.h"
#import "XMLDictionary.h"

@interface JNJenkinsAutoDiscovery () <GCDAsyncUdpSocketDelegate>
@property (nonatomic, weak, readonly) id<JNJenkinsAutoDiscoveryProtocol> delegate;
@property (nonatomic, strong, readonly) GCDAsyncUdpSocket *UDPSocket;
@end

@implementation JNJenkinsAutoDiscovery

static const NSUInteger JenkinsUDPMulticastBroadcastPort = 33848;
static NSString *JenkinsUDPMulticastPacketAddress = @"239.77.124.213";

#pragma mark -

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didNotConnect:(NSError *)error
{
    __strong id<JNJenkinsAutoDiscoveryProtocol> delegate = [self delegate];
    [delegate autoDiscoveryDidFailWithError:error];
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didNotSendDataWithTag:(long)tag dueToError:(NSError *)error
{
    __strong id<JNJenkinsAutoDiscoveryProtocol> delegate = [self delegate];
    [delegate autoDiscoveryDidFailWithError:error];
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didSendDataWithTag:(long)tag
{
    __strong id<JNJenkinsAutoDiscoveryProtocol> delegate = [self delegate];
    [delegate autoDiscoveryDidBegin];
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock
   didReceiveData:(NSData *)data
      fromAddress:(NSData *)address
withFilterContext:(id)filterContext
{
    NSDictionary *jenkinsResponse = [NSDictionary dictionaryWithXMLData:data];
    
    if(jenkinsResponse) {
        // TODO: verify that it is a jenkins response
        NSString *version = [jenkinsResponse objectForKey:@"version"];
        if(version == nil) {
            // error: got a response, but it does not look like a jenkins/hudson server
            return;
        }
        
        NSString *URLString = [jenkinsResponse objectForKey:@"url"];
        if(URLString == nil) {
            // error: found a server by there is no URL; have you configured the Jenkins URL in Manage Jenkins > Configure System
            return;
        }
        
        __strong id<JNJenkinsAutoDiscoveryProtocol> delegate = [self delegate];
        [delegate autoDiscoveryDidSucceedWithURL:URLString];
    }
}

- (void)udpSocketDidClose:(GCDAsyncUdpSocket *)sock withError:(NSError *)error
{
    __strong id<JNJenkinsAutoDiscoveryProtocol> delegate = [self delegate];
    [delegate autoDiscoveryDidEnd];
}

#pragma mark -

-(void)startDiscovery
{
    NSError *error = nil;
    
    [[self UDPSocket] bindToPort:0 error:&error];
    [[self UDPSocket] joinMulticastGroup:JenkinsUDPMulticastPacketAddress error:&error];

    [[self UDPSocket] beginReceiving:&error];
    
    NSData *data = [@"HelloJenkins" dataUsingEncoding:NSUTF8StringEncoding];
    NSTimeInterval timeoutSeconds = 10;
    [[self UDPSocket] sendData:data
                        toHost:JenkinsUDPMulticastPacketAddress
                          port:JenkinsUDPMulticastBroadcastPort
                   withTimeout:timeoutSeconds
                           tag:0];
    
    [self performSelector:@selector(endDiscovery)
                           withObject:nil
                           afterDelay:timeoutSeconds];
}

-(void)endDiscovery
{
    [[self UDPSocket] close];
}

#pragma mark - 

-(instancetype)initWithDelegate:(id<JNJenkinsAutoDiscoveryProtocol>)delegate
                      UDPSocket:(GCDAsyncUdpSocket*)UDPSocket
{
    self = [super init];
    
    if(self) {
        _delegate = delegate;
        
        _UDPSocket = UDPSocket;
    }
    
    return self;
}

-(void)dealloc
{
    [[self UDPSocket] close];
}

#pragma mark -

@end
