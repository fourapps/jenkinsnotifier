//
//  Created by Michael May on 30/12/2012.
//  Copyright (c) 2012 Michael May. All rights reserved.
//

#import "JenkinsNotifier-Swift.h"

#import "JNJenkinsJobsLog.h"

@interface JNJenkinsJobsLog ()
@property (nonatomic, strong, readonly) NSMutableDictionary *failedJobLog;
@end

@implementation JNJenkinsJobsLog

+(instancetype)jobsLog {
    return [[self alloc] init];
}

#pragma mark - 

-(NSString*)keyForJob:(JNJenkinsAPIJob*)job {
    NSString *jobKey = [job URL];
    return jobKey;
}

-(NSObject*)userInfoForJob:(JNJenkinsAPIJob*)job {
    NSString *jobKey = [self keyForJob:job];
    NSObject *userInfoForJob = [[self failedJobLog] objectForKey:jobKey];

    return userInfoForJob;
}

#pragma mark - 

-(BOOL)hasEntryForJob:(JNJenkinsAPIJob*)job {
    BOOL hasEntry = NO;
    
    if(job) {
        NSString *jobKey = [self keyForJob:job];
        NSObject *userInfoForJob = [[self failedJobLog] objectForKey:jobKey];
        hasEntry = (userInfoForJob != nil);
    }
    
    return hasEntry;
}

-(BOOL)setUserInfo:(NSObject*)userInfoForJob forJob:(JNJenkinsAPIJob*)job
{
    BOOL didLogJob = NO;
    
    if(job && userInfoForJob) {
        if([self hasEntryForJob:job] == NO) {
            NSString *jobKey = [self keyForJob:job];
            
            [[self failedJobLog] setObject:userInfoForJob forKey:jobKey];
                
            didLogJob = YES;
        }
    }
    
    return didLogJob;
}

-(NSObject*)removeUserInfoForJob:(JNJenkinsAPIJob*)job {
    NSObject *userInfoForJob = nil;
    
    if([self hasEntryForJob:job]) {
        NSString *jobKey = [self keyForJob:job];

        userInfoForJob = [self userInfoForJob:job];
        
        [[self failedJobLog] removeObjectForKey:jobKey];
    }
    
    return userInfoForJob;
}

#pragma mark - 

-(id)init {
    self = [super init];
    
    if(self) {
        _failedJobLog = [[NSMutableDictionary alloc] init];
    }
    
    return self;
}

@end
