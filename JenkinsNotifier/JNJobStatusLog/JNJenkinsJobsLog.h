//
//  Created by Michael May on 30/12/2012.
//  Copyright (c) 2012 Michael May. All rights reserved.
//

#import <Foundation/Foundation.h>

@class JNJenkinsAPIJob;

@interface JNJenkinsJobsLog : NSObject

+(instancetype)jobsLog;

-(BOOL)hasEntryForJob:(JNJenkinsAPIJob*)job;

// adds the given job to the log, if there is no entry yet
-(BOOL)setUserInfo:(NSObject*)userInfo forJob:(JNJenkinsAPIJob*)job;

-(NSObject*)removeUserInfoForJob:(JNJenkinsAPIJob*)job;

-(NSObject*)userInfoForJob:(JNJenkinsAPIJob*)job;

@end
