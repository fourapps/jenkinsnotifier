//
//  Created by Developer on 22/09/2013.
//  Copyright (c) 2013 Four Apps. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface NSEvent (KeyboardEventHelpers)

-(BOOL)isKeyDownEvent;
-(BOOL)isCommandKeyModifierEvent;
-(BOOL)isCommandShiftKeyModifierEvent;

-(BOOL)isCutKeyDown;
-(BOOL)isPasteKeyDown;
-(BOOL)isCopyKeyDown;
-(BOOL)isUndoKeyDown;
-(BOOL)isRedoKeyDown;
-(BOOL)isSelectAllKeyDown;

@end
