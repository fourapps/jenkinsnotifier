//
//  Created by Developer on 22/09/2013.
//  Copyright (c) 2013 Four Apps. All rights reserved.
//

#import "NSEvent+KeyboardEventHelpers.h"

@implementation NSEvent (KeyboardEventHelpers)

-(BOOL)isKeyDownEvent
{
    return ([self type] == NSKeyDown);
}

-(BOOL)isCommandKeyModifierEvent
{
    return (([self modifierFlags] & NSDeviceIndependentModifierFlagsMask) == NSCommandKeyMask);
}

-(BOOL)isCommandShiftKeyModifierEvent
{
    return (([self modifierFlags] & NSDeviceIndependentModifierFlagsMask) == (NSCommandKeyMask | NSShiftKeyMask));
}

-(BOOL)isKeyDownCharacter:(NSString*)key
{
    return ([[self charactersIgnoringModifiers] isEqualToString:key]);
}

-(BOOL)isCutKeyDown
{
    return [self isKeyDownCharacter:@"x"];
}

-(BOOL)isPasteKeyDown
{
    return [self isKeyDownCharacter:@"v"];
}

-(BOOL)isCopyKeyDown
{
    return [self isKeyDownCharacter:@"c"];
}

-(BOOL)isUndoKeyDown
{
    return [self isKeyDownCharacter:@"z"];
}

-(BOOL)isRedoKeyDown
{
    return [self isKeyDownCharacter:@"Z"];
}

-(BOOL)isSelectAllKeyDown
{
    return [self isKeyDownCharacter:@"a"];
}

@end
