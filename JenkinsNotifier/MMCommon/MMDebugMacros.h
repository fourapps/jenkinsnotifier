//
//  Created by Michael May on 06/10/2012.
//  Copyright (c) 2012 Michael May. All rights reserved.
//

#ifndef NotificationCenterTest_MMDebugMacros_h
#define NotificationCenterTest_MMDebugMacros_h

// from http://iphoneincubator.com/blog/debugging/the-evolution-of-a-replacement-for-nslog
// DLog is almost a drop-in replacement for NSLog
// DLog();
// DLog(@"here");
// DLog(@"value: %d", x);
// Unfortunately this doesn't work DLog(aStringVariable); you have to do this instead DLog(@"%@", aStringVariable);
#ifdef DEBUG

#define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#define DAssert(assertion,message) NSAssert(assertion,message)
#define DAssertClass(aClass,anObject) NSAssert([anObject isKindOfClass:[aClass class]], @"object is not of class expected")

NS_INLINE void DAssertNotNil(id obj) {
    assert(obj != nil);
}

#else

#define DLog(...)
#define DAssert(assertion,message)
#define DAssertClass(aClass,anObject)

NS_INLINE void DAssertNotNil(id obj) {}

#endif

// ALog always displays output regardless of the DEBUG setting
#define ALog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);

#endif
