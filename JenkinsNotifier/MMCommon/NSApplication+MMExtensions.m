//
//  Created by Michael May on 20/01/2012.
//  Copyright (c) 2012 Michael May. All rights reserved.
//

#import "NSApplication+MMExtensions.h"

@implementation NSApplication (MMExtensions)

#pragma mark - 

+(id<NSApplicationDelegate>)applicationDelegate {
    NSApplication *application = [NSApplication sharedApplication];
    id<NSApplicationDelegate> appDelegate = [application delegate];
    
    return appDelegate;
}

#pragma mark -

+(NSString*)appName {
	NSBundle *appBundle = [NSBundle mainBundle];
	NSString *appName = [appBundle objectForInfoDictionaryKey:@"CFBundleName"];

	return appName;
}

+(NSString*)appVersionString {
	NSBundle *appBundle = [NSBundle mainBundle];
	NSString *appBuild = [appBundle objectForInfoDictionaryKey:@"CFBundleVersion"];
	NSString *appVersion = [appBundle objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
	NSString *appVersionString = [NSString stringWithFormat:@"%@ (%@)", appVersion, appBuild];
	
	return appVersionString;
}

+(NSString*)directoryOfType:(NSSearchPathDirectory)directoryType {
	NSArray* directoryPathsArray = NSSearchPathForDirectoriesInDomains(directoryType, NSUserDomainMask, YES);
	NSString* directoryPath = [directoryPathsArray lastObject];
	
	return directoryPath;	
}

+(NSString*)cachesDirectory {
	return [NSApplication directoryOfType:NSCachesDirectory];
}

+(NSString*)documentsDirectory {
	return [NSApplication directoryOfType:NSDocumentDirectory];
}

+(NSString*)libraryDirectory {
	return [NSApplication directoryOfType:NSLibraryDirectory];
}

@end
