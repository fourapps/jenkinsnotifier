//
//  Created by Michael May on 20/04/2013.
//  Copyright (c) 2013 Four Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (ObjectAtIndexIfNotOutOfBounds)

-(id)objectAtIndexIfNotOutOfBounds:(NSInteger)index;


@end
