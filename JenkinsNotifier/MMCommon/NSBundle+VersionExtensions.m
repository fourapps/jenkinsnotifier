//
//  Created by Michael May on 10/07/2013.
//

#import "NSBundle+VersionExtensions.h"

@implementation NSBundle (VersionExtensions)

-(NSString*)versionString
{
    NSString *buildNumber = [self objectForInfoDictionaryKey:@"CFBundleVersion"];
    NSString *versionNumber = [self objectForInfoDictionaryKey:@"CFBundleShortVersionString"];

    if([buildNumber length] > 0) {
        return [NSString stringWithFormat:@"%@ (%@)", versionNumber, buildNumber];
    }
    
    return versionNumber;
}

-(NSString*)stringWithByReplacingVersionPlaceholders:(NSString*)string
{
    NSString *appVersion = [[NSBundle mainBundle] versionString];
    NSString *adjustedString = string;
    
    if(appVersion) {
        adjustedString = [string stringByReplacingOccurrencesOfString:BQOVersionPlaceholder withString:appVersion];
    }
    
    return adjustedString;
}

@end
