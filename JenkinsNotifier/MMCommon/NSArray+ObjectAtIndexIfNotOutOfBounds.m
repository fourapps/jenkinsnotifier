//  JenkinsNotifier
//
//  Created by Michael May on 20/04/2013.
//  Copyright (c) 2013 Four Apps. All rights reserved.
//

#import "NSArray+ObjectAtIndexIfNotOutOfBounds.h"

@implementation NSArray (ObjectAtIndexIfNotOutOfBounds)

-(id)objectAtIndexIfNotOutOfBounds:(NSInteger)index {
    id object = nil;
    
    if(index >= 0 && index < [self count]) {
        return [self objectAtIndex:index];
    }
    
    return object;
}

@end
