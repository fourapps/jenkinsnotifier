//
//  Created by Michael May on 06/10/2012.
//  Copyright (c) 2012 Michael May. All rights reserved.
//

#ifndef NotificationCenterTest_MMGCDMacros_h
#define NotificationCenterTest_MMGCDMacros_h

#define mm_default_dispatch_queue() dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0x00)

#endif
