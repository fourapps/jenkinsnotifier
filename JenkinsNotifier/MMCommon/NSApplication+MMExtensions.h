//
//  Created by Michael May on 20/01/2012.
//  Copyright (c) 2012 Michael May. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSApplication (ASPExtensions)

+(id<NSApplicationDelegate>)applicationDelegate;

+(NSString*)appName;

+(NSString*)appVersionString;

+(NSString*)cachesDirectory;

+(NSString*)documentsDirectory;

+(NSString*)libraryDirectory;

@end
