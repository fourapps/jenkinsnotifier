//
//  Created by Michael May on 10/07/2013.
//

#define BQOVersionPlaceholder @"[VERSION]"

@interface NSBundle (VersionExtensions)

-(NSString*)versionString;

-(NSString*)stringWithByReplacingVersionPlaceholders:(NSString*)string;

@end
