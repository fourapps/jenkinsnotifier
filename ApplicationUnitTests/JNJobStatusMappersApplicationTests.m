//
//  Created by Developer on 2014/03/05.
//  Copyright (c) 2014 Four Apps. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>

#import "JNJenkinsAPIJob.h"
#import "JNJobStatusMappers.h"

@interface JNJobStatusMappersApplicationTests : SenTestCase

@end

@implementation JNJobStatusMappersApplicationTests

#pragma mark - jobImageForState:

-(void)testThatJobImageForJobStateReturnsAStringForEveryState
{
    // given
    // when
    // then
    for(JNJenkinsAPIJobStates state = 0; state < JNJenkinsAPIJobStateCount; state++) {
        NSImage *jobImage = [JNJobStatusMappers jobImageimageForState:state];
        STAssertNotNil(jobImage, @"");
    }
}

@end
