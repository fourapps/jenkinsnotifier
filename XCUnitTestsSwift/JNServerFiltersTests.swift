//
//  Created by Michael May on 2015/12/08.
//  Copyright © 2015 Michael May. All rights reserved.
//

import XCTest

import JenkinsNotifier

class JNServerFiltersTests: XCTestCase {
    private var filters : JNServerFilters?
    
    private let jobIdentifier0 = "MyID0"
    private let jobTitle0 = "MyTitle0"
    private let jobPredicate0 = "0"
    
    private let jobIdentifier1 = "MyID1"
    private let jobTitle1 = "MyTitle1"
    private let jobPredicate1 = "1"
    
    private let unknownIdentifierString = "IdentifierWhichDoesNotExist"
    
    override func setUp() {
        super.setUp()
        
        self.filters = JNServerFilters.init(defaultFilters: [:], userFilters: [:])
    }
    
    override func tearDown() {
        self.filters = nil

        super.tearDown()
    }

    private func setUpDummyJobFilters() {
        let jobEntry0 = [JNServerFiltersIdentifierKey : self.jobIdentifier0,
            JNServerFiltersTitleKey : self.jobTitle0,
            JNServerFiltersPredicateKey : self.jobPredicate0]

        let jobEntry1 = [JNServerFiltersIdentifierKey : self.jobIdentifier1,
            JNServerFiltersTitleKey : self.jobTitle1,
            JNServerFiltersPredicateKey : self.jobPredicate1]

        let jobEntry2 = [JNServerFiltersIdentifierKey : "MyID2",
            JNServerFiltersTitleKey : "MyTitle2",
            JNServerFiltersPredicateKey : "2"]
        
        let jobFilters = [ "MyJobEntry0" : jobEntry0, "MyJobEntry1" : jobEntry1, "MyJobEntry2" : jobEntry2 ]
        
        // currently
        self.filters = JNServerFilters.init(defaultFilters: [:], userFilters: jobFilters)
    }
 
    func testThatFactoryMethodReturnsAValidObjectForEmptyFilters() {
        // given
        
        // when
        
        // then
        XCTAssertNotNil(filters)
    }

    func testThatIdentifierForFilterAtIndexReturnsNilForEmptyFilters() {
        // given
        
        // when
        let identifier = filters?.identifierForFilterAtIndex(0)
        
        // then
        XCTAssertNil(identifier)
    }

    func testThatTitleForFilterAtIndexReturnsNilForEmptyFilters() {
        // given
        
        // when
        let title = self.filters?.titleForFilterAtIndex(0)
        
        // then
        XCTAssertNil(title)
    }

    
//    -(void)
//    {
//    // given
//    
//    // when
//    NSString *title = [filters titleForFilterAtIndex:0];
//    
//    // then
//    XCTAssertNil(title, @"");
//    }
//    


    // this fails because the order of the servers is not honoured
    func testThatIdentifierForFilterAtIndexReturnsValue() {
        // given
        self.setUpDummyJobFilters()
        
        // when
        let identifier = filters?.identifierForFilterAtIndex(0)
        
        // then
        XCTAssertNotNil(identifier)
        
        // TODO: this fails because the order of the jobs is not honoured
        // XCTAssertEqual(identifier, jobIdentifier0);
    }

    func testThatIdentifierForFilterAtIndexOutOfRangeReturnsNil() {
        // given
        self.setUpDummyJobFilters()
        
        // when
        let identifier = filters?.identifierForFilterAtIndex(100)
        
        // then
        XCTAssertNil(identifier)
    }

    func testThatTitleForFilterAtIndexReturnsNilForIndexOutOfBunds() {
        // given
        self.setUpDummyJobFilters()
        
        // when
        let title = self.filters?.titleForFilterAtIndex(1000)
        
        // then
        XCTAssertNil(title)
    }

    func testThatTitleForFilterAtIndexReturnsCorrectTitle() {
        // given
        self.setUpDummyJobFilters()
        
        // when
        let title = self.filters?.titleForFilterAtIndex(0)
        
        // then
        XCTAssertNotNil(title)
        
        // TODO: this fails because the order of the jobs is not honoured
        //XCTAssertEqual(title, jobTitle0)
    }

    func testThatPredicateForFilterAtIndexReturnsNilForEmptyFilters() {
        // given
        
        // when
        let predicate = self.filters?.predicateForFilterAtIndex(0)
        
        // then
        XCTAssertNil(predicate)
    }

    func testThatPredicateForFilterAtIndexReturnsNilForIndexOutOfBunds() {
        // given
        self.setUpDummyJobFilters()
        
        // when
        let predicate = self.filters?.predicateForFilterAtIndex(1000)
        
        // then
        XCTAssertNil(predicate)
    }

    func testThatPredicateForFilterAtIndexReturnsCorrectPredicateForFirstIndex() {
        // given
        self.setUpDummyJobFilters()
        
        // when
        let predicate = self.filters?.predicateForFilterAtIndex(0)
        
        // then
        XCTAssertNotNil(predicate)
        
        // TODO: this fails because the order of the jobs is not honoured
        //XCTAssertEqual(predicate, jobPredicate0)
    }

    func testThatPredicateForFilterAtIndexReturnsCorrectPredicateForSecondIndex() {
        // given
        self.setUpDummyJobFilters()
        
        // when
        let predicate = self.filters?.predicateForFilterAtIndex(1)
        
        // then
        XCTAssertNotNil(predicate)
        
        // TODO: this fails because the order of the jobs is not honoured
        //XCTAssertEqual(predicate, jobPredicate1)
    }

    func testThatNumberOfEntriesReturnsZeroForEmptyFilters() {
        // given
        
        // when
        let numberOfEntries = filters?.numberOfEntries()
        
        // then
        XCTAssertEqual(numberOfEntries, 0)
    }

    func testThatNumberOfEntriesReturnsCorrectValue() {
        // given
        self.setUpDummyJobFilters()
        
        // when
        let numberOfEntries = filters?.numberOfEntries()
        
        // then
        XCTAssertEqual(numberOfEntries, 3)
    }

    func testThatIndexOfItemWithTitleReturnsNSNotFoundForEmptyFilters() {
        // given
        
        // when
        let index = self.filters?.indexOfFilterWithTitle(jobTitle0)
        
        // then
        XCTAssertEqual(index, NSNotFound)
    }

    func testThatIndexOfItemWithTitleReturnsNSNotFoundWhenTitleDoesNotExist() {
        // given
        self.setUpDummyJobFilters()

        // when
        let index = self.filters?.indexOfFilterWithTitle("ATitleWhichDoesNotExist")
        
        // then
        XCTAssertEqual(index, NSNotFound)
    }

    func testThatIndexOfItemWithTitleReturnsCorrectIndexForFirstTitle() {
        // given
        self.setUpDummyJobFilters()
        
        // when
        let index = self.filters?.indexOfFilterWithTitle(self.jobTitle0)
        
        // then
        XCTAssertNotEqual(index, NSNotFound)
        
        // TODO: this fails because the order of the jobs is not honoured
        //XCTAssertEqual(index, 0)
    }

    func testThatIndexOfItemWithTitleReturnsCorrectIndexForSecondTitle() {
        // given
        self.setUpDummyJobFilters()
        
        // when
        let index = self.filters?.indexOfFilterWithTitle(self.jobTitle1)
        
        // then
        XCTAssertNotEqual(index, NSNotFound)
        
        // TODO: this fails because the order of the jobs is not honoured
        //XCTAssertEqual(index, 1)
    }

    func testThatIndexOfFilterWithIdentifierReturnsNSNotFoundForEmptyFilters() {
        // given
        
        // when
        let index = self.filters?.indexOfFiterWithIdentifier(self.jobIdentifier0)
        
        // then
        XCTAssertEqual(index, NSNotFound)
    }
    
    func testThatIndexOfFilterWithIdentifierReturnsNSNotFoundWhenTitleDoesNotExist() {
        // given
        self.setUpDummyJobFilters()
        
        // when
        let index = self.filters?.indexOfFiterWithIdentifier(self.unknownIdentifierString)
        
        // then
        XCTAssertEqual(index, NSNotFound)
    }
    
    func testThatIndexOfFilterWithIdentifierReturnsCorrectIndexForIdentifier0() {
        // given
        self.setUpDummyJobFilters()
        
        // when
        let index = self.filters?.indexOfFiterWithIdentifier(self.jobIdentifier0)
        
        // then
        XCTAssertNotEqual(index, NSNotFound)
        
        // TODO: this fails because the order of the jobs is not honoured
        // XCTAssertEqual(index, 0)
    }

    func testThatIndexOfFilterWithIdentifierReturnsCorrectIndexForIdentifier1() {
        // given
        self.setUpDummyJobFilters()
        
        // when
        let index = self.filters?.indexOfFiterWithIdentifier(self.jobIdentifier1)
        
        // then
        XCTAssertNotEqual(index, NSNotFound)
        
        // TODO: this fails because the order of the jobs is not honoured
        // XCTAssertEqual(index, 1)
    }

    func testThatTitleForFilterWithIdentifierReturnsNilForEmptyFilters() {
        // given
        
        // when
        let title = self.filters?.titleForFilterWithIdentifier(self.jobIdentifier0)

        // then
        XCTAssertNil(title)
    }
    
    func testThatTitleForFilterWithIdentifierReturnsNilForUnknownIdentifier() {
        // given
        self.setUpDummyJobFilters()

        // when
        let title = self.filters?.titleForFilterWithIdentifier(self.unknownIdentifierString)

        // then
        XCTAssertNil(title)
    }
    
    func testThatTitleForFilterWithIdentifierReturnsCorrectTitleForFirstJobIdentifier() {
        // given
        self.setUpDummyJobFilters()

        // when
        let title = self.filters?.titleForFilterWithIdentifier(self.jobIdentifier0)
        
        // then
        XCTAssertNotNil(title)
        XCTAssertEqual(title, jobTitle0)
    }

    func testThatTitleForFilterWithIdentifierReturnsCorrectTitleForSecondJobIdentifier() {
        // given
        self.setUpDummyJobFilters()
        
        // when
        let title = self.filters?.titleForFilterWithIdentifier(self.jobIdentifier1)
        
        // then
        XCTAssertNotNil(title)
        XCTAssertEqual(title, jobTitle1)
    }

    func testThatPredicateForFilterWithIdentifierReturnsNilForEmptyFilters() {
        // given
        
        // when
        let predicate = self.filters?.predicateForFilterWithIdentifier(self.jobIdentifier0)
        
        // then
        XCTAssertNil(predicate)
    }

    func testThatPredicateForFilterWithIdentifierReturnsNilForUnknownIdentifier() {
        // given
        self.setUpDummyJobFilters()
        
        // when
        let predicate = self.filters?.predicateForFilterWithIdentifier(self.unknownIdentifierString)
        
        // then
        XCTAssertNil(predicate)
    }

    func testThatPredicateForFilterWithIdentifierReturnsCorrectPredicateForFirstJobIdentifier() {
        // given
        self.setUpDummyJobFilters()
        
        // when
        let predicate = self.filters?.predicateForFilterWithIdentifier(self.jobIdentifier0)
        
        // then
        XCTAssertEqual(predicate, jobPredicate0)
    }

    func testThatPredicateForFilterWithIdentifierReturnsCorrectPredicateForSecondJobIdentifier() {
        // given
        self.setUpDummyJobFilters()
        
        // when
        let predicate = self.filters?.predicateForFilterWithIdentifier(self.jobIdentifier1)
        
        // then
        XCTAssertEqual(predicate, jobPredicate1)
    }
    
    
}
