//
//  Created by Michael May on 2015/12/09.
//  Copyright © 2015 Michael May. All rights reserved.
//

import XCTest

import JenkinsNotifier

class JNJenkinsServerEntryTests: XCTestCase {

    private let fullValidEncodedString = "myUsername|myPassword|http://www.google.com"
    private let emptyString = "::"
    
    private let userNameOnlyEncodedString = "myUserName||"
    private let paswordOnlyEncodedString = "|myPassword|"
    private let serverURLOnlyEncodedString = "||http://www.google.com"
    private let URLInFullValidEncodedString = "http://www.google.com"
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testThatPassingANilEncodedStringProducesAValidEmptyObject() {
        // given
        
        // when
        let entry = JNJenkinsServerEntry.init(encodedString: nil)
        
        // then
        XCTAssertNotNil(entry)
        XCTAssertNotNil(entry.username)
        XCTAssertNotNil(entry.password)
        XCTAssertNotNil(entry.URLString)
    }

    func testThatPassingAValidByEmptyStringProducesAValidEmptyObject() {
        // given
        
        // when
        let entry = JNJenkinsServerEntry.init(encodedString: self.emptyString)
        
        // then
        XCTAssertNotNil(entry)
        XCTAssertNotNil(entry.username)
        XCTAssertNotNil(entry.password)
        XCTAssertNotNil(entry.URLString)
    }

    func testThatPassingAValidUsernameOnlyStringProducesAValidObject() {
        // given
        
        // when
        let entry = JNJenkinsServerEntry.init(encodedString: self.userNameOnlyEncodedString)
        
        // then
        XCTAssertNotNil(entry)
        XCTAssertEqual(entry.username, "myUserName")
        XCTAssertNotNil(entry.password)
        XCTAssertNotNil(entry.URLString)
    }

    func testThatPassingAValidPasswordOnlyStringProducesAValidObject() {
        // given
        
        // when
        let entry = JNJenkinsServerEntry.init(encodedString: self.paswordOnlyEncodedString)
        
        // then
        XCTAssertNotNil(entry)
        XCTAssertNotNil(entry.username)
        XCTAssertEqual(entry.password, "myPassword")
        XCTAssertNotNil(entry.URLString)
    }

    func testThatPassingAValidURLOnlyStringProducesAValidObject() {
        // given
        
        // when
        let entry = JNJenkinsServerEntry.init(encodedString: self.serverURLOnlyEncodedString)
        
        // then
        XCTAssertNotNil(entry)
        XCTAssertNotNil(entry.username)
        XCTAssertNotNil(entry.password)
        XCTAssertEqual(entry.URLString, "http://www.google.com")
    }

    func testThatPassingAValidEncodedStringProducesAValidObject() {
        // given
        
        // when
        let entry = JNJenkinsServerEntry.init(encodedString: self.fullValidEncodedString)
        
        // then
        XCTAssertNotNil(entry)
        XCTAssertEqual(entry.username, "myUsername")
        XCTAssertEqual(entry.password, "myPassword")
        XCTAssertEqual(entry.URLString, "http://www.google.com")
    }

    func testThatPassingAValidEncodedStringWithReservedCharactersReturnsValidValues() {
        // given
        let reservedCharacterEncodedString = "my%7CUsername|my%7CPassword|http%7C//www.google.com"
        
        // when
        let entry = JNJenkinsServerEntry.init(encodedString: reservedCharacterEncodedString)
        
        // then
        XCTAssertNotNil(entry)
        XCTAssertEqual(entry.username, "my|Username")
        XCTAssertEqual(entry.password, "my|Password")
        XCTAssertEqual(entry.URLString, "http|//www.google.com")
    }

    func testThatSettingUsernameCorrectlySetsTheEntryUsername() {
        // given
        let entry = JNJenkinsServerEntry.init(encodedString: fullValidEncodedString)
        
        // when
        let myNewUsernameValue = "myNewUsername"
        entry.username = myNewUsernameValue;
        
        // then
        XCTAssertEqual(entry.username, myNewUsernameValue)
    }

    func testThatSettingUsernameCorrectlyReturnsTheNewValueInTheEncodedString() {
        // given
        let entry = JNJenkinsServerEntry.init(encodedString: fullValidEncodedString)

        // when
        let myNewUsernameValue = "myNewUsername"
        entry.username = myNewUsernameValue;
        
        // then
        let expectedEncodedString = "myNewUsername|myPassword|http://www.google.com|0"
        let encodedString = entry.encodedAsString()
        XCTAssertEqual(expectedEncodedString, encodedString)
    }

    func testThatSettingUsernameWithReservedCharacterBarIsHandledCorrecty() {
        // given
        let entry = JNJenkinsServerEntry.init(encodedString: fullValidEncodedString)
        
        // when
        let myNewUsernameValue = "myNew|Username"
        entry.username = myNewUsernameValue
        
        // then
        let expectedEncodedString = "myNew%7CUsername|myPassword|http://www.google.com|0"
        let encodedString = entry.encodedAsString()
        XCTAssertEqual(expectedEncodedString, encodedString)
    }

    func testThatSettingUsernameWithReservedCharacterPercentIsHandledCorrecty() {
        // given
        let entry = JNJenkinsServerEntry.init(encodedString: fullValidEncodedString)
        
        // when
        let myNewUsernameValue = "myNew%Username"
        entry.username = myNewUsernameValue

        // then
        let expectedEncodedString = "myNew%25Username|myPassword|http://www.google.com|0"
        let encodedString = entry.encodedAsString()
        XCTAssertEqual(expectedEncodedString, encodedString)
        XCTAssertEqual(entry.username, myNewUsernameValue)
    }

    func testThatSettingUsernameWithReservedCharacterIsEncodedDecodedCorrectly() {
        // given
        let entry = JNJenkinsServerEntry.init(encodedString: fullValidEncodedString)

        // when
        let myNewUsernameValue = "myNew|%Username"
        entry.username = myNewUsernameValue
        
        // then
        XCTAssertEqual(entry.username, myNewUsernameValue)
    }

    func testThatSettingPasswordCorrectlySetsTheEntryPassword() {
        // given
        let entry = JNJenkinsServerEntry.init(encodedString: fullValidEncodedString)

        // when
        let myNewPasswordValue = "myNewPassword"
        entry.password = myNewPasswordValue

        
        // then
        XCTAssertEqual(entry.password, myNewPasswordValue)
    }

    func testThatSettingPasswordCorrectlyReturnsTheNewValueInTheEncodedString() {
        // given
        let entry = JNJenkinsServerEntry.init(encodedString: fullValidEncodedString)

        // when
        let myNewPasswordValue = "myNewPassword"
        entry.password = myNewPasswordValue

        // then
        let expectedEncodedString = "myUsername|myNewPassword|http://www.google.com|0"
        let encodedString = entry.encodedAsString()
        XCTAssertEqual(expectedEncodedString, encodedString)
    }

    func testThatSettingPasswordWithReservedCharacterBarIsHandledCorrecty() {
        // given
        let entry = JNJenkinsServerEntry.init(encodedString: fullValidEncodedString)
        
        // when
        let myNewPasswordValue = "myNew|Password"
        entry.password = myNewPasswordValue
        
        // then
        let expectedEncodedString = "myUsername|myNew%7CPassword|http://www.google.com|0"
        let encodedString = entry.encodedAsString()
        XCTAssertEqual(expectedEncodedString, encodedString)
    }

    func testThatSettingPasswordWithReservedCharacterPercentIsHandledCorrecty() {
        // given
        let entry = JNJenkinsServerEntry.init(encodedString: fullValidEncodedString)

        // when
        let myNewPasswordValue = "myNew%Password"
        entry.password = myNewPasswordValue
        
        // then
        let expectedEncodedString = "myUsername|myNew%25Password|http://www.google.com|0"
        let encodedString = entry.encodedAsString()
        XCTAssertEqual(expectedEncodedString, encodedString)
    }

    func testThatSettingPasswordWithReservedCharactersIsEncodedDecodedCorrectly() {
        // given
        let entry = JNJenkinsServerEntry.init(encodedString: fullValidEncodedString)
        
        // when
        let myNewPasswordValue = "myNew%Password"
        entry.password = myNewPasswordValue
        
        // then
        XCTAssertEqual(entry.password, myNewPasswordValue)
    }

    func testThatSettingURLStringCorrectlySetsTheEntryURLString() {
        // given
        let entry = JNJenkinsServerEntry.init(encodedString: fullValidEncodedString)
        
        // when
        let myNewURLStringValue = "http://www.google.co.uk"
        entry.URLString = myNewURLStringValue
        
        // then
        XCTAssertEqual(entry.URLString, myNewURLStringValue)
    }
    
    func testThatSettingURLStringCorrectlyReturnsTheNewValueInTheEncodedString() {
        // given
        let entry = JNJenkinsServerEntry.init(encodedString: fullValidEncodedString)
        
        // when
        let myNewURLStringValue = "http|//www.google.co.uk"
        entry.URLString = myNewURLStringValue
        
        // then
        let expectedEncodedString = "myUsername|myPassword|http%7C//www.google.co.uk|0"
        let encodedString = entry.encodedAsString()
        XCTAssertEqual(expectedEncodedString, encodedString)
    }

    func testThatSettingURLStringWithReservedCharacterIsHandledCorrecty() {
        // given
        let entry = JNJenkinsServerEntry.init(encodedString: fullValidEncodedString)
        
        // when
        let myNewURLStringValue = "myNew|URL"
        entry.URLString = myNewURLStringValue
        
        // then
        let expectedEncodedString = "myUsername|myPassword|myNew%7CURL|0"
        let encodedString = entry.encodedAsString()
        XCTAssertEqual(expectedEncodedString, encodedString)
    }

    func testThatSettingURLStringWithReservedCharacterPercentIsHandledCorrecty() {
        // given
        let entry = JNJenkinsServerEntry.init(encodedString: fullValidEncodedString)
        
        // when
        let myNewURLStringValue = "myNew%URL"
        entry.URLString = myNewURLStringValue
        
        // then
        let expectedEncodedString = "myUsername|myPassword|myNew%25URL|0"
        let encodedString = entry.encodedAsString()
        XCTAssertEqual(expectedEncodedString, encodedString)
    }

    func testThatSettingURLStringWithReservedCharacterIsEncodedDecodedCorrectly() {
        // given
        let entry = JNJenkinsServerEntry.init(encodedString: fullValidEncodedString)
        
        // when
        let myNewURLStringValue = "|myNew%|%URL"
        entry.URLString = myNewURLStringValue
        
        // then
        XCTAssertEqual(entry.URLString, myNewURLStringValue)
    }
    
    func testThatAnEncodedStringFromOneEntryCanCorrectlyBeDecodedToInitialiseANewEntryWithTheSameValues() {
        // given
        let entry = JNJenkinsServerEntry.init(encodedString: self.fullValidEncodedString)
        let encodedValues = entry.encodedAsString()
        
        // when
        let newEntry = JNJenkinsServerEntry.init(encodedString: encodedValues)
        
        // then
        XCTAssertEqual(entry.username, newEntry.username)
        XCTAssertEqual(entry.password, newEntry.password)
        XCTAssertEqual(entry.URLString, newEntry.URLString)
    }

    func testThatTwoEmptyServerEntriesAreEqualEvenThoughTheyAreNotTheSameObject() {
        // given
        let entry = JNJenkinsServerEntry.init()
        let secondEntry = JNJenkinsServerEntry.init()
        
        // when
        // then
        XCTAssertTrue(entry.isEqual(secondEntry))
    }

    func testThatAnTwoServerEntriesWithDifferingValuesAreNotEqual() {
        // given
        let entry = JNJenkinsServerEntry.init(encodedString: self.fullValidEncodedString)
        let secondEntry = JNJenkinsServerEntry.init(encodedString: "")

        // when
        // then
        XCTAssertFalse(entry.isEqual(secondEntry))
    }

    func testThatAnTwoServerEntriesWithTheSameValuesButAreDifferentObjectsAreEqual() {
        // given
        let entry = JNJenkinsServerEntry.init(encodedString: self.fullValidEncodedString)
        let secondEntry = JNJenkinsServerEntry.init(encodedString: self.fullValidEncodedString)
        
        // when
        // then
        XCTAssertTrue(entry.isEqual(secondEntry))
    }
    
    func testThatAnTwoServerEntriesWithTheSameURLStringAndNilUsernameAndPasswordButAreDifferentObjectsAreEqual() {
        // given
        let entry = JNJenkinsServerEntry.init()
        entry.URLString = "http://www.test.com"
        
        let secondEntry = JNJenkinsServerEntry.init()
        secondEntry.URLString = "http://www.test.com"
        
        // when
        // then
        XCTAssertTrue(entry.isEqual(secondEntry))
    }

    func testThatAnTwoServerEntriesWithTheSameUsernameAndNilURLStringAndPasswordButAreDifferentObjectsAreEqual() {
        // given
        let entry = JNJenkinsServerEntry.init()
        entry.username = "mmay"
        
        let secondEntry = JNJenkinsServerEntry.init()
        secondEntry.username = "mmay"
        
        // when
        // then
        XCTAssertTrue(entry.isEqual(secondEntry))
    }

    func testThatAnTwoServerEntriesWithTheSamePasswordAndNilURLStringAndUsernameButAreDifferentObjectsAreEqual() {
        // given
        let entry = JNJenkinsServerEntry.init()
        entry.password = "password"
        
        let secondEntry = JNJenkinsServerEntry.init()
        secondEntry.password = "password"
        
        // when
        // then
        XCTAssertTrue(entry.isEqual(secondEntry))
    }

    func testThatANewObjectWithNoURLStringReturnsANilBaseURL() {
        // given
        let entry = JNJenkinsServerEntry.init()
        
        // when
        let baseURL = entry.baseURL()
        
        // then
        XCTAssertNil(baseURL)
    }
    
    func testThatANewObjectWithABlankEncodedStringReturnsANullBaseURL() {
        // given
        let entry = JNJenkinsServerEntry.init(encodedString: "")
        
        // when
        let baseURL = entry.baseURL()
        
        // then
        XCTAssertNil(baseURL)
    }

    func testThatANewObjectWithAURLStringReturnsABaseURlThatHasTheSameAbsoluteURLString() {
        // given
        let entry = JNJenkinsServerEntry.init(encodedString: self.fullValidEncodedString)

        // when
        let baseURL = entry.baseURL()
        
        // then
        XCTAssertEqual(baseURL.absoluteString, URLInFullValidEncodedString)
    }

    func testThatANewObjectWithNoEncodedStringReturnsANoForHasAuthenticationDetails() {
        // given
        let entry = JNJenkinsServerEntry.init()

        // when
        // then
        XCTAssertFalse(entry.hasAuthenticationDetails())
    }

    func testThatANewObjectWithFullEncodedStringReturnsYesForHasAuthenticationDetails() {
        // given
        let entry = JNJenkinsServerEntry.init(encodedString: self.fullValidEncodedString)

        // when
        // then
        XCTAssertTrue(entry.hasAuthenticationDetails())
    }

    func testThatANewObjectWithAUsernameOnlyEncodedStringReturnsYesForHasAuthenticationDetails() {
        // given
        let entry = JNJenkinsServerEntry.init(encodedString: self.userNameOnlyEncodedString)

        // when
        // then
        XCTAssertTrue(entry.hasAuthenticationDetails())
    }

    func testThatANewObjectWithAPasswordOnlyEncodedStringReturnsYesForHasAuthenticationDetails() {
        // given
        let entry = JNJenkinsServerEntry.init(encodedString: self.paswordOnlyEncodedString)
        
        // when
        
        // then
        XCTAssertTrue(entry.hasAuthenticationDetails())
    }

    func testThatDescriptionReturnsAString() {
        // given
        let entry = JNJenkinsServerEntry.init(encodedString: self.fullValidEncodedString)

        // when
        let description : String? = entry.description
        
        // then
        XCTAssertNotNil(description)
    }

    func testThatCallingServerURLWithAdditionalPathComponentsWithNilPathComponentsAndEmptyServerEntryReturnsAnNilURL() {
        // given
        let entry = JNJenkinsServerEntry.init(encodedString: nil)

        // when
        let pathComponents : String? = nil
        let serverURL = entry.serverURLWithAdditionalPathComponents(pathComponents)
        
        // then
        XCTAssertNil(serverURL)
    }
    
    func testThatCallingServerURLWithAdditionalPathComponentsWithPathComponentsAndEmptyServerEntryReturnsAnNilURL() {
        // given
        let entry = JNJenkinsServerEntry.init(encodedString: nil)

        // when
        let pathComponents = "/hudson/api/json"
        let serverURL = entry.serverURLWithAdditionalPathComponents(pathComponents)
        
        // then
        XCTAssertNil(serverURL)
    }
    
    func testThatCallingServerURLWithAdditionalPathComponentsWithNilPathComponentsAndValidServerEntryReturnsUntouchedURL() {
        // given
        let entry = JNJenkinsServerEntry.init(encodedString: self.fullValidEncodedString)
        
        // when
        let pathComponents : String? = nil
        let serverURL = entry.serverURLWithAdditionalPathComponents(pathComponents)

        // then
        let expectedServerURL = NSURL.init(string: "http://www.google.com")
        XCTAssertEqual(expectedServerURL, serverURL)
    }

    func testThatCallingServerURLWithAdditionalPathComponentsWithEmptyPathComponentsAndValidServerEntryReturnsUntouchedURL() {
        // given
        let entry = JNJenkinsServerEntry.init(encodedString: self.fullValidEncodedString)
        
        // when
        let pathComponents = ""
        let serverURL = entry.serverURLWithAdditionalPathComponents(pathComponents)
        
        // then
        let expectedServerURL = NSURL.init(string: "http://www.google.com")
        XCTAssertEqual(expectedServerURL, serverURL)
    }

    func testThatCallingServerURLWithAdditionalPathComponentsWithEmptyPathComponentsAndEmptyServerEntryReturnsNilURL() {
        // given
        let entry = JNJenkinsServerEntry.init(encodedString: "")

        // when
        let pathComponents = ""
        let serverURL = entry.serverURLWithAdditionalPathComponents(pathComponents)
        
        // then
        XCTAssertNil(serverURL)
    }

    
    func testThatCallingServerURLWithAdditionalPathComponentsWithPathComponentsAndValidServerEntryReturnsAValidURL() {
        // given
        let pathComponents = "/hudson/api/json"
        let entry = JNJenkinsServerEntry.init(encodedString: self.fullValidEncodedString)

        // when
        let serverURL = entry.serverURLWithAdditionalPathComponents(pathComponents)
        
        // then
        let expectedServerURL = NSURL.init(string: "http://www.google.com/hudson/api/json")
        XCTAssertEqual(expectedServerURL, serverURL)
    }
    
    func testThatCallingServerURLWithAdditionalPathComponentsWithPathComponentsAndValidServerEntryIncludingPortAndPathReturnsTheRightURL() {
        // given
        let fullValidCIServerWithPortPathString = "myUsername|myPassword|http://www.google.com:8080/ci"

        let entry = JNJenkinsServerEntry.init(encodedString: fullValidCIServerWithPortPathString)

        // when
        let pathComponents = "/hudson/api/json"
        let serverURL = entry.serverURLWithAdditionalPathComponents(pathComponents)
        
        // then
        let expectedServerURL = NSURL.init(string: "http://www.google.com:8080/ci/hudson/api/json")
        XCTAssertEqual(expectedServerURL, serverURL)
    }

    private let fullValidCIServerWithUsernamePasswordBasicAuthAndPortPathString = "myUsername|myPassword|http://gooduser:secretpassword@www.google.com:8080/ci/path/to"
    
    func testThatCallingServerURLWithAdditionalPathComponentsWithPathComponentsAndValidServerEntryIncludingBasicAuthAndPortAndPathReturnsTheRightURL() {
        // given
        let entry = JNJenkinsServerEntry.init(encodedString: fullValidCIServerWithUsernamePasswordBasicAuthAndPortPathString)
        
        // when
        let pathComponents = "/hudson/api/json"
        let serverURL = entry.serverURLWithAdditionalPathComponents(pathComponents)
        
        // then
        let expectedServerURL = NSURL.init(string: "http://gooduser:secretpassword@www.google.com:8080/ci/path/to/hudson/api/json")
        XCTAssertEqual(expectedServerURL, serverURL)
    }

    func testThatCallingServerURLWithAdditionalPathComponentsWithServerURLAlreadyEndingInTheSamePathComponentsReturnsTheServerURL1() {
        // given
        let entry = JNJenkinsServerEntry.init(encodedString: fullValidCIServerWithUsernamePasswordBasicAuthAndPortPathString)

        // when
        let pathComponents = "/ci/path/to"
        let serverURL = entry.serverURLWithAdditionalPathComponents(pathComponents)

        // then
        let expectedServerURL = NSURL.init(string: "http://gooduser:secretpassword@www.google.com:8080/ci/path/to")
        XCTAssertEqual(expectedServerURL, serverURL)
    }
    
    func testThatCallingServerURLWithAdditionalPathComponentsWithServerURLAlreadyEndingInTheSamePathComponentsReturnsTheServerURL2() {
        // given
        let entry = JNJenkinsServerEntry.init(encodedString: fullValidCIServerWithUsernamePasswordBasicAuthAndPortPathString)

        // when
        let pathComponents = "to"
        let serverURL = entry.serverURLWithAdditionalPathComponents(pathComponents)

        // then
        let expectedServerURL = NSURL.init(string: "http://gooduser:secretpassword@www.google.com:8080/ci/path/to")
        XCTAssertEqual(expectedServerURL, serverURL)
    }

    func testThatCallingServerURLWithAdditionalPathComponentsWithServerURLAlreadyEndingInTheSamePathComponentsReturnsTheServerURL3() {
        // given
        let entry = JNJenkinsServerEntry.init(encodedString: fullValidCIServerWithUsernamePasswordBasicAuthAndPortPathString)
        
        // when
        let pathComponents = "/to"
        let serverURL = entry.serverURLWithAdditionalPathComponents(pathComponents)
        
        // then
        let expectedServerURL = NSURL.init(string: "http://gooduser:secretpassword@www.google.com:8080/ci/path/to")
        XCTAssertEqual(expectedServerURL, serverURL)
    }

    func testThatCallingServerURLWithAdditionalPathComponentsWithServerURLSimilarToPathComponentsReturnsTheNewFullURL() {
        // given
        let entry = JNJenkinsServerEntry.init(encodedString: fullValidCIServerWithUsernamePasswordBasicAuthAndPortPathString)

        // when
        let pathComponents = "/ci/paths/to"
        let serverURL = entry.serverURLWithAdditionalPathComponents(pathComponents)

        // then
        let expectedServerURL = NSURL.init(string: "http://gooduser:secretpassword@www.google.com:8080/ci/path/to/ci/paths/to")
        XCTAssertEqual(expectedServerURL, serverURL)
    }
    
}
